---
description: Träna på att skapa klasser
---

# Uppgifter

## Uppgift 1

![](/.gitbook/assets/oop/klasser/image-85.png)

* Skapa en klass `Elev` som innehåller variablerna `förnamn` och `efternamn`.
* Avsluta med att skriva ihop ett exempel där du använder klassen och visar att den fungerar.

## Uppgift 2

* Lägg till en variabel av typen DateTime som beskriver **födelsedatum**.

## Uppgift 3

* Lägg till variabler för **längd** och **vikt**. Använd lämplig datatyp.

## Uppgift 4

* Rita klassdiagrammet i [app.diagrams.net](https://app.diagrams.net/) eller med [app.creately.com](https://app.creately.com/).

## Uppgift 5

![](/.gitbook/assets/oop/klasser/image-86.png)

* Skapa en klass som representerar en bil. Förutom **märke** och **modell** så lägger du själv till ytterligare 3 variabler som beskriver en bil.
* Skriv sedan ett kort exempel där du skapar en ny bil, fyller objektet med data och sedan skriver ut datan.
* Rita klassdiagrammet i [app.diagrams.net](https://app.diagrams.net) eller [app.creately.com](https://app.creately.com/).

