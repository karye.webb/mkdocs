# Labb mediaregister

## Inledning

Skapa ett program som hanterar inmatning av böcker och filmer.

### Utseende & funktioner

Använd två flikar (TabControl) i programmet. När en bok eller film läggs till så ska resultat-fönstret uppdatera sig.

Om du saknar någon inmatning för bok eller film så ska du få upp ett felmeddelande om att värden saknas. Du ska endast kunna lägga till böcker och filmer om alla värden finns inmatade (värdet 0 för antal sidor eller spellängd räknas ej som ett giltigt värde).

Utseendet ska i så stor utsträckning som möjligt likna:

![](/.gitbook/assets/oop/image-104.png)

Du ska också kunna filtrera resultatet med de radio buttons som finns, där ”Alla” visar just all media, ”Böcker” visar endast böcker, etc.

### Layouten i xaml

Den övergripande layouten byggs med grid i xaml-filen:

```xml
<Grid Margin="10">
    <Grid.RowDefinitions>
        <RowDefinition Height="*"/>
        <RowDefinition Height="Auto"/>
    </Grid.RowDefinitions>
    <Grid.ColumnDefinitions>
        <ColumnDefinition Width="Auto"/>
        <ColumnDefinition Width="*"/>
    </Grid.ColumnDefinitions>
    <TabControl ...>
        <TabItem ...>
        ..
        </TabItem>
        <TabItem ...>
        ..
        </TabItem>
    </TabControl>
    <ListBox ...>
    <StackPanel ...>
        <Label ...>
        <RadioButton ...>
        <RadioButton ...>
        <RadioButton ...>
    </StackPanel>
<Grid>
```

#### Tabbarna

```xml
<TabControl Name="flikar" Grid.Row="0" Grid.Column="0" Margin="5" MinWidth="200">
    <TabItem Header="Film">
        <Grid>
            <Grid.RowDefinitions>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
            </Grid.RowDefinitions>
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="Auto"/>
                <ColumnDefinition Width="*"/>
            </Grid.ColumnDefinitions>
            <Label Grid.Row="0" Grid.Column="0">Titel</Label>
            <TextBox Name="rutaFilmtitel" Grid.Row="0" Grid.Column="1" Margin="5"/>
            <Label Grid.Row="1" Grid.Column="0">Regissör</Label>
            <TextBox Name="rutaRegissör" Grid.Row="1" Grid.Column="1" Margin="5"/>
            <Label Grid.Row="2" Grid.Column="0">Spellängd (min)</Label>
            <TextBox Name="rutaLängd" Grid.Row="2" Grid.Column="1" Margin="5"/>
            <Button Click="KlickSparaFilm" Grid.Row="3" Grid.Column="0">Spara</Button>
        </Grid>
    </TabItem>
    ...
</TabControl>
```

* [Hur man skapar tabbar](https://www.c-sharpcorner.com/uploadfile/prathore/tab-control-in-wpf)
* [Hur man skapar radioknappar](https://www.c-sharpcorner.com/UploadFile/mahesh/radiobutton-in-wpf)
* [Hur man använder en listbox](https://www.c-sharpcorner.com/uploadfile/mahesh/listbox-in-wpf/)


```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#FFF',
      'primaryBorderColor': '#000'
    }
  }
}%%
classDiagram
    class Media {
        + string Titel
        + «virtual» TillText() : string
    }
    class Bok {
        + string Författare
        + int AntalSidor
        + Bok(string titel, string författare, int antalsidor)
        + «override» TillText() : string
    }
    class Film {
        + string Regissör
        + int Längd
        + Film(string titel, string regissör, int längd)
        + «override» TillText() : string
    }
    Media <|-- Bok
    Media <|-- Film
```

### Flödeschema

Använd en List<> av typen `Bok` för att lagra böcker, och en List<> av typen `Film` för att lagra filmer. Översättningen till string sker med en `override` på metoden `TillText()`.

Felmeddelande visas med hjälp av klassen `MessageBox()`.  
Resultatet kan vara av typen `TextBox` (multiline) eller `ListBox`.

* När användaren klickar på knapparna Spara händer följande: ![](/.gitbook/assets/oop/image-106.png)
* När användaren klickar på en radioknapp händer följande: ![](/.gitbook/assets/oop/image-107.png)

### Tips

* För att veta vilken radioknapp som tryckts:

```csharp
string typ = ((RadioButton)sender).Name;
```

* För att veta vilken typ instansen är:

```csharp
if (instans is Bok)
...
```

### Förbättringar

Gör så att mediaregistrets data blir beständigt.\
Data skall sparas i JSON-filerna film.json och bok.json.\
När programmet startas skall data läsas in från json-filerna.

För att fånga fönstrets stänga event lägger man till `Closing="StängFönster"` i xaml:

```xml
<Window x:Class="MediaRegister.MainWindow" xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation" xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml" xmlns:d="http://schemas.microsoft.com/expression/blend/2008" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:local="clr-namespace:MediaRegister" mc:Ignorable="d" Title="Mediaregister" Height="400" MinWidth="500" Width="500" Background="#EEE"
Closing="StängFönster">
```

Och metoden StängFönster():

```csharp
void StängFönster(object sender, CancelEventArgs e)
{
...
}
```

### Körbar exe-fil

{% file src="/.gitbook/assets/oop/MediaRegister.exe" %}

## Förbättringar

Med polymorphism kan vi förenkla och använda en enda List<> av typen `Media` så att både böcker och filmer lagras i samma lista. Översättningen till string sker med en `override` på metoden `TillText()`.

### Flödeschema

* När användaren klickar på knapparna Spara händer följande: ![](/.gitbook/assets/oop/image-96.png)
* När användaren klickar på en radioknapp händer följande: ![](/.gitbook/assets/oop/image-97.png)

### Förbättringar

Mediaregistrets Data sparas i JSON-filerna `film.json` och `bok.json`. Hur kan man spara data i bara en json-fil?
