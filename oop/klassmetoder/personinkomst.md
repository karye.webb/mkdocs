---
description: Skapa och använda klassmetoder
---

# Labb PersonInkomst

![](/.gitbook/assets/oop/klassmetoder/image-127.png)

## Klassen PersonInkomst

För att samla data om en persons inkomst har vi skapat en klass:

```csharp
public class PersonInkomst
{
    public string namn;
    public int timlön;
    public int timmar;
}
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class PersonInkomst {
    + namn : string
    + timlön : int
    + timmar : int
}
@enduml
```

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#FFF',
      'primaryBorderColor': '#000'
    }
  }
}%%
classDiagram
    class PersonInkomst {
        + namn : string
        + timlön : int
        + timmar : int
    }
```

### Räkna ut lönekostnaden

Nu ska vi räkna ut den totala lönekostnaden för ett antal anställda.

Vi läser in flera anställda. Dessa lagra vi i en lista:

```csharp
// Lista med anställda
List<PersonInkomst> listaAnställda = new List<PersonInkomst>();

while (true)
{
    PersonInkomst person = new PersonInkomst();

    Console.Write("Ange namn: ");
    person.namn = Console.ReadLine();
    Console.Write("Ange timlön: ");
    person.timlön = int.Parse(Console.ReadLine());
    Console.Write("Ange antal timmar: ");
    person.timmar = int.Parse(Console.ReadLine());

    // Lägg till ansälld i listan
    listaAnställda.Add(person);

    // Vill du lägga till fler?
    Console.WriteLine("Vill du lägga till fler? (j/n)");
    if (Console.ReadLine().ToLower() == "n")
    {
        break;
    }
}
```

Efter att vi läste in alla anställda räknar vi ut totala lönekostnaderna:

```csharp
// Skriv ut antal anställda
Console.WriteLine($"Antal anställda: {listaAnställda.Count}");

// Loopa igenom alla anställda
int totalLön = 0;
foreach (var person in listaAnställda)
{
    Console.WriteLine($"{person.namn} tjänar {person.timlön * person.timmar} kr");

    // Lägg till timlön till totala lönen
    totalLön += person.timlön * person.timmar;
}

// Skriv ut totala lönen
Console.WriteLine($"Totala lönen är: {totalLön}");
```

## Klassmetoder

### Metoden BeräknaLön()

Vi kan även räkna ut lönen i klassen:

```csharp
public class PersonInkomst
{
    public string namn;
    public int inkomst;
    public int timmar;
    
    ...

    // Metod för att räkna ut lönen
    public int BeräknaLön()
    {
        return timlön * timmar;
    }
}
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class PersonInkomst {
    + namn : string
    + inkomst : int
    + timmar : int
    + BeräknaLön() : int
}
@enduml
```

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#FFF',
      'primaryBorderColor': '#000'
    }
  }
}%%
classDiagram
    class PersonInkomst {
        + namn : string
        + inkomst : int
        + timmar : int
        + BeräknaLön() : int
    }
```

#### Program.cs

Programmets kod blir då:

```csharp
Console.Clear();
Console.WriteLine("Lönekostnader");

...

// Skriv ut antal anställda
Console.WriteLine($"Antal anställda: {listaAnställda.Count}");

// Loopa igenom alla anställda
int totalLön = 0;
foreach (var person in listaAnställda)
{
    Console.WriteLine($"{person.namn} tjänar {person.BeräknaLön()} kr");

    // Lägg till timlön till totala lönen
    totalLön += person.BeräknaLön();
}

// Skriv ut totala lönen
Console.WriteLine($"Totala lönen är: {totalLön}");
```
```

### Konstruktor

En klass kan ha en startmetod som körs en gång i början, den har samma namn som klassen.\
Konstruktorn har inget returvärde:

```csharp
public class PersonInkomst
{
    public string namn;
    public int timlön;
    public int timmar;
    public DateTime registreratDatum;

    // Konstruktor
    public PersonInkomst(string namnIn, int timlönIn, int timmarIn)
    {
        namn = namnIn;
        timlön = timlönIn;
        timmar = timmarIn;
        
        // När den anställde registrerades
        registreratDatum = DateTime.Now;
    }
    
    // Beräkna lön
    public int BeräknaLön()
    {
        return timlön * timmar;
    }
}
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class PersonInkomst {
    + namn : string
    + timlön : int
    + timmar : int
    + registreratDatum : DateTime
    + PersonInkomst(namnIn : string, timlönIn : int, timmarIn : int)
    + BeräknaLön() : int
}
@enduml
```

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#FFF',
      'primaryBorderColor': '#000'
    }
  }
}%%
classDiagram
    class PersonInkomst {
        + namn : string
        + timlön : int
        + timmar : int
        + registreratDatum : DateTime
        + PersonInkomst(namnIn : string, timlönIn : int, timmarIn : int)
        + BeräknaLön() : int
    }
```

#### Program.cs

Nu blir huvudkoden:

```csharp
// Lista med anställda
List<PersonInkomst> listaAnställda = new List<PersonInkomst>();

while (true)
{
    // Läs in data från användaren
    Console.Write("Ange namn: ");
    string namn = Console.ReadLine();
    Console.Write("Ange timlön: ");
    int timlön = int.Parse(Console.ReadLine());
    Console.Write("Ange antal timmar: ");
    int timmar = int.Parse(Console.ReadLine());
    PersonInkomst person = new PersonInkomst(namn, timlön, timmar);

    // Lägg till ansälld i listan
    listaAnställda.Add(person);

    // Vill du lägga till fler?
    Console.WriteLine("Vill du lägga till fler? (j/n)");
    if (Console.ReadLine().ToLower() == "n")
    {
        break;
    }
}
```
