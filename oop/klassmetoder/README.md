---
description: Registrera boklån
---

# Klassmetoder

## Projekt Bibliotek

Vi skapar ett nytt konsolprojekt som heter **Bibliotek**.

![](/.gitbook/assets/oop/klassmetoder/image-110.png)

## Klassen Boklån

Vi skapar en klass för att registrera boklån. Klassen ska ha följande variabler:

* `bok` - en bok som lånas
* `låntagare` - en person som lånar
* `datum` - datum då boken lånades


```csharp
class Boklån
{
    public string bok;
    public string låntagare;
    public DateTime datum;
}
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Boklån {
    + bok : string
    + låntagare : string
    + datum : DateTime
}
@enduml
```

### Program.cs

I Main kan vi med en loop registrera flera boklån. Vi skapar ett objekt av klassen Boklån och läser in information om boklånet.  
Vi kan sedan skriva ut informationen om boklånet.

```csharp
Console.WriteLine("Registrera boklån!");

// Lista med boklån
List<Boklån> listaLån = new List<Boklån>();

// Programloop
while (true)
{
    Console.Write("Bok: ");
    string bok = Console.ReadLine();

    Console.Write("Låntagare: ");
    string låntagare = Console.ReadLine();

    // Skapa ett nytt lån
    Boklån lån = new Boklån();
    lån.bok = bok;
    lån.låntagare = låntagare;

    // Lägg till lånet i listan
    listaLån.Add(lån);

    // Igen?
    Console.Write("Gör ett nytt lån? (j/n): ");
    if (Console.ReadLine() != "j")
    {
        break;
    }
}

// Skriv ut alla boklån
Console.WriteLine("Alla boklån:");
foreach (boklån lån in listaBoklån)
{
    Console.WriteLine($"Bok: {lån.bok}");
    Console.WriteLine($"Låntagare: {lån.låntagare}");
    
    // När boken skall lämnas tillbaka, låneperioden är 30 dagar
    Console.WriteLine($"Lämna tillbaka: {lån.datum.AddDays(30)}");
}
```

## Metoder i klassen

Vi kan skapa metoder i klassen för att göra det lättare att registrera boklån.  

#### Metoden LämnasTillbaka()

Vi skapar en metod som returnerar datumet då boken skall lämnas tillbaka. Låneperioden är 30 dagar.

```csharp
class Boklån
{
    public string bok;
    public string låntagare;
    public DateTime datum;

    // När boken skall lämnas tillbaka
    // Låneperioden är 30 dagar
    public DateTime LämnasTillbaka()
    {
        return datum.AddDays(30);
    }
}
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Boklån {
    + bok : string
    + låntagare : string
    + datum : DateTime
    + LämnasTillbaka() : DateTime
}
@enduml
```

#### Metoden DagarKvar()

Vi skapar en metod som returnerar antalet dagar kvar till boken skall lämnas tillbaka.

```csharp
class Boklån
{
    public string bok;
    public string låntagare;
    public DateTime datum;

    // När boken skall lämnas tillbaka
    // Låneperioden är 30 dagar
    public DateTime LämnasTillbaka()
    {
        return datum.AddDays(30);
    }

    // Hur många dagar som är kvar
    public int DagarKvar()
    {
        return (int)(LämnasTillbaka() - DateTime.Now).TotalDays;
    }
}
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Boklån {
    + bok : string
    + låntagare : string
    + datum : DateTime
    + LämnasTillbaka() : DateTime
    + DagarKvar() : int
}
@enduml
```

### Program.cs

I huvudprogrammet kan vi använda metoderna för att skriva ut information om boklånet.  

```csharp
// Skriv ut alla boklån
Console.WriteLine("Alla boklån:");
foreach (boklån lån in listaBoklån)
{
    Console.WriteLine($"Bok: {lån.bok}");
    Console.WriteLine($"Låntagare: {lån.låntagare}");

    // När boken skall lämnas tillbaka
    // Låneperioden är 30 dagar
    Console.WriteLine($"Lämna tillbaka: {lån.LämnasTillbaka()}");

    // Hur många dagar som är kvar
    Console.WriteLine($"Dagar kvar: {lån.DagarKvar()}");
}
```

### Fler klassmetoder

Vi skapar även som skriver ut information om boklånet.  

#### Metoden SkrivUt()

```csharp
// Skriv ut information om boklånet
public void SkrivUt()
{
    Console.WriteLine($"Bok: {bok}");
    Console.WriteLine($"Låntagare: {låntagare}");
    Console.WriteLine($"Lånat: {datum}");
    Console.WriteLine($"Lämna tillbaka: {LämnasTillbaka()}");
    Console.WriteLine($"Dagar kvar: {DagarKvar()}");
}
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Boklån {
    + bok : string
    + låntagare : string
    + datum : DateTime
    + LämnasTillbaka() : DateTime
    + DagarKvar() : int
    + SkrivUt() : void
}
@enduml
```

#### Metoden FörlängLån()

```csharp
// Förläng lånet med 30 dagar
public void FörlängLån()
{
    datum = datum.AddDays(30);
}
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Boklån {
    + bok : string
    + låntagare : string
    + datum : DateTime
    + LämnasTillbaka() : DateTime
    + DagarKvar() : int
    + SkrivUt() : void
    + FörlängLån() : void
}
@enduml
```

## Konstruktor

Vi kan också skapa en metod som "körs" i början, en sorts "autostart-metod".\
I den metoden ska automatiskt registrera lånetidpunkten.

Vanligtvis skriver man konstruktorn i början klassen under variablerna:

```csharp
class Boklån
{
    public string bok;
    public string låntagare;
    public DateTime datum;

    // Konstruktor
    public Boklån(string bokIn, string låntagareIn)
    {
        bok = bokIn;
        låntagare = låntagareIn;
        datum = DateTime.Now;
    }

    ...
}
```

**OBS! Konstruktorn har inget returvärde.**

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#FFF',
      'primaryBorderColor': '#000'
    }
  }
}%%

classDiagram
    class Boklån {
        - bok : string
        - låntagare : string
        - datum : DateTime
        + Boklån(bokIn : string, låntagareIn : string) : void
        + LämnasTillbaka() : DateTime
        + DagarKvar() : int
        + SkrivUt() : void
        + FörlängLån() : void
    }
```

### Program.cs

Med konstruktorn blir Main som följer:

```csharp
// Lista med Boklån
List<Boklån> listaLån = new List<Boklån>();

// Programloop
while (true)
{
    Console.Write("Bok: ");
    string bok = Console.ReadLine();

    Console.Write("Låntagare: ");
    string låntagare = Console.ReadLine();

    // Skapa ett nytt Boklån
    Boklån Boklån = new Boklån(bok, låntagare);

    // Skriv ut information om boklånet
    Boklån.SkrivUt();

    // Lägg till boklånet i listan
    listaBoklån.Add(Boklån);

    // Igen?
    Console.Write("Gör ett nytt lån? (j/n): ");
    if (Console.ReadLine() != "j")
    {
        break;
    }
}
```
