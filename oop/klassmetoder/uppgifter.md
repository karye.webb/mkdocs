---
description: Träna på klassmetoder
---

# Uppgifter

## Uppgift 6

![](/.gitbook/assets/oop/klasser/image-85.png)

Bygg vidare på uppgift 4:

```csharp
class Elev
{
    public string förnamn;
    public string efternamn;
    public DateTime födelseDatum;
    public int längd;
    public double vikt;
}
```

* Lägg till en ny metod `Myndig()` av typen bool som returnerar `true` om personen är myndig (minst 18 år gammal).
* Avsluta med att skriva ihop ett exempel där du använder klassen och visar att metoden `Myndig()` fungerar.

För att räkna ut ålder kan du använda:

```csharp
int age = DateTime.Now.Year - birthday.Year
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Elev {
    + förnamn : string
    + efternamn : string
    + födelseDatum : DateTime
    + längd : int
    + vikt : double
    + Myndig() : bool
}
@enduml
```

## Uppgift 7

Bygg vidare på uppgift 6.

* Lägg till ytterligare en metod kallad `BMI()`.\
  Använd formeln för [BMI (Body Mass Index)](https://sv.wikipedia.org/wiki/Body\_Mass\_Index), BMI = vikten / (längden \* längden).\
  Vikt i kg och längd i meter.
* Utöka ditt exempel där du använder klassen och visar att metoden `BMI()` fungerar.  
  Skriv ut både vikten och längden för att se att det blir rätt.  
* Skapa en konstruktor som tar emot förnamn och efternamn och sätter födelsedatum till 1 januari 2000 och längd och vikt till 0.  
  Skapa ett nytt objekt med hjälp av konstruktorn och skriv ut det.

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Elev {
    + förnamn : string
    + efternamn : string
    + födelseDatum : DateTime
    + längd : int
    + vikt : double
    + Myndig() : bool
    + BMI() : double
    + Elev(förnamn : string, efternamn : string)
}
@enduml
```

## Uppgift 8

![](/.gitbook/assets/oop/klassmetoder/image-124.png)

Bygg vidare på uppgift 5 med klassen Bil:

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Bil {
    + märke : string
    + modell : string
    + färg : string
    + årsmodell : string
    + antalHästkrafter : int
}
@enduml
```

* Lägg till variablerna mätarställning (int), senastBesiktning (DateTime).
* Skapa en metod `Ålder()` som räknar ut åldern på bilen.
* Skapa en metod `DagarTillBesiktning()` som räknar ut antal dagar fram till nästa besiktning.  
  Om besiktningen redan har varit, returnera 0.
* Skapa en konstruktor som tar emot registreringsnummer och sätter mätarställning och senastBesiktning till 0.  
  Skapa ett nytt objekt med hjälp av konstruktorn och skriv ut det.  
* Avsluta med att skriva ihop ett exempel där du använder klassen och visar att metoderna fungerar.

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Bil {
    + märke : string
    + modell : string
    + färg : string
    + årsmodell : string
    + antalHästkrafter : int
    + registreringsnummer : string
    + mätarställning : int
    + senastBesiktning : DateTime
    + Ålder() : int
    + DagarTillBesiktning() : int
    + Bil(registreringsnummer : string)
}
@enduml
```

