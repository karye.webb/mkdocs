# Uppgifter

## Uppgift 14

![](/.gitbook/assets/oop/arv/image-108.png)

Hur ser klasserna ut till följande klassdiagram?

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class GameObject {
    + IsDead : bool «get» «set»
    + Radius : float «get» «set»
    + Rotation : float «get» «set»
    + Position : Vector2 «get» «set»
    + Speed : Vector2 «get» «set»
    + CollidesWith(other:IGameObject) : bool
}
class Meteor {
    + Type : MeteorType «get» «set»
    + ExplosionScale : float «get» «set»
    + Meter(type:MeteorType) : void
    + Update(gametime:GameTime) : void
    + BreakMeteor(meteor:Meteor) : IEnumerable<Meteor>
}
interface IGameObject {
}
class GameTime {
}
class MeteorType {
}
GameObject <|-- Meteor
@enduml
```

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#FFF',
      'primaryBorderColor': '#000'
    }
  }
}%%
classDiagram
    class GameObject {
        - IsDead : bool «get» «set»
        - Radius : float «get» «set»
        - Rotation : float «get» «set»
        - Position : Vector2 «get» «set»
        - Speed : Vector2 «get» «set»
        + CollidesWith(other:IGameObject) : bool
    }
    class Meteor {
        - Type : MeteorType «get» «set»
        - ExplosionScale : float «get» «set»
        + Meter(type:MeteorType) : void
        + Update(gametime:GameTime) : void
        + BreakMeteor(meteor:Meteor) : IEnumerable<Meteor>
    }
    GameObject <|-- Meteor
```

## Uppgift 15

![](/.gitbook/assets/oop/arv/image-109.png)

Skapa klasserna enligt UML-diagrammet.

Metoden `GetInfo()` ska returnera en beskrivning av den aktuella `Creature` (med Name, Health & Shield). 
Metoden `Attack()` i subklasserna kan skriva ut något i stil med : ”Knight {Name} attacks with {Weapon}” eller ”{Color} dragon {Name} attacks with fire”.

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Creature {
    + Health : int «get» «set»
    + Shield : int «get» «set»
    # Name : string «get» «set»
    + Creature(name:string)
    + GetInfo() : string
}
class Knight {
    # Weapon : string «get» «set»
    + Knight(name:string, weapon:string)
    + Attack() : void
}
class Dragon {
    # Color : string «get» «set»
    + Dragon(name:string, color:string)
    + Attack() : void
}
Creature <|-- Knight
Creature <|-- Dragon
@enduml
```

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#FFF',
      'primaryBorderColor': '#000'
    }
  }
}%%
classDiagram
    class Creature {
        - Health : int «get» «set»
        - Shield : int «get» «set»
        - Name : string «get» «set»
        + Creature(name:string)
        + GetInfo() : string
    }
    class Knight {
        - Weapon : string «get» «set»
        + Knight(name:string, weapon:string)
        + Attack() : void
    }
    class Dragon {
        - Color : string «get» «set»
        + Dragon(name:string, color:string)
        + Attack() : void
    }
    Creature <|-- Knight
    Creature <|-- Dragon
```

Skapa sedan ett program som visar att klasserna fungerar.

## Uppgift 16

### Egenskaper

Som tidigare skapar vi en klass för lite persondata:

```csharp
class Person
{
    // Två egenskaper
    private string Förnamn { get; set; }
    private string Efternamn { get; set; }
    
    // Konstruktorn
    public Person(string förnamn, string efternam)
    {
        Förnamn = förnamn;
        Efternamn = efternam;
    }
}
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Person {
    + Förnamn : string «get» «set»
    + Efternamn : string «get» «set»
    + Person(förnamn:string, efternamn:string)
}
@enduml
```

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#FFF',
      'primaryBorderColor': '#000'
    }
  }
}%%
classDiagram
    class Person {
        + Förnamn : string «get» «set»
        + Efternamn : string «get» «set»
        + Person(förnamn:string, efternamn:string)
    }
```

Detta är egentligen samma sak som följande långa kod:

```csharp
class Person
{
    // Två privata variabler
    private string förnamn;
    private string efternamn;

    // En metod för att ge tillgång data
    public string Förnamn
    { 
        get
        {
            return förnamn;
        }

        set
        {
            förnamn = value;
        }
    }
    
    // En metod för att ge tillgång data
    public string Efternamn
    { 
        get
        {
            return efternamn;
        }

        set
        {
            efternamn = value;
        }
    }
    
    // Konstruktorn
    public Person(string förnamn, string efternam)
    {
        Förnamn = förnamn;
        Efternamn = efternam;
    }
}
```

### Anpassa egenskaperna

Syftet är att lägga till kontroll av datan när den kommer in:

```csharp
class Person
{
    // Två privata variabler
    private string förnamn;
    private string efternamn;

    // En metod för att ge tillgång data
    private string Förnamn
    { 
        get
        {
            return förnamn;
        }

        set
        {
            // Börjar det med en versal
            if (!char.IsUpper(value[0]))
            {
                throw new ArgumentException("Förnamn måste börja med en versal");
            }
            // Är förnamnet minst 4 tecken långt
            else if (value.Length < 4)
            {
                throw new ArgumentException("Förnamn måste vara minst 4 tecken långt");
            }
            else
            {
                förnamn = value;
            }
        }
    }
    
    ...
```

* Gör samma koll på efternamnet.
