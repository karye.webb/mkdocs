---
description: Hur man återanvänder klasser
---

# Arv

## Enkelt arv

### Basklassen person

Vi utgår från en enkel klass:

```csharp
class Person
{
    public string Namn { get; set; }
    public string Mobil { get; set; }

    public void VisaInfo()
    {
        Console.WriteLine($"Namn: {Namn}, mobil: {Mobil}");
    }
}
```

Klassdiagrammet ser ut såhär:

```mermaid
classDiagram
    class Person {
        + Namn : «get» «set»
        + Mobil : string
        + VisaInfo() : void
    }
```

Som vanligt använder vi klassen `Person` såhär:

```csharp
static void Main(string[] args)
{
    Person person = new Person();
    Console.Write("Vad heter du? ");
    person.Namn = Console.ReadLine();
    Console.Write("Ange din mobil: ");
    person.Mobil = Console.ReadLine();
    person.VisaInfo();
}
```

### Klassen Student

Vi kan skapa en ny klass `Student` som baseras på klassen `Person`.

```csharp
class Student : Person
{
    public string Inskrivningsår { get; set; }
    public bool BetaldKåravgift { get; set; }

    public void VisaStudentInfo()
    {
        Console.WriteLine($"Namn: {Namn}, mobil: {Mobil}, inskrivningsår: {Inskrivningsår}");
        if (BetaldKåravgift)
        {
            Console.WriteLine("Kåravgiften är betald!");
        }
    }
}
```

Klassdiagrammet ser nu ut såhär:

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
    'primaryColor': '#FFF',
    'primaryBorderColor': '#888',
    }
  }
}%%
classDiagram
    class Person {
        + Namn : «get» «set»
        + Mobil : string
        + VisaInfo() : void
    }
    class Student {
        + Inskrivningsår : string
        + BetaldKåravgift : bool
        + VisaStudentInfo() : void
    }
    Person <|-- Student
```

`Student` har nu totalt 2 metoder: `VisaInfo()` och `VisaStudentInfo()`:

```csharp
static void Main(string[] args)
{
    // Vi skapar en elev
    Student elev = new Student();
    
    Console.Write("Vad heter du? ");
    elev.Namn = Console.ReadLine();
    Console.Write("Ange din mobil: ");
    elev.Mobil = Console.ReadLine();
    Console.Write("När skrevs du in? ");
    elev.Inskrivningsår = Console.ReadLine();
    Console.Write("Har du betalt kåravgiften? (j/n) ");
    if (Console.ReadLine().ToLower() == "j")
    {
        elev.BetaldKåravgift = true;
    }
    else
    {
        elev.BetaldKåravgift = false;
    }
    elev.VisaInfo();
    elev.VisaStudentInfo();
}
```

### Klassen Lärare

Vi använder klassen `Person` för att skapa ytterligare en variant klassen `Lärare`:

```csharp
class Lärare : Person
{
    public string Anställningsår { get; set; }
    public bool BetaldSkatt { get; set; }

    public void VisaLärarInfo()
    {
        Console.WriteLine($"Namn: {Namn}, mobil: {Mobil}, anställningsår: {Anställningsår}");
        if (BetaldSkatt)
        {
            Console.WriteLine("Skatten är betald!");
        }
    }
}
```

På samma sätt kan vi använda klassen `Student`:

```csharp
// En lärare
Lärare lärare = new Lärare();
Console.Write("Vad heter du? ");
lärare.Namn = Console.ReadLine();
Console.Write("Ange din mobil: ");
lärare.Mobil = Console.ReadLine();
Console.Write("När anställdes du? ");
lärare.Anställningsår = Console.ReadLine();
Console.Write("Har du betalt skatten? (j/n) ");
if (Console.ReadLine().ToLower() == "j")
{
    lärare.BetaldSkatt = true;
}
else
{
    lärare.BetaldSkatt = false;
}

lärare.VisaInfo();
```

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
        'primaryColor': '#FFF',
        'primaryBorderColor': '#888',
        'secondaryColor': '#888',
    }
  }
}%%
classDiagram
    class Person {
        + Namn : «get» «set»
        + Mobil : string
        + VisaInfo() : void
    }
    class Student {
        + Inskrivningsår : string
        + BetaldKåravgift : bool
        + VisaStudentInfo() : void
    }
    class Lärare {
        + Anställningsår : string
        + BetaldSkatt : bool
        + VisaLärarInfo() : void
    }
    Person <|-- Student
    Person <|-- Lärare
```

## Ärva metoder

Metoderna `VisaInfo()`, `VisaStudentInfo()` och `VisaLärarInfo()` gör i princip samma sak.\
Vi behöver inte 3 olika metoder om vi gör `VisaInfo()` virtuell med `virtual`:

```csharp
class Person
{
    public string Namn { get; set; }
    public string Mobil { get; set; }

    public virtual void VisaInfo()
    {
        Console.WriteLine($"Namn: {Namn}, mobil: {Mobil}");
    }
}
```

Och i de 2 ärvda klasserna skriver vi `override`:

```csharp
class Student : Person
{
    public string Inskrivningsår { get; set; }
    public bool BetaldKåravgift { get; set; }

    public override void VisaInfo()
    {
        Console.WriteLine($"Namn: {Namn}, mobil: {Mobil}, inskrivningsår: {Inskrivningsår}");
        if (BetaldKåravgift)
        {
            Console.WriteLine("Kåravgiften är betald!");
        }
    }
}

class Lärare : Person
{
    public string Anställningsår { get; set; }
    public bool BetaldSkatt { get; set; }

    public override void VisaInfo()
    {
        Console.WriteLine($"Namn: {Namn}, mobil: {Mobil}, anställningsår: {Anställningsår}");
        if (BetaldSkatt)
        {
            Console.WriteLine("Skatten är betald!");
        }
    }
}
```

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
        'primaryColor': '#FFF',
        'primaryBorderColor': '#888',
        'secondaryColor': '#888',
    }
  }
}%%
classDiagram
    class Person {
        + Namn : «get» «set»
        + Mobil : string
        + VisaInfo() : void
    }
    class Student {
        + Inskrivningsår : string
        + BetaldKåravgift : bool
        + VisaStudentInfo() : void
    }
    class Lärare {
        + Anställningsår : string
        + BetaldSkatt : bool
        + VisaLärarInfo() : void
    }
    Person <|-- Student
    Person <|-- Lärare
```

I tillämpningen använder vi bara `VisaInfo()`:

```csharp
class Program
{
    static void Main(string[] args)
    {
        // En student
        Student elev = new Student();
        Console.Write("Vad heter du? ");
        elev.Namn = Console.ReadLine();
        Console.Write("Ange din mobil: ");
        elev.Mobil = Console.ReadLine();
        Console.Write("När skrevs du in? ");
        elev.Inskrivningsår = Console.ReadLine();
        Console.Write("Har du betalt kåravgiften? (j/n) ");
        if (Console.ReadLine().ToLower() == "j")
        {
            elev.BetaldKåravgift = true;
        }
        else
        {
            elev.BetaldKåravgift = false;
        }
        elev.VisaInfo();

        // En lärare
        Lärare lärare = new Lärare();
        Console.Write("Vad heter du? ");
        lärare.Namn = Console.ReadLine();
        Console.Write("Ange din mobil: ");
        lärare.Mobil = Console.ReadLine();
        Console.Write("När anställdes du? ");
        lärare.Anställningsår = Console.ReadLine();
        Console.Write("Har du betalt skatten? (j/n) ");
        if (Console.ReadLine().ToLower() == "j")
        {
            lärare.BetaldSkatt = true;
        }
        else
        {
            lärare.BetaldSkatt = false;
        }
        lärare.VisaInfo();
    }
}
```

## Ärva Konstruktor

För att underlätta skapar vi en konstruktor i basklassen `Person`:

```csharp
class Person
{
    private string Namn { get; set; }
    private string Mobil { get; set; }
    
    public Person(string n, string m)
    {
        Namn = n;
        Mobil = m;
    }

    public virtual void VisaInfo()
    {
        Console.WriteLine($"Namn: {Namn}, mobil: {Mobil}");
    }
}
```

Nu ärver vi basklassens konsktruktor med `base():`

```csharp
class Student : Person
{
    private string Inskrivningsår { get; set; }
    private bool BetaldKåravgift { get; set; }

    public Student(string i, bool b, string n, string m) : base(n, m)
    {
        Inskrivningsår = i;
        BetaldKåravgift = b;
    }

    public override void VisaInfo()
    {
        Console.WriteLine($"Namn: {Namn}, mobil: {Mobil}, inskrivningsår: {Inskrivningsår}");
        if (BetaldKåravgift)
        {
            Console.WriteLine("Kåravgiften är betald!");
        }
    }
}

class Lärare : Person
{
    private string Anställningsår { get; set; }
    private bool BetaldSkatt { get; set; }

    public Lärare(string a, bool b, string n, string m) : base(n, m)
    {
        Anställningsår = a;
        BetaldSkatt = b;
    }

    public override void VisaInfo()
    {
        Console.WriteLine($"Namn: {Namn}, mobil: {Mobil}, anställningsår: {Anställningsår}");
        if (BetaldSkatt)
        {
            Console.WriteLine("Skatten är betald!");
        }
    }
}
```

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
        'primaryColor': '#FFF',
        'primaryBorderColor': '#888',
        'secondaryColor': '#888',
    }
  }
}%%
classDiagram
    class Person {
        + Namn : «get» «set»
        + Mobil : string
        + VisaInfo() : void
    }
    class Student {
        + Inskrivningsår : string
        + BetaldKåravgift : bool
        + VisaStudentInfo() : void
    }
    class Lärare {
        + Anställningsår : string
        + BetaldSkatt : bool
        + VisaLärarInfo() : void
    }
    Person <|-- Student
    Person <|-- Lärare
```

## Videogenomgång

{% embed url="https://youtu.be/4sxyDXt1igs" %}
