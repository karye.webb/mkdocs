# Uppgifter

## Uppgift 9

![](/.gitbook/assets/oop/inkapsling/image-77.png)

Skapa en klass som beskriver en `Bok`. Du ska ha med följande egenskaper: `Titel`, `Författare`, `AntalSidor`. Klassen ska också ha en metod `SkrivUt` som skriver ut bokens info.\
Skriv ett program som använder klassen `Bok` och demonstrerar att den fungerar.

![](/.gitbook/assets/oop/inkapsling/image-73.png)

## Uppgift 10

Skydda alla egenskaperna; `Titel`, `Författare` och `AntalSidor` så att det ej går att ändra dessa efter det att ett objekt av typen `Bok` har skapats.

![](/.gitbook/assets/oop/inkapsling/image-74.png)

## Uppgift 11

Vad är skillnaden mellan private och protected? Formulera din egen förklaring eller förklara för en kompis.

## Uppgift 12

![](/.gitbook/assets/oop/inkapsling/image-78.png)

Skapa en klass `Tärning` som ska fungera som en tärning. Den ska ha endast en konstruktor som tar hur många sidor tärningen ska ha. Du ska sedan kunna anropa en metod `Kasta()` som ska returnera ett slumpmässigt värde mellan 1 och antalet sidor på tärningen.

Skriv klassen samt ett kort program som visar att klassen fungerar som den ska. Gör därefter ett UML-diagram över klassen `Tärning`.

![](/.gitbook/assets/oop/inkapsling/image-75.png)

## Uppgift 13

![](/.gitbook/assets/oop/inkapsling/image-79.png)

Skapa ett Black Jack-spel.

#### Bakgrund

Black Jack är ett kortspel som en, eller flera, spelare spelar mot en ”dealer” (bank). Målet är att få 21. Kort delas ut till alla spelare, inklusive ”dealern”. Korten har värden 1-10 där alla klädda kort är värda 10, Ess är värda 1 eller 11.

Två kort delas ut till varje spelare. Dealern får 1 öppet kort och ett gömt kort. Sedan har spelaren möjlighet att dra kort till dess att a) spelaren är nöjd eller b) spelaren hamnar över 21 och förlorar automatiskt.

Eftersom spelaren får två kort så är det möjligt att nå 21 (ess + 10), även kallat Black Jack, direkt, varpå spelaren vinner.

Reglerna variera mellan olika länder och ibland mellan olika kasinon. Uppgiften kommer att bli att skapa ett spel för **en** spelare och **utan** hantering av satsningar.

![](/.gitbook/assets/oop/inkapsling/image-76.png)

Notera att `GameStatus` och `SuitType` är av typen [enum](https://csharp.progdocs.se/csharp-ref/grundlaeggande/datatyper/enum):

```csharp
// Enums
public enum GameStatus
{
    Won, Lost, Playing, Tie, BlackJack
}
public enum SuitType
{
    Club, Diamond, Heart, Spade
}
```

#### Regler

* Spelaren vinner alltid vid 21.
* Dealern måste alltid räkna Ess som 11.
* Dealern måste alltid dra under 16.
* Dealern måste stanna på 17 eller över.
* Om spelaren och dealern båda har 17, 18, eller 19 så vinner ändå dealern.
* Om spelaren och dealern och spelaren båda har 20 så blir det lika.

Det finns några regler till som t.ex. att man kan dela (”split”) vissa händer. Vi kommer att bortse från dessa regler i denna uppgift.

#### Klasserna

`Card`, beskriver ett spelkort. Den har ett värde (Value; 1-13) samt en färg (Suit; Klöver, Ruter, Hjärter, Spader). Kortet ska också kunna ge en beskrivning av sig själv som sträng via metoden `ToString()`. `BlackJackValue` ger det lägsta möjliga Black Jack-värdet på ett kort, dvs 1 för Ess.

`Deck`, representerar en eller flera kortlekar. När man spelar Black Jack så används alltid minst 4 kortlekar då det annars är väldigt enkelt att räkna kort och på så sätt få ett övertag på dealern. När en kortlek skapas så ska den vara blandad (`Shuffle()`) och redo att användas. Man ska kunna dra kort via metoden `Draw()`.

`Player`, beskriver en spelare. Det kan faktiskt också beskriva en dealer. Spelaren har en `Hand` som håller de kort som spelaren har. Egenskapen `LowValue` ger alltid det lägsta Black Jack-värdet. `HighValue` ger det högre värdet om man håller ett ess. `BestValue` ger det mest förmånliga värdet, t.ex. 16 om man håller ess + 5:a eller 18 om man håller Kung + 6:a + Ess + Ess. Spelaren ska också kunna ge sin totala hand-beskrivning som sträng via `ToString()`.

`Game`, beskriver logiken i själva spelet. Spelet har **en** spelare och en dealer. Spelet har också en kortlek (Deck) samt en status (Status) som säger om vi spelar (Playing) eller spelaren har förlorat (Lost), Vunnit (Won), fått BlackJack eller om det blivit lika (Tie).

Spelet har bara två metoder. `DealerDraw()` eller `PlayerDraw()`. Efter varje anrop till respektive metod så ska Status i spelet vara uppdaterad.

#### Implementation

Skriv spelet som ett Console Application. Programmet som använder ovan beskrivna klasser måste sköta in/utmatning samt hålla reda på vems tur det är. Spelaren börjar alltid med att dra tills dess att spelaren ev. förlorar. Så fort spelaren stannar så börjar dealern dra.

Det gömda kort som dealern vanligtvis tilldelas i början kan man istället dra först när spelaren är klar. Sedan kan man göra det lite mer spännande genom att lägga till en paus på ca. 2000ms mellan varje drag som dealern gör. Detta kan man göra med `Thread.Sleep()`.
