---
description: Utnyttja att klasser är liknande egenskaper
---

# Polymorfism

## Statisk bindning

### En basklass

En enkel subklass med 2 metoder:

```csharp
using System;

namespace HelloWorld
{
    public class Animal
    {
        public void Eat()
        {
        }

        public void Sleep()
        {
        }
    }
}
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Animal {
    +Eat() : void
    +Sleep() : void
}
@enduml
```

### Två subklasser

Vi skapar 2 nya klasser som ärver från den första:

```csharp
using System;

namespace HelloWorld
{
    public class Animal
    {
        public void Eat()
        {
        }

        public void Sleep()
        {
        }
    }
    
    public class Cat : Animal
    {
    }
    
    public class Dog : Animal
    {
    }
}
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Animal {
    +Eat() : void
    +Sleep() : void
}
class Cat {
}
class Dog {
}
Animal <|-- Cat
Animal <|-- Dog
@enduml
```

### Utan polymorphism

Nu skall vi använda våra klasser:

```csharp
public class Program
{
    // create two arrays, one of type Dog, one of type Cat, to keep track of both animal types
    private static Cat[] cats = new Cat[2];
    private static Dog[] dogs = new Dog[2];

    public static void Main(string[] args)
    {
        // add some cats
        cats[0] = new Cat();
        cats[1] = new Cat();

        // add some dogs
        dogs[0] = new Dog();
        dogs[1] = new Dog();

        // do something with all the cats
        foreach (Cat cat in cats)
            cat.Eat();

        // do something with all the dogs
        foreach (Dog dog in dogs)
            dog.Sleep();
    }
}
```

### Med Polymorphism

Koden blir enklare om vi använder polymorphism. Vi slipper dubblering av raderna:

```csharp
public class Program
{
    // create a single array of type Animal
    private static Animal[] animals = new Animal[2];

    public static void Main(string[] args)
    {
        // add some different animals
        animals[0] = new Cat();
        animals[1] = new Dog();

        // do something with all the animals
        foreach (Animal animal in animals)
            animal.Eat();
    }
}
```

Däremot vad är felen på denna kod?

```csharp
public class Program
{
    // create a single array of type Animal
    private static Animal[] animals = new Animal[2];

    public static void Main(string[] args)
    {
        // add some different animals
        animals[0] = new Cat();
        animals[1] = new Dog();
       
        // do something with all the animals of type Cat
        foreach (Cat cat in animals)
            cat.Eat();
    }
}
```

## Dynamisk bindning

```csharp
class Player
{
    public virtual void Attack()
    {
        Console.WriteLine("A Player attacks!");
    }
}

class Mage : Player
{
    public override void Attack()
    {
        Console.WriteLine("A Mage attacks with a fireball!");
    }    
}

class Warrior : Player
{
    public override void Attack()
    {
        Console.WriteLine("A Warrior attacks with a sword!");
    }
}
```


```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Player {
    +Attack() : void
}
class Mage {
    +Attack() : void
}
class Warrior {
    +Attack() : void
}
Player <|-- Mage
Player <|-- Warrior
@enduml
```

### Polymorfism

Sedan skapar vi ett program som använder en list<> för att lagra instanserna:

```csharp
class Program
{
    static void Main(string[] args)
    {
        List<Player> players = new List<Player>();

        players.Add(new Player());
        players.Add(new Mage());
        players.Add(new Warrior());

        foreach (Player p in players)
        {
            p.Attack();
        }
    }
}
```

## Mer information

* Läs mer här [https://www.rusoaica.com/object-oriented-programming/polymorphism](https://www.rusoaica.com/object-oriented-programming/polymorphism/)
