---
description: Bra resurser för att träna C#-programmering på egen hand
---
# Metoder

## Material

### Allmänna bra länkar

* [Metoder](https://csharp.progdocs.se/grundlaeggande/egna-metoder)

## Övningar

Skapa ett ny mapp **RepMetoder**.

### 1. Metoder som skriver ut text

* Skapa ett nytt konsolprojekt **MetoderText**.
* Skapa en metod som heter `SkrivHej()` som inte tar några parametrar. Metoden ska skriva ut "Hej!".
* Skapa en metod som heter `SkrivHejMedNamn()` som tar en `string`-parameter som heter `namn`. Metoden ska skriva ut "Hej {namn}!" 
* Skapa en metod som heter `SkrivHejMedNamnOchAlder()` som tar en `string`-parameter som heter `namn` och en `int`-parameter som heter `ålder`. 
  * Metoden ska skriva ut "Hej {namn}! Du är ålder år gammal."
  * Lägg till en ´if´-sats i metoden `SkrivHejMedNamnOchAlder()` som skriver ut "Du är myndig!" om `ålder` är större än 18.
* Skapa en metod `SkivUtMånad()` som slumpar fram ett tal mellan 1 och 100 och skriver ut.
  * Skapa en metod som tar en `int`-parameter som heter månad.
  * I metoden skapa en array med 12 strängar som innehåller månadernas namn.
  * Använd `int`-parametern som index i arrayen och skriv ut månadens namn.

### 2. Metoder som returnerar värden

* Skapa ett nytt konsolprojekt **MetoderRetur**.
* Skapa en ny metod som heter `Summera()` och som tar två int-parametrar som heter tal1 och tal2.
  * Inuti metoden, skapa en variabel som heter summa och tilldela den summan av tal1 och tal2 genom att använda den aritmetiska operatorn +.
  * Returnera sedan summan genom att använda `return`-satsen och ange summa som värdet som ska returneras.
* Skapa en metod som heter `BeräknaCircleArea()` som tar en `double`-parameter som heter radius och returnerar arean av en cirkel med den angivna radien.
  * Returnera Math.PI * radius * radius.
* Skapa en metod som heter `BeräknaTriangelArea()` som tar två `double`-parameter som heter bas och höjd och returnerar arean av en triangel med den angivna basen och höjden.
  * Returnera bas * höjd / 2.
* Skapa en metod som heter `HeltalsDivision()` som tar två `int`-parameter som heter täljare och nämnare.
  * Returnera täljare / nämnare.
  * Lägg till en `if`-sats som kontrollerar att nämnare inte är 0 och skriver ut ett felmeddelande om det är fallet.
* Skapa en metod som heter `RäknaBokstäver()` som tar en `string`-parameter som heter text och returnerar antalet bokstäver i texten (exklusive mellanslag).
  * Om parametern är tom ("") ska metoden returnera 0.
  * Om parametern innehåller mer än 100 tecken ska metoden returnera -1.
* Skapa en metod som heter `ÄrTaletPrimtal()` som tar en `int`-parameter som heter tal och returnerar `true` om talet är ett primtal, annars returnerar den `false`.
  * Skapa en loop som går igenom alla tal mellan 2 och parametern.
  * Använd `if`-satsen och använd modulus-operatorn (%) för att kontrollera om talet är jämnt delbart med något tal mellan 2 och talet. Dvs om `talet %% i == 0`.

### 3. Metoder som tar Listor som parametrar

* Skapa ett nytt konsolprojekt **MetoderListor**.
* Skapa en metod som heter `SummeraLista()` som tar en `List<int>`-parameter som heter lista. Metoden ska returnera summan av alla tal i listan.
  * Skapa en foreach-loop som går igenom alla tal i listan.
  * Lägg till varje tal i listan till en variabel som heter summa.
  * Returnera summan.
* Skapa en metod som heter `Bilar()` som inte tar några parametrar. Metoden ska returnera en `List<string>` med namn på fyra bilar: "Volvo", "Ford", "BMW" och "Mazda".
* Skapa en metod som heter `HittaStörstaTalet()` som tar en `List<int>`-parameter som heter talen. Metoden ska returnera det största talet i listan.
  * Lägg till en `if`-sats i metoden `HittaStörstaTalet()` som kollar om listan är tom. Om så är fallet ska metoden returnera 0.