---
description: Bra resurser för att träna C#-programmering på egen hand
---
# Generiska klasser - List och Dictionary

## Allmänna bra länkar
* [C# referens: Listor & arrayer](https://csharp.progdocs.se/grundlaeggande/listor-och-arrayer)
* [C# referens: Dictionary](https://csharp.progdocs.se/klasser-och-objektorientering/generiska-klasser#dictionary)

## Övningar

Skapa en ny mapp **RepListDictionary**.

### 1. Array och loopar

* Skapa ett nytt konsolprojekt **ArrayLoopar**.
* Skapa en `string`-array med namnen på fem olika leksaker.
* Skriv en `for`-loop (eller `foreach`-loop) som skriver ut namnet på varje leksak på en egen rad.
* Skapa en `string`-`array` med namnen på fem av dina klasskamrater.
* Skapa en `int`-array med fem värden mellan 0 och 10.
* Komplettera for-loopen så att den på varje rad skriver ut något liknande "Pelle ger Barbie betyget 5".  
Använd dina klasskamraters namn, leksakernas namn och värdena från `int`-arrayen.
* Skapa en tom `string`-lista som heter "cities".
* Skapa en loop där du varje gång loopen körs hämtar en `string` från användaren med `Console.ReadLine()`.  
Lägg till den `string` du får från användaren till cities-listan. Avbryt loopen om användaren skriver "exit".

### 2. Listor och loopar

* Skapa ett nytt konsolprojekt **ListLoopar**.
* Skapa en tom `int`-lista som heter `numbers`.
* Skapa en loop där du varje gång loopen körs hämtar en `int` från användaren med `Console.ReadLine()`.  
Lägg till den `int` du får från användaren till numbers-listan. Avbryt loopen om användaren skriver "exit".
* Skapa en loop som ligger efter den ovanstående. Den här loopen ska skriva ut alla `int` som finns sparade i numbers-listan.

### 3. Listor, programloopar och meny

* Skapa ett nytt konsolprojekt **ListLooparMeny**.
* Skapa en tom `string`-lista som heter `names`.
* Skapa en meny som skriver ut en meny med följande val:  

```shell
1. Add name  
2. List names  
3. Exit  
Choose:   
```

* Om användaren väljer "1" så ska användaren få skriva in ett namn. Namnet ska läggas till i `names`-listan.
* Om användaren väljer "2" så ska alla namn som finns i `names`-listan skrivas ut.
* Om användaren väljer "3" så ska loopen avbrytas.
* Om användaren väljer något annat så ska menyn skrivas ut igen.

### 4. Dictionary, programloop och meny - enkelt

* Skapa ett nytt konsolprojekt **DictionaryLoopMeny**.
* Skapa en dictionary som heter `cities` och som innehåller följande key-value-par:  

```text
"Stockholm" - "Sverige"  
"Paris" - "Frankrike"  
"Berlin" - "Tyskland"  
"London" - "Storbritannien"  
"Madrid" - "Spanien"  
```

Så här kan du skapa en dictionary:
```csharp
// Skapa en dictionary som heter "cities" med key-value-par
var cities = new Dictionary<string, string>();

// Lägg till key-value-par i dictionaryn
cities.Add("Stockholm", "Sverige");
```

* Skapa en meny som skriver ut en meny med följande val:  

```shell
1. Add city  
2. List cities  
3. Exit  
Choose:   
```

* Om användaren väljer "1" så ska användaren få skriva in en stad.  
Sedan ska användaren få skriva in vilket land staden ligger i. Staden och landet ska läggas till i cities-dictionaryn. 
* Om användaren väljer "2" så ska alla städer och länder som finns i `cities`-dictionaryn skrivas ut.
* Om användaren väljer "3" så ska loopen avbrytas.
* Om användaren väljer något annat så ska menyn skrivas ut igen.

### 5. Dictionary, programloop och meny - spel

* Skapa ett nytt konsolprojekt **DictionaryLoopMenySpel**.
* Skapa en dictionary som heter `games` och som innehåller följande key-value-pairs:  
  
```text
"Super Mario Bros" - "1985"  
"The Legend of Zelda" - "1986"  
"Tetris" - "1984" 
"Super Mario Bros 3" - "1988"
"Super Mario World" - "1990"
"Super Mario 64" - "1996"
``` 

Så här kan du skapa en dictionary:

```csharp
// Skapa en dictionary som heter "games" med key-value-par
Dictionary<string, string> games = {
    {"Super Mario Bros", "1985"},
    {"The Legend of Zelda", "1986"},
    {"Tetris", "1984"}
};
```

* Skapa en meny som skriver ut en meny med följande val: 

```shell
1. Add game  
2. List games  
3. Find game  
4. Exit  
Choose:   
```

* Om användaren väljer "1" så ska användaren få skriva in ett spel.  
Sedan ska användaren få skriva in vilket år spelet släpptes. Spelet och året ska läggas till i `games`-dictionaryn.
* Om användaren väljer "2" så ska alla spel och årtal som finns i `games`-dictionaryn skrivas ut.
* Om användaren väljer "3" så ska användaren få skriva in ett spel.  
Om spelet finns i `games`-dictionaryn så ska året spelet släpptes skrivas ut.  
Om spelet inte finns i `games`-dictionaryn så ska användaren få skriva in ett nytt spel.

Så här kan du kolla om ett värde finns i en dictionary:

```csharp
// Kolla om ett värde finns i en dictionary
if (games.ContainsKey("Super Mario Bros"))
{
    Console.WriteLine("Super Mario Bros finns i dictionaryn");
}
```

* Om användaren väljer "4" så ska loopen avbrytas.
* Om användaren väljer något annat så ska menyn skrivas ut igen.

### 6. Dictionary, programloop och meny - filmer 

* Skapa ett nytt konsolprojekt **DictionaryLoopMenyFilmer**.
* Skapa en dictionary som heter `movies` och som innehåller följande key-value-pairs:  
    
```text
"The Shawshank Redemption" - "1994"  
"The Godfather" - "1972"  
"The Godfather: Part II" - "1974"  
"The Dark Knight" - "2008"  
"The Good, the Bad and the Ugly" - "1966"  
"Pulp Fiction" - "1994"  
"Schindler's List" - "1993"
```

* Skapa en meny som skriver ut en meny med följande val:  

```shell
1. Add movie  
2. List movies  
3. Find movie  
4. Exit  
Choose:   
```

* Om användaren väljer "1" så ska användaren få skriva in en film. Sedan ska användaren få skriva in vilket år filmen släpptes. Filmen och året ska läggas till i movies-dictionaryn.
* Om användaren väljer "2" så ska alla filmer och årtal som finns i movies-dictionaryn skrivas ut.
* Om användaren väljer "3" så ska användaren få skriva in en film. Om filmen finns i movies-dictionaryn så ska året filmen släpptes skrivas ut. Om filmen inte finns i movies-dictionaryn så ska användaren få skriva in en ny film.
* Om användaren väljer "4" så ska loopen avbrytas.
* Om användaren väljer något annat så ska menyn skrivas ut igen.