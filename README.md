---
description: Innehåll i kursen programmering 2
---

# Kursinnehåll

## Vad är OOP?

{% embed url="https://youtu.be/1ONhXmQuWP8" %}

## Centralt innehåll

### Undervisningen i kursen ska behandla följande centrala innehåll

* Grunderna för **klasserna, objekt, egenskaper och metoder**.
* **Arv, inkapsling** och polymorfism.
* **Skapande av klasser och objekt** i ett objektorienterat programspråk utifrån tidigare analys och design.
* **Användning av klasser** och att genom **arv** förändra beteende hos klasser som ingår i egna och andras klasshierarkier och standardbibliotek.
* **Generiska klasser** och metoder.
* Variablers och metoders **synlighet och livslängd**.
* Stark och svag samt statisk och dynamisk typning.
* Identifierares **synlighet** och livslängd.
* Det valda programspråkets **kontrollstrukturer**.
* **Undantagshantering**.
* Analys, nedbrytning och modellering av programmeringstekniska problem med lämpligt analysverktyg, till exempel användningsfall.
* Design av lämplig lösning ur föregående analys med lämpligt verktyg och metoder som klassdiagram.
* Skapande av **användarvänliga gränssnitt**.
* Skrivning och läsning av **lagrad** data.
* Utveckling av program som nyttjar **kommunikation över internet**.

## Konkretisering

### Praktik

Eleven ska kunna skapa ett program:

* [ ] Som använder klasser och objekt
* [ ] Som klasser med arv
* [ ] Som använder klasser med korrekt inkapsling
* [ ] Som läser och skriver till fil
* [ ] Som läser data från internet
* [ ] Som använder polymorfism
* [ ] Som använder generiska klasser som `List<>` och/eller `Dictionary<>`
* [ ] Som använder undantagshantering
* [ ] Med användarvänlig gränssnitt
* [ ] Med korrekt kodstruktur, variabelnamn enligt konventioner
* [ ] Med tydlig och korrekt kommentering

### Teori

Eleven ska kunna förklara:

* [ ] Vad är OOP?
* [ ] Vad är klasser och objekt?
* [ ] Vad är arv?
* [ ] Vad är inkapsling?
* [ ] Vad är polymorfism?
* [ ] Skapa klassdiagram
* [ ] Skapa kod utifrån klassdiagram

## Bra länkar

* [https://csharp.progdocs.se/csharp-ref/](https://csharp.progdocs.se/csharp-ref/)
