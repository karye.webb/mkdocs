#!/usr/bin/env python3
# To do bulk deletions, you can use the pipelines API to programmatically list and delete pipelines.
# In Python (with the python-gitlab library)

import gitlab

# User token
token = 'glpat-Z-ax1W9FPxb5h3qtHAA8'

# Connect to GitLab
gl = gitlab.Gitlab('https://gitlab.com', private_token=token)
gl.auth()

# Get user info
user = gl.user
print(user.username + ' (' + user.name + ')')

# Get users all projects
projects = gl.projects.list(owned=True, get_all=True)

# Loop through the projects
for project in projects:
    # Print the project ID and name
    print(project.id, project.name)

    # Get all pipelines for a project
    pipelines = gl.projects.get(project.id).pipelines.list(get_all=True)

    # Calculate the number of pipelines - 5, but minimum 0
    number_of_pipelines = max(len(pipelines) - 5, 0)

    # Wait for j for confirmation
    input('Press j to delete ' + str(number_of_pipelines) + ' pipelines: ')

    if input == 'j':
        pass

    # Loop through the pipelines and delete them (except the first 5)
    for pipeline in pipelines[5:]:
        # Print the pipeline ID and status
        print(pipeline.id, pipeline.status)
        # Delete the pipeline
        pipeline.delete()