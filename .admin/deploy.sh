#!/bin/bash
# Körs i rotkatalogen utanför alla kurswebbar
# Uppdaterar alla skript, css, gitbook2mkdocs.py, base.yml

for i in graegrae prrprr01-js prrprr01-te prrprr02-te tillprr wesweb01 wesweb02 weuweb01 weuweb02
do
    echo "$i:"
    rm -r $i/scripts $i/style
    rsync -r mkdocs/scripts $i
    echo "Merged changes in $i/scripts"
    rsync -r mkdocs/style $i
    echo "Merged changes in $i/style"
    rsync -r mkdocs/gitbook2mkdocs.py $i
    echo "Updated $i/gitbook2mkdocs.py"
    rsync -r mkdocs/base.yml $i
    cat $i/head.yml > $i/mkdocs.yml
    cat $i/base.yml >> $i/mkdocs.yml
    echo -e "Updated $i/mkdocs.yml"
    rsync -r mkdocs/.gitlab-ci.yml $i
    echo "Updated $i/.gitlab-ci.yml"
    echo ""
done
