#!/bin/bash

# This script is used to publish the site to the web server.

# Run the python script to generate the site
python3 .admin/gitbook2mkdocs.py

# Run mkdocs to build the static site
mkdocs build -d /var/www/mkdocs.smutje.se

# Remove the .git directory from the docs folder
rm -rf docs/.git