#!/usr/bin/python3

import sys
import glob
import os
import shutil
import re
import json
import urllib.parse
from nav import NavGenerator

class GitbookToMkDocs:

    def __init__(self, private=False):
        """
        Initialize the transformation process from Gitbook to MkDocs.
        """
        self.private = private
        self.doc_root = "docs/"
        self.images_source = ".gitbook/assets/"
        self.images_dest = "images/"
        self.file_assets = ".admin/assets.json"
        self.assets_dict = {}
        # Regex patterns
        self.gb_hint_rg = r'{% hint style=\"(.*)\" %}(.*|[\s\S]+?){% endhint %}'
        self.gb_tab_rg = r'{% tab title=\"(.*)\" %}(.*|[\s\S]+?){% endtab %}'
        self.gb_embedY_rg = r'{% embed url=\"https://w*\.*youtu.*/(.*)\" %}'
        self.gb_embed_rg = r'{% embed url=\"(.*)\" %}'
        self.gb_file1_rg = r'{% file src=\"(.*)\" %}\n(.*|[\s\S]+?)\n{% endfile %}'
        self.gb_file2_rg = r'{% file src=\"(.*)\" %}'
        self.gb_img_rg = r'\!\[.*\]\(<?(.*)/(.*)>?\)'
        self.gb_figure_rg = r'<figure><img src=\"(.*)/(.*)\" alt=\"\"><figcaption></figcaption></figure>'
        self.nav_generator = NavGenerator(private)

    # Replace gitbook hint and file extensions to mkdocs-compatible format. e.g.
    def hint_group(self, groups):
        """
        Format for the 'Hint' content block for MkDocs.
        """
        hint_type = groups.group(1)
        hint_content = groups.group(2)
        hint_content = hint_content.replace("\n", "\n\t")
        return "!!! " + hint_type + "\n" + hint_content

    # Replace gitbook tab and file extensions to mkdocs-compatible format. e.g.
    def tab_group(self, groups):
        tab_type = groups.group(1)
        tab_content = groups.group(2)
        tab_content = tab_content.replace("\n", "\n\t")
        return f"=== \"{tab_type}\"\n{tab_content}"

    # Replace gitbook embedY extensions to mkdocs-compatible format. e.g.
    def embedY_group(self, groups):
        embedY_url = groups.group(1)
        return f"<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/{embedY_url}\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"

    # Replace gitbook embed extensions to mkdocs-compatible format. e.g.
    def embed_group(self, groups):
        embed_url = groups.group(1)
        return f"<div class=\"embed\"><i class=\"fas fa-link\"></i><a href=\"{embed_url}\">{embed_url}</a></div>"

    # Replace gitbook file extensions to mkdocs-compatible format. e.g.
    def file1_group(self, groups):
        file_src = groups.group(1).replace('\\', '')
        asset = groups.group(2).replace('\\', '')
        if not file_src.startswith("http") and asset not in self.assets_dict:
            self.assets_dict[asset] = asset
        return f"!!! file\n\n\t[{asset}]({file_src})"

    # Replace gitbook file extensions to mkdocs-compatible format. e.g.
    def file2_group(self, groups):
        file_src = groups.group(1).replace('\\', '')
        asset = file_src.split('/')[-1].replace('\\', '')
        if not file_src.startswith("http") and asset not in self.assets_dict:
            self.assets_dict[asset] = asset
        return f"!!! file\n\n\t[{asset}]({file_src})"

    # Replace gitbook image extensions to mkdocs-compatible format. e.g.
    def img_group(self, groups):
        img_path = groups.group(1).replace('\\', '')
        img_src = groups.group(2).replace('\\', '')
        img_src = img_src.replace('>', '')
        img = os.path.basename(img_src)
        img_ext = os.path.splitext(os.path.basename(img_src))[1]
        if img_path.startswith("http"):
            return f"![]({img_path}/{img})"
        if not img_path.startswith("http") and img not in self.assets_dict:
            self.assets_dict[img] = f"image-{len(self.assets_dict)}{img_ext}"
            print(f"... {img_path}, {img_src} >> {self.assets_dict[img]}")
        return f"![]({img_path}/{img})"

    # Replace gitbook figure extensions to mkdocs-compatible format. e.g.
    def figure_group(self, groups):
        img_path = groups.group(1).replace('\\', '')
        img_src = groups.group(2).replace('\\', '')
        img_src = img_src.replace('>', '');
        img = os.path.basename(img_src)
        img_ext = os.path.splitext(os.path.basename(img_src))[1]
        if img not in self.assets_dict:
            self.assets_dict[img] = f"image-{len(self.assets_dict)}{img_ext}"
            print(f"... {img_path}, {img_src} >> {self.assets_dict[img]}")
        return f"![]({img_path}/{img})"

    # Replace gitbook image extensions to mkdocs-compatible format. e.g.
    def perform_transformation(self):
        """
        Perform the actual transformation from Gitbook to MkDocs.
        """

        print("Starting...")

        # Delete docs/*
        if os.path.exists(self.doc_root):
            shutil.rmtree(self.doc_root)
            print("... deleted: " + self.doc_root)

        # Get all folders
        folders = glob.glob("*/")
        print("... found folders: " + str(folders))

        # Create docs/ if not exists
        if not os.path.exists(self.doc_root + self.images_dest):
            os.makedirs(self.doc_root + self.images_dest)
            print("... created: " + self.doc_root + ", " + self.doc_root + self.images_dest)

            # Copy md-pages tree to docs/
            for md_file in glob.glob("*.md"):
                shutil.copy(md_file, "docs/" + md_file)

            # Copy all folders to docs/
            for folder in folders:
                shutil.copytree(folder, "docs/" + folder)
        else:
            print("... please delete docs/")
            print("Aborting!")
            exit()

        # Recursiv replace in all *.md
        print("\nStarting copying md-pages to " + self.doc_root + " ...")
        for md_file in glob.glob(self.doc_root + "**/*.md", recursive=True):
            with open(md_file, 'r') as file:
                print("parsing: " + md_file)
                filedata = file.read()

            #
            # replace hint
            filedata = re.sub(self.gb_hint_rg, self.hint_group, filedata)
            # replace tabs
            filedata = re.sub(self.gb_tab_rg, self.tab_group, filedata)
            # replace youtube embeds
            filedata = re.sub(self.gb_embedY_rg, self.embedY_group, filedata)
            # replace general embeds
            filedata = re.sub(self.gb_embed_rg, self.embed_group, filedata)
            # replace files
            filedata = re.sub(self.gb_file1_rg, self.file1_group, filedata)
            filedata = re.sub(self.gb_file2_rg, self.file2_group, filedata)
            # replace images
            filedata = re.sub(self.gb_img_rg, self.img_group, filedata)
            # replace figures
            filedata = re.sub(self.gb_figure_rg, self.figure_group, filedata)

            # Write the file out again
            with open(md_file, 'w', encoding='utf-8') as file:
                file.write(filedata)
                
        # Generate navigation structure for MkDocs
        nav_structure = self.nav_generator.generate_nav()

        # Read head.yml and base.yml
        with open('head.yml', 'r', encoding='utf-8') as file:
            head_content = file.read()
        with open('base.yml', 'r', encoding='utf-8') as file:
            base_content = file.read()
        
        with open('mkdocs.yml', 'w', encoding='utf-8') as ymlfile:
            ymlfile.write(head_content + base_content)
            ymlfile.write(nav_structure)
            
        print("... transformation done.")
        
# Main
if __name__ == "__main__":
    transformer = GitbookToMkDocs()
    transformer.perform_transformation()
