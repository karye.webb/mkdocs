#!/usr/bin/python3

import re
import os

class NavGenerator:

    # Define a regular expression to match the links in the SUMMARY.md file
    def __init__(self, skip_private=False):
        self.skip_private = skip_private
        self.pattern = re.compile(r"^(\s*)\*\s*\[([^\]]+)\]\(([^\)]+)\)( p)?")
        self.nav = ["nav:"]
        self.summary_file = "SUMMARY.md"

    # Check if SUMMARY.md exists
    def check_summary_file(self):
        if not os.path.isfile(self.summary_file):
            print("... SUMMARY.md not found")
            exit()

    # Read the lines from SUMMARY.md file
    def read_summary_lines(self):
        with open(self.summary_file, "r", encoding="utf-8") as f:
            self.lines = f.readlines()

    # Parse the lines and add the information to the nav.yml data structure
    def parse_lines(self):
        for line in self.lines:
            self.handle_chapter_line(line)
            self.handle_title_and_link_line(line)

    # If lines stat with ##, it is a chapter
    def handle_chapter_line(self, line):
        if line.startswith("## "):
            chapter = line[3:].strip()
            self.nav.append("  - \'{}\':".format(chapter))

    # If lines start with *, it is a title and link
    def handle_title_and_link_line(self, line):
        match = self.pattern.match(line)
        if match:
            indent, title, link, private = match.groups()
            if private and self.skip_private:
                return
            
            if link.endswith("README.md") and self.is_subchapter(indent, line):
                self.nav.append("    {}- \'{}\':".format(indent, title))
                self.nav.append("      {}- \'{}\': {}".format(indent, title, link))
            else:
                self.nav.append("    {}- \'{}\': {}".format(indent, title, link))

    # If link ends with READMD.md and next line is more indented, it is a subchapter
    def is_subchapter(self, indent, line):
        next_line = self.lines[self.lines.index(line) + 1]
        return next_line.startswith(indent + "  ")

    # Return the nav.yml data structure and the list of chapters and titles
    def generate_nav(self):
        self.check_summary_file()
        self.read_summary_lines()
        self.parse_lines()
        return "\n".join(self.nav)

# Generate the nav.yml data structure
def generate_nav(skip_private=False):
    generator = NavGenerator(skip_private)
    return generator.generate_nav()
