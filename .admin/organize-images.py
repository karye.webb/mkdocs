#!/usr/bin/python3

import os
import re
import shutil
import json
from typing import Dict, List

# Constants
# Define the source directory for the markdown files
md_dir = "."

# Root folder for mkdocs
doc_root = "docs/"

# Define the directory for the images
image_dir = ".gitbook/assets/"

# Define the output files for the JSON data
file_markdowns = ".admin/images-markdowns.json"
file_images = ".admin/images-all.json"
file_image_folders = ".admin/images-folders.json"

# Function to create the folder structure for the image
def create_image_folder_structure(images: Dict[str, List[str]], src_dir: str, dst_dir: str, output_file: str):
    # Dictionary to store the collected image paths and their new paths
    markdown_images = {}

    # Iterate through the images dictionary
    for md_file, image_paths in images.items():
        # Create the destination folder for the markdown page
        folder = os.path.join(dst_dir, os.path.dirname(md_file))
        os.makedirs(folder, exist_ok=True)

        # Get the source and destination paths for the images
        for image_path in image_paths:
            print(f"Processing image: {image_path}")
            src_path = os.path.join(src_dir, os.path.basename(image_path))
            dst_path = os.path.join(folder, os.path.basename(image_path))

            # Get the relative path from markdown file to image-folder
            rel_path = os.path.relpath(folder, os.path.dirname(md_file))

            # Copy the image to the destination folder if not already there
            if not os.path.exists(dst_path):
                print(f"Copying image from {src_path} to {dst_path}")
                try :
                    shutil.copy(src_path, dst_path)

                    # Delete the image from the source folder
                    print(f"Deleting image from {src_path}")
                    os.remove(src_path)
                except FileNotFoundError:
                    print(f"Image not found: {src_path}")


            # Store the source and destination paths in the markdown_images dictionary
            markdown_images[image_path] = os.path.join(rel_path, os.path.basename(image_path))

        # Save the markdown_images dictionary to the output file
        with open(output_file, "w") as f:
            json.dump(markdown_images, f, indent=4)

# Function to parse the markdown files and collect the image paths
def parse_markdown_images(md_dir, image_dir, markdown_file, images_file, folders_file):
    # Regular expression to match image paths in markdown files
    # This pattern searches for a string that starts with an exclamation mark
    image_pattern = re.compile(r"!\[.*\]\((.*)\)")

    # Regex to match {% file src="../.gitbook/assets/karate_gfx.zip" %}
    file_pattern = re.compile(r"{% file src=\"(.*)\" %}")

    # Regex to match HTML <figure><img src="..." /><figcaption>...</figcaption></figure>
    html_pattern = re.compile(r"<figure><img src=\"(.*)\" /><figcaption>.*</figcaption></figure>")
    
    # Dictionary to store the collected image paths
    # images: Dict[str, List[str]]
    images = {}
    folders = []

    # Iterate through all markdown files in the source directory and its subdirectories
    for root, dirs, files in os.walk(md_dir):
        # Skip hidden dot-folders and "docs" folder
        dirs[:] = [d for d in dirs if not d.startswith(".") and d != "docs"]
        print(f"Processing directory: {root}")
        
        for file in files:
            # Check if the file is a markdown file
            if file.endswith(".md"):
                # Print the name of the file being parsed
                print(f"Parsing file: {file}")

                # Get the full path of the markdown file
                md_file = os.path.join(root, file)

                # Get the relative path of the markdown file
                md_rel_path = os.path.relpath(md_file, md_dir)

                # Open the markdown file and read its contents
                with open(md_file, "r") as f:
                    content = f.read()

                # Search for image paths in the markdown file
                image_list = []
                for match in re.finditer(image_pattern, content):
                    # Skip external images
                    # An image is considered external if it starts with "http" or "www"
                    if match.group(1).startswith("http") or match.group(1).startswith("www"):
                        continue

                    # Print the image path
                    print(f"Found image: {match.group(1)}")

                    # Add the image path
                    image_list += [match.group(1)]

                # Search for image paths in the markdown file
                for match in re.finditer(html_pattern, content):
                    # Skip external images
                    # An image is considered external if it starts with "http" or "www"
                    if match.group(1).startswith("http") or match.group(1).startswith("www"):
                        continue

                    # Print the image path
                    print(f"Found image: {match.group(1)}")

                    # Add the image path
                    image_list += [match.group(1)]

                # Search for file paths in the markdown file
                for match in re.finditer(file_pattern, content):
                    # Skip external images
                    # An image is considered external if it starts with "http" or "www"
                    if match.group(1).startswith("http") or match.group(1).startswith("www"):
                        continue

                    # Print the image path
                    print(f"Found file: {match.group(1)}")

                    # Add the image path
                    image_list += [match.group(1)]

                # Store the image path in the images dictionary, using the
                # relative path of the markdown file as the key
                images[md_rel_path] = image_list
            
                # Get markdown file folder
                folder = os.path.dirname(md_rel_path)
                # Check if folder is already in folders list nor empty
                if folder not in folders and folder != "":
                    folders += [folder]

    # Write the images dictionary to the output file as a JSON object
    with open(markdown_file, "w") as f:
        json.dump(images, f, indent=4)

    # Write the folders list to the output file as a JSON object
    with open(folders_file, "w") as f:
        json.dump(folders, f, indent=4)

    # Copy the images to the image_dir directory
    create_image_folder_structure(images, ".gitbook/assets", image_dir, images_file)

# Function to replace the image paths in the markdown files
# Providing the files for markdown_images dictionary and images dictionary as arguments
def replace_image_paths(md_dir, file_markdowns, file_images):
    # Read the markdown_images dictionary from the output file
    with open(file_markdowns, "r") as f:
        markdown_images = json.load(f)

    # Read the images dictionary from the output file
    with open(file_images, "r") as f:
        images = json.load(f)

    # Iterate through all markdown files in the markdown_images dictionary
    for md_file, image_paths in markdown_images.items():
        print(f"Processing file: {md_file}")

        # Open the markdown file and read its contents
        with open(md_file, "r") as f:
            content = f.read()

        # Iterate through all image paths in the markdown file
        for image_path in image_paths:
            # Get the new image path
            new_image_path = images[image_path]

            # Replace the old image path with the new image path
            content = content.replace(image_path, new_image_path)

        # Write the updated content to the markdown file
        with open(md_file, "w") as f:
            f.write(content)

# Call the parse_markdown_images function to parse the markdown files
parse_markdown_images(md_dir, image_dir, file_markdowns, file_images, file_image_folders)

# Call the replace_image_paths function to replace the image paths
replace_image_paths(doc_root, file_markdowns, file_images)
