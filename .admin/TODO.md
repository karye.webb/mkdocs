* {% file src="/.gitbook/assets/raylib/jagamonster/resurser.zip" %}
  * [resurser.zip](../../images/resurser.zip)

* Radera alla originalfiler i mappen **/.gitbook/assets**

* Byta ut HTML **figure** mot **img** i alla markdown-filer

* Städa alla bilder med markdown **[](..)**

* Nu är omgenering av numrerade bilder bortkommenterad i **gitbook2mkdocs.py**

* Måste hitta en säkrare lösning för att kopiera bildmappar till **mkdocs/images**

* Testa post-checkout git hook

* Kunna hantera rubriker i SUMMARY.md