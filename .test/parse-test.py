#!/usr/bin/python3
# Inspired by Henry Zhang (@henryz) https://gist.github.com/henryz00/366dbb9b34cab1dd45fd1b0a96ea41de
# From gitbook to mkdocs on gitlab.com
# https://www.kite.com/python/answers/how-to-list-immediate-subdirectories-in-python

import glob
import os
import shutil
import re

print("Starting...")

# Tabsize in spaces
tabsize = 2

# mkdocs root folder
doc_root = "docs/"

# move all assets files
source = ".gitbook/assets/"
dest = "images/"

# Get all folders
folders = glob.glob("*/")

# Create docs/images
if not os.path.exists(doc_root + dest):
    os.makedirs(doc_root + dest)
    print("... Created docs/")
else:
    print("... Please delete docs/")
    print("Aborting!")
    exit()

# Copy all assets to docs/images
if os.path.exists(source):
    for file in os.listdir(source):
        shutil.copy(source + file, doc_root + dest)

    print("... Moved .gitbook/assets to docs/images")

    # Copy md-pages tree to docs/
    for md_file in glob.glob("*.md"):
        shutil.copy(md_file, "docs/" + md_file)

    for folder in folders:
        shutil.copytree(folder, "docs/" + folder)
else:
    print("... Could not copy assets and md-pages to docs/")
    print("Aborting!")
    exit()

# remove .github folder
#if os.path.exists(doc_root + ".github/"):
#    shutil.rmtree(doc_root + ".github")

# Move into docs/
os.chdir(doc_root)

# replace gitbook hint and file extensions to mkdocs-compatible format. e.g.
# --------- replace ---------
# {% hint style="warning" %}
# some text
# {% endhint %}
# --------- to ---------
# !!! warning
#     some text
gb_hint_rg = r'{% hint style=\"(.*)\" %}(.*|[\s\S]+?){% endhint %}'
def hint_group(groups):
    hint_type = groups.group(1)
    hint_content = groups.group(2)
    hint_content = hint_content.replace("\n", "\n\t")
    return "!!! " + hint_type + "\n" + hint_content

# replace gitbook tab and file extensions to mkdocs-compatible format. e.g.
# --------- replace ---------
# {% tab title="file.txt" %}
# some text
# {% endtab %}
# --------- to ---------
# ### warning
#     some text
gb_tab_rg = r'{% tab title=\"(.*)\" %}(.*|[\s\S]+?){% endtab %}'
def tab_group(groups):
    tab_type = groups.group(1)
    tab_content = groups.group(2)
    tab_content = tab_content.replace("\n", "\n\t")
    return "=== \"" + tab_type + "\"\n" + tab_content

# replace gitbook embed to mkdocs-compatible format. e.g.
# --------- replace ---------
# {% embed url="https://youtu.be/..." %}
gb_embed_rg = r'{% embed url=\"https://youtu.be/(.*)\" %}'
def embed_group(groups):
    embed_url = groups.group(1)
    return "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/" + embed_url + "\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"
    
# replace gitbook embed to mkdocs-compatible format. e.g.
# --------- replace ---------
# {% embed url="..." %}
gb_embedY_rg = r'{% embed url=\"(.*)\" %}'
def embedY_group(groups):
    embedY_url = groups.group(1)
    return "[" + embedY_url  + "](" + embedY_url + ")"

# Recursiv replace in all *.md
for md_file in glob.glob("**/*.md", recursive=True):
    with open(md_file, 'r') as file:
        print(md_file)
        filedata = file.read()

    filedata = filedata.replace(source, dest)
    filedata = re.sub(gb_hint_rg, hint_group, filedata)
    filedata = re.sub(gb_tab_rg, tab_group, filedata)
    filedata = re.sub(gb_embed_rg, embed_group, filedata)
    filedata = re.sub(gb_embedY_rg, embed_group, filedata)
    
    filedata = filedata.replace("{% tabs %}\n", "")
    filedata = filedata.replace("{% endtabs %}\n", "")

    filedata = filedata.replace("\\\n", "  \n")

    with open(md_file, 'w') as file:
        file.write(filedata)

print("... Copied md-pages tree to docs/")
