#!/usr/bin/python3
# Inspired by Henry Zhang (@henryz) https://gist.github.com/henryz00/366dbb9b34cab1dd45fd1b0a96ea41de
# From gitbook to mkdocs on gitlab.com
# https://www.kite.com/python/answers/how-to-list-immediate-subdirectories-in-python

import glob
import os
import shutil
import re

print("Starting...")

# Tabsize in spaces
tabsize = 2

# mkdocs root folder
doc_root = "docs/"

# move all assets files
source = ".gitbook/assets/"
dest = "images/"

# Move into docs/
os.chdir(doc_root)

gb_embedY_rg = r'{% embed url=\"https://youtu.be/(.*)\" %}'
def embedY_group(groups):
    embed_url = groups.group(1)
    return "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/" + embed_url + "\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"

# Filter out gitbook tags
gb_rg = r'{% hint|tab|embed|file style|title|url|src=\"(.*)\" %}(.*|[\s\S]+?){% endhint|endtab|endembed|endfile %}'
def gb_group(groups):
    gb_type = groups.group(1)
    gb_content = groups.group(2)
    gb_content = gb_content.replace("\n", "\n\t")
    return "!!! " + gb_type + "\n" + gb_content

# Recursiv replace in all *.md
for md_file in glob.glob("**/*.md", recursive=True):
    with open(md_file, 'r') as file:
        print(md_file)
        filedata = file.read()

    filedata = filedata.replace(source, dest)
    filedata = re.sub(gb_embedY_rg, embedY_group, filedata)
    filedata = re.sub(gb_rg, gb_group, filedata)
    
    filedata = filedata.replace("{% tabs %}\n", "")
    filedata = filedata.replace("{% endtabs %}\n", "")
    filedata = filedata.replace("{% file %}\n", "")
    filedata = filedata.replace("{% endfile %}\n", "")

    # https://www.markdownguide.org/basic-syntax/#line-break-best-practices
    filedata = filedata.replace("\\\n", "  \n")

    # Replace tags
    filedata = filedata.replace("\<", "&lt;")
    filedata = filedata.replace("\>", "&gt;")

    with open(md_file, 'w') as file:
        file.write(filedata)

print("... Copied md-pages tree to docs/")