import glob
import os
import shutil
import re

print("Starting...")

# Tabsize in spaces
tabsize = 2

# mkdocs root folder
doc_root = "docs/"

# move all assets files
source = ".gitbook/assets/"
dest = "images/"

# replace nav with SUMMARY.md
subpage_match_rg = r'## (.*)\n(.*|[\s\S]+?)(\Z|(?=##))'
def subpage_group(groups):
    title = groups.group(1)
    title = re.sub(" <a.*>", "", title)
    content = groups.group(2)
    content = content.replace("- '", "  - '")
    return "  - '" + title + "':\n" + content

with open('SUMMARY.md', 'r') as file:
    nav_content = file.read()

nav_content = nav_content.replace('# Table of contents', 'nav:')
nav_content = nav_content.replace('\n\n', '\n')
nav_content = re.sub(r"\* \[(.*)\]\((.*)\)", r"  - '\1': \2", nav_content)
nav_content = re.sub(subpage_match_rg, subpage_group, nav_content)
print(nav_content)

# Rebuild nav YAML-compliant
indent = 0
indent_pre = 0
pre = ""
lines = nav_content.splitlines()
for index, line in enumerate(lines):
    # Find new subgroup
    # Count indent length
    indents = re.findall(r"^( +)- '", line)
    if len(indents) > 0:
        indent = len(indents[0])
    else:
        indent = 0
    if (indent > tabsize and indent > indent_pre):
        # Check if there is a page
        #print(lines[index - 1].split("': "))
        if len(lines[index - 1].split("': ")) == 2:
            # Insert page as subgroup:page
            lines.insert(index, "  " + lines[index - 1])
            # Remove page from subgroup
            lines[index - 1] = lines[index - 1].split("': ", 1)[0] + "':"
    indent_pre = indent
    pre = line
nav_content = "\n".join(lines)

print("... Collected navigation from SUMMARY.md")
print(nav_content)

# write nav changes to yml
#os.chdir('..')

# Remove old nav-content
print("... Opening nav in mkdocs.yml")
num = 0
lines = 0
with open('mkdocs.yml', 'r') as file:
    lines = file.readlines()
    for num, line in enumerate(lines):
        if "nav:" ==  line[0:4]:
            print('... Found nav: at line:', num)
            break
if num != 0:
    with open('mkdocs.yml', 'w') as file:
        file.writelines(lines[:num - 1])

# Add new nav-content
with open('mkdocs.yml', 'a') as file:
    file.write("\n" + nav_content)

print("... Added nav in mkdocs.yml")
print("Done!")