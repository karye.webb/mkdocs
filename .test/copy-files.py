#!/usr/bin/python3
# Inspired by Henry Zhang (@henryz) https://gist.github.com/henryz00/366dbb9b34cab1dd45fd1b0a96ea41de
# From gitbook to mkdocs on gitlab.com
# https://www.kite.com/python/answers/how-to-list-immediate-subdirectories-in-python

import glob
import os
import shutil
import re
import uuid
import pathlib

print("Starting...")

# Tabsize in spaces
tabsize = 2

# Root folder for mkdocs
doc_root = "docs/"

# All assets files
source = ".gitbook/assets/"
dest = "images/"

images = {}

# Get all folders
folders = glob.glob("*/")

# Create docs/images
if not os.path.exists(doc_root + dest):
    os.makedirs(doc_root + dest)
    print("... Created docs/")
else:
    print("... Please delete docs/")
    print("Aborting!")
    exit()

# Copy all assets to docs/images
if os.path.exists(source):
    for file in os.listdir(source):
        print(doc_root + dest + file)
        fileNew = str(uuid.uuid1()) + str(pathlib.Path(file).suffix)
        images[file] = fileNew
        shutil.copy(source + file, doc_root + dest + fileNew)

    print("... Moved .gitbook/assets to docs/images")

    # Copy md-pages tree to docs/
    for md_file in glob.glob("*.md"):
        shutil.copy(md_file, "docs/" + md_file)

    # Copy all folders to docs/
    for folder in folders:
        shutil.copytree(folder, "docs/" + folder)

# Move into docs/
os.chdir(doc_root)

# Recursiv replace in all *.md
for md_file in glob.glob("**/*.md", recursive=True):
    with open(md_file, 'r') as file:
        print(md_file)
        filedata = file.read()

    # Replace with new images location
    filedata = filedata.replace(source, dest)

    # Replace image src
    for key, value in images.items():
        filedata = filedata.replace(dest + key, dest + value)

    # Save result back to file
    with open(md_file, 'w') as file:
        file.write(filedata)

print("... Copied md-pages tree to docs/")