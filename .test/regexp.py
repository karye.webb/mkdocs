import os, re

# replace gitbook hint and file extensions to mkdocs-compatible format. e.g.
# --------- replace ---------
# {% hint style="warning" %}
# some text
# {% endhint %}
# --------- to ---------
# !!! warning
#     some text
gb_hint_rg = r'{% hint style=\"(.*)\" %}(.*|[\s\S]+?){% endhint %}'
def hint_group(groups):
    hint_type = groups.group(1)
    hint_content = groups.group(2)
    hint_content = hint_content.replace("\n", "\n\t")
    return "!!! " + hint_type + "\n" + hint_content

# replace gitbook tab and file extensions to mkdocs-compatible format. e.g.
# --------- replace ---------
# {% tab title="file.txt" %}
# some text
# {% endtab %}
# --------- to ---------
# ### warning
#     some text
gb_tab_rg = r'{% tab title=\"(.*)\" %}(.*|[\s\S]+?){% endtab %}'
def tab_group(groups):
    tab_type = groups.group(1)
    tab_content = groups.group(2)
    tab_content = tab_content.replace("\n", "\n\t")
    return "=== \"" + tab_type + "\"\n" + tab_content

# replace gitbook embed to mkdocs-compatible format. e.g.
# --------- replace ---------
# {% embed url="https://youtu.be/..." %}
gb_embedY_rg = r'{% embed url=\"https://w*\.*youtu.*/(.*)\" %}'
def embedY_group(groups):
    embedY_url = groups.group(1)
    return "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/" + embedY_url + "\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"
    
# replace gitbook embed to mkdocs-compatible format. e.g.
# --------- replace ---------
# {% embed url="..." %}
gb_embed_rg = r'{% embed url=\"(.*)\" %}'
def embed_group(groups):
    embed_url = groups.group(1)
    #return "<div class=\"embed\">[" + embed_url  + "](" + embed_url + ")</div>"
    return "<div class=\"embed\"><i class=\"fas fa-link\"></i><a href=\"" + embed_url  + "\">" + embed_url + "</a></div>"

# replace gitbook embed to mkdocs-compatible format. e.g.
# --------- replace ---------
# {% file src="..." %}
#assets_dict = {}
gb_file1_rg = r'{% file src=\"(.*)\" %}\n(.*|[\s\S]+?)\n{% endfile %}'
def file1_group(groups):
    file_src = groups.group(1)
    asset = groups.group(2)

    # Add asset to list if not found in array
    if asset not in assets_dict:
        assets_dict[asset] = asset

    return "!!! file\n\n\t[" + asset  + "](" + urllib.parse.quote(file_src) + ")"

gb_file2_rg = r'{% file src=\"(.*)\" %}'
def file2_group(groups):
    file_src = groups.group(1)
    asset = file_src.split('/')[-1]

    # Add asset to list if not found in array
    if asset not in assets_dict:
        assets_dict[asset] = asset
        
    return "!!! file\n\n\t[" + asset + "](" + urllib.parse.quote(file_src) + ")"

# Find image
gb_img_rg = r'\!\[.*\]\(<?(.*)/(.*)>?\)'
def img_group(groups):
    img_path = groups.group(1)
    img_src = groups.group(2)

    # Fix ending '>'
    img_src = img_src.replace('>', '');
    img = os.path.basename(img_src)
    img_ext = os.path.splitext(os.path.basename(img_src))[1]

    # Get assets_dict length
    img_index = len(assets_dict) + 1

    # if img_path not starting with http, and not in assets_dict, add to assets_dict
    if not img_path.startswith("http") and img_path not in assets_dict:
        assets_dict[img] = "image-" + str(img_index) + img_ext
        #img_index += 1

        print("... " + img_path + ", " + img_src + " >> " + assets_dict[img])
        return "![](" + img_path + "/" + assets_dict[img] + ")"

gb_figure_rg = r'<figure><img src=\"(.*)/(.*)\" alt=\"\"><figcaption></figcaption></figure>'
def figure_group(groups):
    img_path = groups.group(1)
    img_src = groups.group(2)

    # Fix ending '>'
    img_src = img_src.replace('>', '');

    img = os.path.basename(img_src)
    img_ext = os.path.splitext(os.path.basename(img_src))[1]

    # Get assets_dict length
    img_index = len(assets_dict) + 1

    # Add image to list if not found in array
    if img not in assets_dict:
        assets_dict[img] = "image-" + str(img_index) + img_ext
        #img_index += 1

    print("... " + img_path + ", " + img_src + " >> " + assets_dict[img])
    return "![](" + img_path + "/" + assets_dict[img] + ")"