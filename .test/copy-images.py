#!/usr/bin/python3

import os
import re
import shutil

# Find all markdown files in the current directory and its subdirectories
def extract_image_paths(markdown_text):
    image_paths = re.findall('!\[.*\]\((.*)\)', markdown_text)
    return image_paths

# Copy images to the same folder as the markdown file
def copy_images_to_folder(image_paths, source_folder, dest_folder):
    for image_path in image_paths:
        filename = os.path.basename(image_path)
        source_path = os.path.join(source_folder, filename)
        dest_path = os.path.join(dest_folder, filename)
        shutil.copy2(source_path, dest_path)

# Find all markdown files in the current directory and its subdirectories
def main():
    source_folder = '.gitbook/assets/'
    markdown_folder = '.'
    for root, dirs, files in os.walk(markdown_folder):
        for file in files:
            if file.endswith('.md'):
                print('Processing file: ' + file)
                markdown_path = os.path.join(root, file)
                with open(markdown_path, 'r') as f:
                    markdown_text = f.read()
                image_paths = extract_image_paths(markdown_text)
                dest_folder = root
                copy_images_to_folder(image_paths, source_folder, dest_folder)

if __name__ == '__main__':
    main()
