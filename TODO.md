# Logg

## Mermaid

### VS Code Inställningar

För att få ett snyggt diagram i VS Code Inställningar:

```json
    "markdown-mermaid.lightModeTheme": "neutral",
    "markdown-mermaid.darkModeTheme": "neutral"
```

### I markdown

Eller i texten:

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#FFF',
      'primaryBorderColor': '#000'
    }
  }
}%%
classDiagram
    class PersonInkomst {
        - namn : string
        - inkomst : int
        - timmar : int
        + GetTimlon() int
        + GetTimlon(int skatt) int
    }
```

### I css

Eller i css:

```css
/* Mermaid */
--md-mermaid-node-bg-color: #fff;
--md-mermaid-node-fg-color: #111;
--md-mermaid-label-fg-color: #333;
```
