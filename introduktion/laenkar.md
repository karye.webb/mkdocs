---
description: Matnyttiga länkar
---

# Länkar

### Material på svenska

Bra kurs på svenska med övningar och uppgifter:

{% embed url="https://www.progsharp.se" %}

Referens på svenska över det viktigaste delarna i C#:  

{% embed url="https://csharp.progdocs.se %}

### Material på engelska

Cheat Sheet med bra överblick:

{% embed url="https://www.codecademy.com/learn/learn-c-sharp/modules/csharp-hello-world/cheatsheet" %}

Dokumentation över hela C#:

{% embed url="https://docs.microsoft.com/en-us/dotnet/csharp/" %}

## Tutorials

### C# programmering med csharpskolan

Träna på OOP i C# med csharpskolan.se:

{% embed url="https://www.youtube.com/watch?v=yK6zlTVqWzo&list=PLGIaaBeLgSj-Co97ECdCbGeWKBbHpz9tS" %}

