---
description: Installera alla verktyg, konfigurera alla inställningar, skapar alla mappar
---

# Utvecklingsmiljön

## Installera och ställa in VS Code

* Installera VSCode från [https://code.visualstudio.com](https://code.visualstudio.com)
* Installera git från [https://git-scm.com/download/win](https://git-scm.com/download/win)
* Installera dotnet **installers x64** från:
  * [https://dotnet.microsoft.com/en-us/download/dotnet/6.0](https://dotnet.microsoft.com/en-us/download/dotnet/6.0)
  * [https://dotnet.microsoft.com/en-us/download/dotnet/5.0](https://dotnet.microsoft.com/en-us/download/dotnet/5.0)
* Ladda ned och installera [ffmpeg](https://github.com/icedterminal/ffmpeg-installer/releases/download/5.1.0.20220727/FFmpeg\_Essentials.msi)

### Installera tilläggen

* [C#](https://www.google.com/url?q=https%3A%2F%2Fmarketplace.visualstudio.com%2Fitems%3FitemName%3Dms-vscode.csharp\&sa=D\&sntz=1\&usg=AFQjCNGOzgSFj14Pbd9ut66JAvh0loJsEw) – Ger Visual Studio Code stöd för C#
* [C# Toolbox of Productivity](https://marketplace.visualstudio.com/items?itemName=RichardZampieriprog.csharp-snippet-productivity) – Lägger till en del extra användbara genvägar och funktioner, tex för att skapa nya projekt och klasser
* [gitignore](https://www.google.com/url?q=https%3A%2F%2Fmarketplace.visualstudio.com%2Fitems%3FitemName%3Dcodezombiech.gitignore\&sa=D\&sntz=1\&usg=AFQjCNHu8aUEHuuoWIdAZQcCdvDqnSWhSQ) – Underlättar arbetet med git och Visual Studio Code. Om du söker efter den, se till att ta den av CodeZombie!
* [VSCode Great Icons](https://marketplace.visualstudio.com/items?itemName=emmanuelbeziat.vscode-great-icons) – Gör det lättare att känna igen filtyper
* [XML Complete](https://marketplace.visualstudio.com/items?itemName=rogalmic.vscode-xml-complete) – För att jobba med WPF och Xaml
* [GitHub Classroom](https://marketplace.visualstudio.com/items?itemName=GitHub.classroom) - För att arbeta med labbar och prov på Github Classroom
* [Chronicler](https://marketplace.visualstudio.com/items?itemName=arcsine.chronicler) - För att spela in kodsessioner
* [Open Folder Context Menu](https://marketplace.visualstudio.com/items?itemName=chrisdias.vscode-opennewinstance) - För att enkelt öppna en mapp
* [C# XML Documentation Comments](https://marketplace.visualstudio.com/items?itemName=k--kato.docomment) - För att kommentera metoder och klasser

### Inställningar i VS Code

Gå in inställningar (Settings):

* [Multi-cursor](https://code.visualstudio.com/docs/editor/codebasics#\_multiple-selections-multicursor) – Välj Ctrl-Cmd

![](/.gitbook/assets/introduktion/image-1.png)

* [Compact folders](https://code.visualstudio.com/updates/v1\_41#\_compact-folders-in-explorer) – Stäng av

![](/.gitbook/assets/introduktion/image-2.png)

* [Code lens](https://code.visualstudio.com/blogs/2017/02/12/code-lens-roundup) - Stäng av

![](/.gitbook/assets/introduktion/image-3.png)

### Kortare prompt i Powershell

Gör följande för att korta ned prompten i Terminalen/Konsolen i VS Code.

* Öppna terminalen och skriv/kör dessa rader:

```powershell
test-path $profile
new-item -path $profile -itemtype file -force
```

* Öppna Powershell-profilen i VS Code

```powershell
code $profile
```

* Skriv in följande i Powershell-profilen och spara:

```powershell
function prompt {
   $p = Split-Path -leaf -path (Get-Location)
   "$p> "
}
```

* I terminalen skriv/kör:

```powershell
Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
```

* Starta om VS Code

{% hint style="info" %}
[https://superuser.com/questions/446827/configure-windows-powershell-to-display-only-the-current-folder-name-in-the-shel](https://superuser.com/questions/446827/configure-windows-powershell-to-display-only-the-current-folder-name-in-the-shel)
{% endhint %}

### Intro till VS Code

{% embed url="https://youtu.be/4NfFFsQC77M" %}

## Komma igång med github

* Skapa en konto på [https://github.com](https://github.com)
  * Använd namnet på ditt ga-konto: Tex förnamn.efternamn
  * Valfritt lösenord
  * Använd din skolmail

## C# snippets

Med snippets kan du skapa egna kortkommandon för kod du använder ofta:

* Inställningar -> _User Snippets_
* _-> New Global Snippets -> C#_

```json
{
	"EventXAML": {
		"prefix": "eventXAML",
		"body": [
			"private void $1(object sender, RoutedEventArgs e)",
			"{",
			"\t$2",
			"}"
		],
		"description": "Handle event from XAML WPF"
	}
}
```
