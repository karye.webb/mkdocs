---
description: Raylib
---

# Raylib

## Skapa ett Raylib-projekt

Skapa en ny projektmapp. Gör det till en konsolapplikation och lägg till Raylib-cs som en beroende.

```shell
dotnet new console
dotnet add package Raylib-cs
```

## Grundkoden

```csharp
using Raylib_cs;

namespace JagaMonster
{
    class Program
    {
        static void Main(string[] args)
        {
            Raylib.InitWindow(512, 480, "Raylib");
            Raylib.SetTargetFPS(60);

            // Skapa objekt här

            while (!Raylib.WindowShouldClose())
            {
                Raylib.BeginDrawing();
                Raylib.ClearBackground(Color.DARKGREEN);

                // Rita ut här

                Raylib.EndDrawing();
            }
        }
    }
}
```

## VS Code snippet

Du kan skapa en snippet för att snabbt få tillgång till koden:

1. Öppna VS Code
2. Gå till `File` -> `Preferences` -> `User Snippets`
3. Välj `csharp`
4. Lägg till följande kod:

```json
"Raylib grundkod": {
		"prefix": "raylib",
		"body": [
			"Raylib.InitWindow(512, 480, \"Raylib\");",
            "Raylib.SetTargetFPS(60);",
			"\n$1\n",
			"while (!Raylib.WindowShouldClose())",
			"{",
			"\tRaylib.BeginDrawing();",
			"\tRaylib.ClearBackground(Color.WHITE);",
			"\t$2",
			"\tRaylib.EndDrawing();",
			"}"
		],
		"description": "Log output to console"
	}
```

## Hur man ritar

### Rita en rektangel

Du kan rita en rektangel med `Raylib.DrawRectangle()`:

```csharp
Raylib.InitWindow(512, 480, "Raylib");
Raylib.SetTargetFPS(60);

while (!Raylib.WindowShouldClose())
{
    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.DARKGREEN);

    Raylib.DrawRectangle(10, 10, 100, 100, Color.RED);

    Raylib.EndDrawing();
}
```

### Rita en bild

Du kan rita en bild med `Raylib.DrawTexture()`:

```csharp
Raylib.InitWindow(512, 480, "Raylib");
Raylib.SetTargetFPS(60);

Texture2D texture = Raylib.LoadTexture("resurser/monster.png");

while (!Raylib.WindowShouldClose())
{
    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.DARKGREEN);

    Raylib.DrawTexture(texture, 10, 10, Color.WHITE);

    Raylib.EndDrawing();
}
```

## Kollisionshantering

För att upptäcka kollisioner måste du använda rektanglar. 
Varje objekt har en hitbox som är en rektangel. 

### Skapa en hitbox

Du kan rita en bild med en hitbox med `Raylib.DrawRectangleRec()`:

```csharp
Raylib.InitWindow(512, 480, "Raylib");
Raylib.SetTargetFPS(60);

Texture2D texture = Raylib.LoadTexture("resurser/monster.png");
Rectangle hitbox = new Rectangle(10, 10, texture.width, texture.height);

while (!Raylib.WindowShouldClose())
{
    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.DARKGREEN);

    Raylib.DrawTexture(texture, hitbox.x, hitbox.y, Color.WHITE);

    Raylib.EndDrawing();
}
```

### Kolla om två hitboxar kolliderar

Du kan kolla om två hitboxar kolliderar med `Raylib.CheckCollisionRecs()`:

```csharp
Raylib.InitWindow(512, 480, "Raylib");
Raylib.SetTargetFPS(60);

Texture2D texture = Raylib.LoadTexture("resurser/hero.png");
Rectangle hero = new Rectangle(10, 10, texture.width, texture.height);

Texture2D texture2 = Raylib.LoadTexture("resurser/monster.png");
Rectangle monster = new Rectangle(200, 200, texture2.width, texture2.height);

while (!Raylib.WindowShouldClose())
{
    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.DARKGREEN);

    Raylib.DrawTexture(texture, hero.x, hero.y, Color.WHITE);
    Raylib.DrawTexture(texture2, monster.x, monster.y, Color.WHITE);

    if (Raylib.CheckCollisionRecs(hero, monster))
    {
        Raylib.DrawText("Kollision!", 10, 10, 20, Color.RED);
    }

    Raylib.EndDrawing();
}
```

## Interaktion med tangentbordet

Du kan kontrollera objekt med tangentbordet med `Raylib.IsKeyDown()`:

```csharp
Raylib.InitWindow(512, 480, "Raylib");
Raylib.SetTargetFPS(60);

Texture2D texture = Raylib.LoadTexture("resurser/hero.png");
Rectangle hero = new Rectangle(10, 10, 100, 100);

while (!Raylib.WindowShouldClose())
{
    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.DARKGREEN);

    Raylib.DrawRectangleRec(hero, Color.RED);

    if (Raylib.IsKeyDown(KeyboardKey.KEY_RIGHT))
    {
        hero.x += 1;
    }

    Raylib.EndDrawing();
}
```

## Interaktion med musen

Du kan kontrollera objekt med musen mha `Raylib.IsMouseButtonDown()`:

```csharp
Raylib.InitWindow(512, 480, "Raylib");
Raylib.SetTargetFPS(60);

Texture2D texture = Raylib.LoadTexture("resurser/hero.png");
Rectangle hero = new Rectangle(10, 10, 100, 100);

while (!Raylib.WindowShouldClose())
{
    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.DARKGREEN);

    Raylib.DrawRectangleRec(hero, Color.RED);

    if (Raylib.IsMouseButtonDown(MouseButton.MOUSE_LEFT_BUTTON))
    {
        hero.x = Raylib.GetMouseX();
        hero.y = Raylib.GetMouseY();
    }

    Raylib.EndDrawing();
}
```
