---
description: Shmup med OOP
---

# Spelet Shmup

## Scenen

Till sist skapar vi en klass för scenen. Klassen ska ha följande egenskaper:

* Bakgrundsbild
* Poängräknare
* Livräknare
* Kontrollerar kollisioner mellan spelaren, fienderna och skotten
* Uppdaterar poäng och liv vid kollisioner och/eller träffar
* Avslutar spelet när spelaren har förlorat alla liv eller nått en viss poäng.