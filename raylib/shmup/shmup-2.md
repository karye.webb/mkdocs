---
description: Shmup med OOP
---

# Spelet Shmup

## Skotten

Nästa klass vi skapar är för skotten. Klassen ska ha följande egenskaper:

* Position (x, y)
* Hastighet
* Rör sig uppåt på skärmen
* Försvinner när den når övre kanten av skärmen

### Klassen Skott

Klassen heter `Skott()` och ser ut så här:

```csharp
public class Skott
{
    // Positionen på skottet
    private Vector2 _position;

    // Hastigheten för skottet
    private int _hastighet;

    // Konstruktor för att skapa en ny instans
    public Skott(int x, int y)
    {
        // Initialisera positionen och hastigheten
        _position = new Vector2(x, y);
        _hastighet = 5;
    }

    // Rita ut skottet
    public void Rita()
    {
        Raylib.DrawRectangle((int)_position.X + 30, (int)_position.Y - 15, 4, 20, Color.WHITE);
    }

    // Uppdatera skottets position
    public void Uppdatera()
    {
        _position.Y -= _hastighet;
    }
}
```

### Skjuta skott i klassen Skepp

Med hjälp av klassen `Skott()` skapar vi ett skott när vi trycker på mellanslag. Vi gör det i klassen `Skepp()`.
Vi lägger till en lista för skotten i klassen `Skepp()`:

```csharp
// Lista för skotten
private List<Skott> _skotten;
```

I konstruktorn för `Skepp()` skapar vi en ny lista för skotten:

```csharp
// Konstruktor för att skapa en ny instans
public Skepp(string texturFil)
{
    // Ladda in textur för skeppet
    _bild = Raylib.LoadTexture(texturFil);

    // Initialisera positionen och hastigheten
    _position = new Vector2(400, 250);

    // Initialisera liv till 3
    _liv = 3;

    // Initialisera antal skott till 10
    _skott = 10;

    // Skapa en ny lista för skotten
    _skotten = new List<Skott>();
}
```

I metoden `Uppdatera()` i klassen `Skepp()` skjuter vi ett skott när vi trycker på mellanslag. Vi lägger till följande kod:

```csharp 
// Skjut skott
if (Raylib.IsKeyDown(KeyboardKey.KEY_SPACE))
{
    Skjuta();
}
```

Vi skapar en metod för att skjuta skott och lägger ett ljud när vi skjuter. Metoden `Skjuta()` ser ut så här:

```csharp
// Skjut skott
private void Skjuta()
{
    // Skapa ett nytt skott
    Skott skott = new Skott((int)_position.X, (int)_position.Y);
    _skotten.Add(skott);

    // Spela upp skottljud
    Raylib.PlaySound(_skottLjud);
}
```

### Ljud

Vi kan ladda ned ett wav-ljud från [freesound.org](https://freesound.org/search/?q=arcade+shot&f=&w=&s=Automatic+by+relevance&advanced=0&g=1). 

Vi kan även skapa ljud i [pico-8](https://pico-8-edu.com). För att exportera ljudet skriver vi `export sfx` i pico-8 terminalen. Ljudet sparas i en fil som heter **sfx.wav**. 
Ljudet ska sedan ligga i mappen **resurser**. Se [export från Pico-8](https://pico-8.fandom.com/wiki/Export) för mer information.

Vi lägger ljudet i mappen **resurser** och laddar in det i konstruktorn i klassen `Skepp()`:

```csharp
// Ladda in skottljud
_skottLjud = Raylib.LoadSound("./resurser/sfx01.wav");
```

### Uppdatera skotten

I metoden `Uppdatera()` i klassen `Skepp()` uppdaterar vi skotten. Vi lägger till följande kod:

```csharp
// Uppdatera skotten
foreach (Skott skott in _skotten)
{
    skott.Uppdatera();
}
```

### Sammanfattning

Klassen `Skepp()` ser ut så här:

```csharp
public class Skepp
{
    // Bild på skeppet
    private Texture2D _bild;

    // Ljud för skott
    private Sound _skottLjud;

    // Positionen för skeppet
    private Vector2 _position;

    // Antal liv
    private int _liv;

    // Lista för skotten
    private List<Skott> _skotten;

    // Konstruktor för att skapa en ny instans
    public Skepp(string texturFil)
    {
        // Ladda in textur för skeppet
        _bild = Raylib.LoadTexture(texturFil);

        // Ladda in skottljud
        _skottLjud = Raylib.LoadSound("./resurser/sfx01.wav");

        // Initialisera positionen och hastigheten
        _position = new Vector2(400, 250);

        // Initialisera liv till 3
        _liv = 3;

        // Skapa en lista för skotten
        _skotten = new List<Skott>();
    }

    ...

    // Styra skeppet
    public void Uppdatera()
    {
        ...

        // Skjut skott
        if (Raylib.IsKeyDown(KeyboardKey.KEY_SPACE))
        {
            Skjuta();
        }

        // Uppdatera skotten
        foreach (Skott skott in _skotten)
        {
            skott.Uppdatera();
        }
    }

    // Skjut skott
    private void Skjuta()
    {
        // Skapa ett nytt skott
        Skott skott = new Skott((int)_position.X, (int)_position.Y);
        _skotten.Add(skott);
    }
}
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Skepp {
    -_bild: Texture2D
    -_skottLjud: Sound
    -_position: Vector2
    -_liv: int
    -_skotten: List<Skott>
    +Skepp(texturFil: string) : void
    +Rita() : void
    +Uppdatera() : void
    +Skjuta() : void
}
class Skott {
    -_position: Vector2
    -_hastighet: int
    +Skott(x: int, y: int) : void
    +Rita() : void
    +Uppdatera() : void
}
@enduml
```

Nu ser vi att skottet skjuts från skeppets position:

![](/.gitbook/assets/raylib/shmup/Shmup-2.png)

## Förbättringar

### Förbättring 1

* Prova olika sätt att skjuta skott. 
* Sätt hastigheten till 10 och skjut 3 skott i taget.  
* Skjut åt olika håll.

### Förbättring 2

* Prova olika ljud för skottet.
* Prova ljud när skeppet förflyttar sig.