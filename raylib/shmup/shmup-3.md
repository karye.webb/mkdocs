---
description: Shmup med OOP
---

# Spelet Shmup

## Fienderna

Vi fortsätter med att skapa en klass för fienderna. Klassen ska ha följande egenskaper:

* Position (x, y)
* Hastighet
* Liv
* Skjuter tillbaka mot spelaren
* Kan inte åka utanför scenen

