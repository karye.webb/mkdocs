---
description: Shmup med OOP
---

# Spelet Shmup

## Inledning

I denna uppgift ska du skapa ett spel som heter Shmup. Spelet är ett arkadspel där spelaren kontrollerar ett ryndskepp som skjuter mot fiender. Det finns också en bakgrundsbild och en poängräknare (Scen).

Vi skapar en klass för spelarens rymdskepp, en klass för fiender och en klass för skott. Vi skapar också en klass för scenen som håller reda på poäng och liv.

## Skeppets egenskaper

Vi börjar med att skapa en klass för skeppet. Klassen ska ha följande egenskaper:

* Position (x, y)
* Liv
* Skott
* Rör sig med piltangenterna
* Skjuter med mellanslag
* Kan inte åka utanför scenen

### Klass Skepp

Klassen heter Skepp och ser ut så här:

```csharp
class Skepp
{
    // Bild på skeppet
    private Texture2D _bild;

    // Positionen för skeppet
    private Vector2 _position;

    // Antal liv
    private int _antalLiv;

    // Antal skott som skeppn har
    private int _antalSkott;

    // Konstruktor för att skapa en ny instans
    public Skepp(string texturFil)
    {
        // Ladda in textur för skeppet
        _bild = LoadTexture(texturFil);

        // Initialisera positionen och hastigheten
        _position = new Vector2(400, 250);

        // Initialisera liv till 3
        _antalLiv = 3;

        // Initialisera antal skott till 10
        _antalSkott = 10;
    }

    // Rita ut skeppet
    public void Rita()
    {
        DrawTexture(_bild, (int)_position.X, (int)_position.Y, Color.WHITE);
    }
}
```


```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class skepp {
    -_bild : Texture2D
    -_position : Vector2
    -_antalLiv : int
    -_antalSkott : int
    +skepp(string texturFil) : void
    +Rita() : void
    +Liv : int
}
@enduml
```

### Main och animationsloopen

I Main skapar vi en instans av `Skepp` och kallar den för skepp.  
Välj en bild på en rymdskepp (Arcade) på [FlatIcon](https://www.flaticon.com/).  
Ladda ned storlek 32px och spara bilden i en mapp som heter **resurser**.

```csharp
using System;
using Raylib_cs;
using System.Numerics;

namespace Shmup
{
    class Program
    {
        static void Main(string[] args)
        {
            Raylib.InitWindow(800, 600, "Shmup");
            Raylib.SetTargetFPS(60);

            // Sätt fönstrets uppdateringshastighet till 60 uppdateringar per sekund
            Raylib.SetTargetFPS(60);

            // Skapa en instans av skeppn
            Skepp skepp = new Skepp("./resurser/spaceship.png");

            // Animationsloopen
            while (!Raylib.WindowShouldClose())
            {
                // Uppdatera skeppet

                // Rita bakgrunden
                Raylib.BeginDrawing();
                Raylib.ClearBackground(Color.BLACK);

                // Rita skeppet
                skepp.Rita();

                Raylib.EndDrawing();
            }
        }
    }
}
```

![](/.gitbook/assets/raylib/shmup/Shmup-1.png)

## Styra skeppet

Vi gör så att att du flytta skeppet åt vänster och höger med piltangenterna.  
Skeppet ska inte kunna lämna fönstret, det ska stanna vid kanten till vänster och höger.

### Klassen Uppdatera()

Vi skapar en klass som heter `Uppdatera()` som lyssnar efter tangenttryck och uppdaterar skeppets position.

```csharp
    // Styra skeppet
    public void Uppdatera()
    {
        // Rör skeppet med piltangenterna
        if (Raylib.IsKeyDown(KeyboardKey.KEY_LEFT))
        {
            _position.X -= 5;
        }
        if (Raylib.IsKeyDown(KeyboardKey.KEY_RIGHT))
        {
            _position.X += 5;
        }
        if (Raylib.IsKeyDown(KeyboardKey.KEY_UP))
        {
            _position.Y -= 5;
        }
        if (Raylib.IsKeyDown(KeyboardKey.KEY_DOWN))
        {
            _position.Y += 5;
        }
    }
```

### I Main

Vi kallar på `Uppdatera()` i `Main()`. Och nu kan du styra skeppet med piltangenterna.

```csharp
// Animationsloopen
while (!Raylib.WindowShouldClose())
{
    // Uppdatera skeppet
    skepp.Uppdatera();

    // Rita bakgrunden
    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.BLACK);

    // Rita skeppet
    skepp.Rita();

    Raylib.EndDrawing();
}
```

### Begränsa skeppets rörelse

Vi vill att skeppet inte ska kunna åka utanför fönstret.  
Vi lägger till if-satser för att kolla om skeppet är på väg att åka utanför fönstret. 

```csharp
// Styra skeppet
public void Uppdatera()
{
    ...

    // Skeppet kan inte åka utanför scenen
    if (_position.X < 0)
    {
        _position.X = 0;
    }
    if (_position.X > 800 - _bild.width)
    {
        _position.X = 800 - _bild.width;
    }
    if (_position.Y < 0)
    {
        _position.Y = 0;
    }
    if (_position.Y > 600 - _bild.height)
    {
        _position.Y = 600 - _bild.height;
    }
}
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class skepp {
    -_bild : Texture2D
    -_position : Vector2
    -_antalLiv : int
    -_antalSkott : int
    +skepp(string texturFil) : void
    +Rita() : void
    +Uppdatera() : void
    +Liv : int
}
@enduml
```

## Skjuta skott

Vi vill att du ska kunna skjuta skott med tangenten **space**.  
För det måste vi lyssna efter om tangenten är nedtryckt och skapa ett nytt skott.

### Utökar metoden Uppdatera()

Vi lägger till en if-sats som kollar om tangenten **space** är nedtryckt.  
OBS! Vi använder `IsKeyPressed()` istället för `IsKeyDown()` för att vi bara vill att skottet ska skjutas en gång.

```csharp
// Styra skeppet
public void Uppdatera()
{
    ...

    // Skjut skott
    if (Raylib.IsKeyPressed(KeyboardKey.KEY_SPACE))
    {
        // Rita ut ett skott
        Raylib.DrawCircle((int)_position.X + 32, (int)_position.Y, 5, Color.RED);
    }
}
```

## Förbättringar

### Förbättring 1

* Vi kan anpassa utseendet på skeppet efter om vi trycker på vänster eller höger piltangent.  
* Lägg till en bild för skeppet som åker åt höger och en bild för skeppet som åker åt vänster.

### Förbättring 2

* Lägg till ett explosion när du skjuter.  
* Vi kan använda [pico-8](https://pico-8-edu.com) för att rita sprites. För att exportera spritegrafiken  skriver man i terminalen `export sprite.png`.
* Man kan också prova med [piskel](https://www.piskelapp.com/p/create/sprite)

### Förbättring 3

* Animera en raketflamma efter skeppet med sprites.

Att implementera sprites animationer är lite mer avancerat och kräver lite mer kod. Såhär kan man göra:

```csharp
// För fördröjning mellan varje sprite kan du använda en timer
float _timer = 0;
int _spriteIndex = 0;

// Ladda in flera sprites som är del av en animation
Texture2D[] _sprites = new Texture2D[4];
for (int i = 0; i < _sprites.Length; i++)
{
    _sprites[i] = Raylib.LoadTexture($"flame{i}.png");
}

// I Update()
_timer += Raylib.GetFrameTime();

// Uppdatera _spriteIndex
// Om _timer är större än 0.1 sekunder
if (_timer > 0.1f)
{
    _timer = 0;
    _spriteIndex++;

    // Om _spriteIndex är större än längden på _sprites arrayen
    if (_spriteIndex >= _sprites.Length)
    {
        _spriteIndex = 0;
    }
}

// Rita ut en sprite
Raylib.DrawTexture(_sprites[_spriteIndex], 0, 0, Color.WHITE);
```
