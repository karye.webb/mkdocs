---
description: Raylib med OOP
---

# Spelet månlandare

## Resultat

![Månlandare](/.gitbook/assets/raylib/manlandare/lander.png)

## Beskrivning

Vi ska bygga ett arkadspel där spelaren kontrollerar en månlandare som försöker landa på månen. Det finns också en bakgrundsbild och en poängräknare (Scen).  
Spelet använder **Raylib_cs** för grafik och inputhantering och använder **OOP** för att organisera koden.

Spelet kan ha med följande:

* Spelaren styr månlandaren med piltangenterna
* Månlandaren måste landa på en platt yta
  * Visa en marklinje
  * Skriv ut höjden på månlandaren
  * Skriv ut hastigheten på månlandaren
* Månlandaren får inte lämna scenen, om den åker ut på sidorna så åker den tillbaka till andra sidan
* Det finns en begänsad mängd bränsle
  * Visa bränsle som en stapel
* Gravitation måste funka, dvs falla snabbare och snabbare
* Landa mjukt annars kraschar och förlora spelet
* Landa på ett speciellt ställe
* Lägga till tid, 1 min på sig att landa annars Game Over
* Plocka bränsle som ligger slumpmässigt på scenen
* Landaren måste undvika asteroider som faller slumpmässigt från himlen