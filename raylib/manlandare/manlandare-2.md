---
description: Månlandare med OOP
---

# Styra månlandaren

## Inledning

I denna uppgift ska vi styra månlandaren som vi skapat i föregående uppgift.  
Med piltangenterna ska vi kunna styra månlandaren åt vänster och höger.   
Med mellanslag ska vi kunna starta raketmotorn och få månlandaren att bromsa in och landa säkert.  
Vi använder klassen `Landare` som vi skapade i föregående uppgift.

Från föregående uppgift har vi ett klassdiagram som ser ut så här:

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Landare {
    -_position : Vector2
    -_textur : Texture2D
    -_höjd : float
    -_hastighet : float
    +Landare(int x, int y, string texturFil, float höjd, float hastighet) : void
    +Fall(int måneHöjd) : void
    +Rita() : void
    +RitaHöjdHastighet() : string
    +Landat(int måneHöjd) : bool
}
@enduml
```

### Styra landaren

Vi ska nu lägga till en metod i klassen `Landare` som heter `Input()` som hanterar inmatning från tangentbordet.

```csharp
class Landare
{
    ...

    // Metod för att hantera inmatning från tangentbordet
    public void Input()
    {
        // Om vänsterpil trycks ner
        if (Raylib.IsKeyDown(KeyboardKey.KEY_LEFT))
        {
            // Flytta landaren åt vänster
            _position.X -= 1;
        }

        // Om högerpil trycks ner
        if (Raylib.IsKeyDown(KeyboardKey.KEY_RIGHT))
        {
            // Flytta landaren åt höger
            _position.X += 1;
        }

        // Om mellanslag trycks ner
        if (Raylib.IsKeyDown(KeyboardKey.KEY_SPACE))
        {
            // Starta raketmotorn
            _hastighet -= 0.05f;
        }
    }
}
```

Vi anropar metoden `Input()` i metoden `Faller()` i klassen `Landare` så att inmatningen hanteras varje gång landaren faller.

```csharp
class Landare
{
    ...

    // Metod för att landaren ska falla
    public void Fall(int måneHöjd)
    {
        // Kontrollera om landaren landat på månen
        if (Landat(måneHöjd))
        {
            // Om landaren landat så ska den inte falla längre
            _hastighet = 0;
        }
        else
        {
            // Kolla om användaren har tryckt på en tangent för att styra landaren
            Input();

            // Om landaren inte landat så ska den fortsätta falla
            _höjd -= _hastighet;
            _hastighet += 0.01f;
        }

        // Räkna ut y-positionen när marken är noll och positiv höjd är uppåt
        _position.Y = 600 - _höjd - _textur.height;
    }
}
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Landare {
    -_position : Vector2
    -_textur : Texture2D
    -_höjd : float
    -_hastighet : float
    +Landare(int x, int y, string texturFil, float höjd, float hastighet) : void
    +Fall(int måneHöjd) : void
    +Rita() : void
    +RitaHöjdHastighet() : string
    +Landat(int måneHöjd) : bool
    +Input() : void
}
@enduml
```

## Förbättringar

### Förbättring 1

Testa olika värden på hastigheten när raketmotorn kölrs för se vad som ger bästa resultat.  
Rita ut en varning på scenen om landaren faller för fort.  
Rita ut en varning på scenen när landaren närmar sig marken.

### Förbättring 2

Lägg till en variabel för bränsle i klassen `Landare`.  I konstruktorn ska bränsle sättas till tex 100.
När raketmotorn körs ska bränsle minskas.  
Rita ut en stapel på scenen som visar hur mycket bränsle som finns kvar.

### Förbättring 3

När bränsle är tömt ska raketmotorn inte kunna startas.
Skriv ut en varning på scenen.

### Förbättring 4

Rita ut en raketflamma när raketmotorn körs.  
Du kan använda en bild för flamman eller rita ut ett par rektanglar eller trianglar med `Raylib.DrawTriangle()`.    
Rita även ut små raketflammor för sidoraketerna, dvs när landaren rör sig åt vänster eller höger.

![](/.gitbook/assets/raylib/manlandare/Moonlander-5.png)

### Förbättring 5

Styrningen i sidled ska vara mer realistisk.  
Skapa en variabel för hastigheten i x-led och y-led: `Vector2 _hastighet`
Skriv om koden så piltangenterna vänster och höger ändrar hastigheten i x-led.  
