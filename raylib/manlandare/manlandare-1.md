---
description: Månlandare med OOP
---

# Månlandare

## Inledning

I denna uppgift ska vi skapa ett spel där vi styr en månlandare.
För att koda en månlandare kan vi skapa en ny klass som heter `Landare`.  
I klassen kan vi lägga till egenskaper och metoder som är specifika för månlandaren, till exempel en egenskap för höjd och en metod för att simulera fall till månens yta.  

## Raylib grundkod

Vi börjar med att skapa en grundkod för att kunna använda Raylib.

```csharp
using System;
using Raylib_cs;

namespace MoonLander
{
    class Program
    {
        static void Main(string[] args)
        {
            Raylib.InitWindow(800, 600, "Månlandning");
            Raylib.SetTargetFPS(60);

            // Skapa objekt här

            while (!Raylib.WindowShouldClose())
            {
                // Uppdatera här


                Raylib.BeginDrawing();
                Raylib.ClearBackground(Color.BLACK);

                // Rita ut här

                Raylib.EndDrawing();
            }
        }
    }
}
```

## Objekten

Vi skall rita ut en månlandare.

### Bilderna

Välj en bild på en månlandare på [FlatIcon](https://www.flaticon.com/). Ladda ned storlek 64px och spara bilden i en mapp som heter **resurser**.

### Månlandaren

Vi skapa ett objekt för månlandaren. Objektet håller reda på figurens utseende och position.  
Objektet ska ha en egenskap för att hålla reda på höjden och en metod för att simulera lyft från marken.
Klassen skall heta `Landare` och ligga i en fil som heter `Landare.cs`.

```csharp
using System;
using Raylib_cs;
using System.Numerics;

class Landare
{
    // Egenskaper för landaren
    private Vector2 _position;
    private Texture2D _textur;
    private float _höjd;
    private float _hastighet;

    // Konstruktor för att skapa en landare med startvärden
    public Landare(int x, int y, string texturFil, float höjd, float hastighet)
    {
        _position.X = x;
        _position.Y = y;
        _textur = Raylib.LoadTexture(texturFil);
        _höjd = höjd;
        _hastighet = hastighet;
    }

    // Rita-metoden (anropas varje bildruta)
    public void Rita()
    {
        // Rita ut landaren här
        // Y
        Raylib.DrawTexture(_textur, (int)_position.X, (int)_position.Y, Color.WHITE);
    }
}
```

#### Gravitationen

Vi skall simulera gravitationen genom att lägga till en metod som heter `Faller()`.  
I metoden skall vi räkna ut hur mycket landaren faller varje bildruta.  
Vi skall också räkna ut landarens y-position på fönstret.

```csharp
using System;
using Raylib_cs;
using System.Numerics;

class Landare
{
    ...

    // Metod för att simulera gravitation
    public void Faller()
    {
        _höjd -= _hastighet;
        _hastighet += 0.01f;

        // Räkna ut y-positionen när marken är noll och positiv höjd är uppåt
        _position.Y = 600 - _höjd - _textur.height;
    }
}
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Landare {
    -_position : Vector2
    -_textur : Texture2D
    -_höjd : float
    -_hastighet : float
    +Landare(int x, int y, string texturFil, float höjd, float hastighet) : void
    +Faller() : void
    +Rita() : void
}
@enduml
```

### Rita ut månlandaren

I `Main`-metoden skall vi skapa en landare och sedan anropa `Rita`-metoden varje bildruta.

```csharp
using System;
using Raylib_cs;

static void Main(string[] args)
{
    Raylib.InitWindow(800, 600, "Månlandning");
    Raylib.SetTargetFPS(60);

    // Skapa en landare på position 100, 100 med startvärden
    Landare landare = new Landare(100, 100, "./resurser/lander.png", 100, 0);

    while (!Raylib.WindowShouldClose())
    {
        // Simulera fall från ovan
        landare.Faller();

        Raylib.BeginDrawing();
        Raylib.ClearBackground(Color.BLACK);

        // Rita ut landaren
        landare.Rita();

        Raylib.EndDrawing();
    }

    Raylib.CloseWindow();
}
```

![](/.gitbook/assets/raylib/manlandare/Moonlander-1.png)

### Höjd och hastighet

Vi ska nu skriva ut landarens höjd och hastighet i fönstret.  
Vi lägger till en metod som heter `RitaHöjdHastighet()` som returnerar en sträng med höjd och hastighet.

```csharp
class Landare
{
    ...

    // Metod för att skriva ut höjd och hastighet
    // Skriv med 1 decimal
    public void RitaHöjdHastighet()
    {
        Raylib.DrawText($"Höjd: {_höjd:0} m", 10, 10, 20, Color.WHITE);
        Raylib.DrawText($"Hastighet: {_hastighet:0.0} m/s", 10, 40, 20, Color.WHITE);
    }
}
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Landare {
    -_position : Vector2
    -_textur : Texture2D
    -_höjd : float
    -_hastighet : float
    +Landare(int x, int y, string texturFil, float höjd, float hastighet) : void
    +Faller() : void
    +Rita() : void
    +RitaHöjdHastighet() : string
}
@enduml
```

Vi anropar denna metod i `Main`-metoden och skriver ut strängen i fönstret.

```csharp
using System;
using Raylib_cs;

static void Main(string[] args)
{
    Raylib.InitWindow(800, 600, "Månlandning");
    Raylib.SetTargetFPS(60);

    // Skapa en landare på position 100, 100 med startvärden
    Landare landare = new Landare(100, 100, "./resurser/lander.png", 100, 0);

    while (!Raylib.WindowShouldClose())
    {
        // Simulera fall från ovan
        landare.Faller();

        Raylib.BeginDrawing();
        Raylib.ClearBackground(Color.BLACK);

        // Rita ut landaren
        landare.Rita();

        // Skriv ut höjd och hastighet
        landare.RitaHöjdHastighet();

        Raylib.EndDrawing();
    }

    Raylib.CloseWindow();
}
```

![](/.gitbook/assets/raylib/manlandare/Moonlander-2.png)

### Landning

Vi skall nu lägga till en metod för att kontrollera om landaren landat på månen.  
Vi anger en parameter `måneHöjd` som är höjden på månen. Dvs hur högt ovanför nivå 0 meter månen är.
    
```csharp
class Landare
{
    ...

    // Metod för att kontrollera om landaren landat på månen
    public bool Landat(int måneHöjd)
    {
        // Om landarens höjd är noll
        if (_höjd <= måneHöjd)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
```

Vi använder detta i metoden `Faller()` för att kontrollera om landaren landat på månen.

```csharp
class Landare
{
    ...

    // Metod för att simulera gravitation
    public void Fall(int måneHöjd)
    {
        // Kontrollera om landaren landat på månen
        if (Landat(måneHöjd))
        {
            // Om landaren landat så ska den inte falla längre
            _hastighet = 0;
        }
        else
        {
            // Om landaren inte landat så ska den fortsätta falla
            _höjd -= _hastighet;
            _hastighet += 0.01f;
        }

        // Räkna ut y-positionen när marken är noll och positiv höjd är uppåt
        _position.Y = 600 - _höjd - _textur.height;
    }
}
```

Såja nu har vi en landare som kan landa på månen.

![](/.gitbook/assets/raylib/manlandare/Moonlander-3.png)

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Landare {
    -_position : Vector2
    -_textur : Texture2D
    -_höjd : float
    -_hastighet : float
    +Landare(int x, int y, string texturFil, float höjd, float hastighet) : void
    +Fall(int måneHöjd) : void
    +Rita() : void
    +RitaHöjdHastighet() : string
    +Landat(int måneHöjd) : bool
}
@enduml
```

Vi ritar ut marken på månen. Vi kan göra det med en rektangel eller med en bild.

![](/.gitbook/assets/raylib/manlandare/Moonlander-4.png)

## Förbättringar

### Förbättring 1


Vi skall nu rita ut en stjärna. Vi skapar ett objekt för stjärnan och lägger till en egenskap för att hålla reda på stjärnans position.  Ladda ned en bild på en stjärna och spara den i mappen **resurser**.
Skapa en klass som heter `himlakropp` och lägg den i en fil som heter `himlakropp.cs`.

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Himlakropp {
    -_position : Vector2
    -_textur : Texture2D
    +Himlakropp(int x, int y, string texturFil) : void
    +Rita() : void
}
@enduml
```

### Förbättring 2

Använd klassen `Himlakropp` för att skapa månens mark som månlandaren ska landa på.  
Ladda ned en bild på en måne och spara den i mappen **resurser**.  

### Förbättring 3

Skapa en ny konstruktor för `Landare` som slumpar fram en position och en höjd.

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Landare {
    -_position : Vector2
    -_textur : Texture2D
    -_höjd : float
    -_hastighet : float
    +Landare(int x, int y, string texturFil, float höjd, float hastighet) : void
    +Landare(string texturFil) : void
    +Fall(int måneHöjd) : void
    +Rita() : void
    +RitaHöjdHastighet() : string
    +Landat(int måneHöjd) : bool
}
@enduml
```

