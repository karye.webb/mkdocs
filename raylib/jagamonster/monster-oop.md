---
description: Raylib med OOP
---

# Skapa objekten med klasser

## Resultat

### Nytt projekt JagaMonster

Skapa en ny projektmapp **JagaMonster-1**.  
Här ska vi skapa ett Raylib-projekt.

```csharp
dotnet new console
dotnet add package Raylib-cs
```

### Bilder

Skapa en mapp **resurser** och infoga följande bilder:  
{% file src="/.gitbook/assets/raylib/jagamonster/resurser.zip" %}

## Objekten

### Figur

Vi skapar en klass för spelfiguren.  
Med en konstruktor för att ladda in grafik och sätta positionen.  
Och en metod `Rita()` för utritning i Raylib:

```csharp
class Figur
{
    public Texture2D heroTex;
    public Rectangle heroRec;

    // Initierar figurens grafik och position
    public Figur()
    {
        heroTex = Raylib.LoadTexture(@"resurser/hero.png");
        heroRec = new Rectangle(0, 0, heroTex.width, heroTex.height);
    }

    // Ritar ut figur
    public void Rita()
    {
        Raylib.DrawTexture(heroTex, 0, 0, Color.WHITE);
    }
}
```

#### Main

Vi implementerar `Figur()` i Main och ritar ut i animationsloopen:

```csharp
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Hello objekt!");

        // Initialisera Raylib
        Raylib.InitWindow(512, 480, "Jaga monster");
        Raylib.SetTargetFPS(30);

        /** Initiering **/
        Figur figur = new Figur();

        // Animationsloopen
        while (!Raylib.WindowShouldClose())
        {
            /** Uppdatering **/

            // Starta ritning
            Raylib.BeginDrawing();
            Raylib.ClearBackground(Color.WHITE);

            /** Utritning **/
            figur.Rita();

            // Stäng av ritning
            Raylib.EndDrawing();
        }
    }
}
```

Det funkar!

![](/.gitbook/assets/raylib/jagamonster/image-15.png)

### Scen

Det ser lite tomt ut. Vi skapar en scen med bakgrundsbilden.    
Vi skapar en klass för scenen.  
Med samma delar som ovan, en konstruktor och en metod för att rita ut:

```csharp
class Scen
{
    public Texture2D scenTex;
    public Rectangle scenRec;

    public Scen()
    {
        scenTex = Raylib.LoadTexture(@"resurser/scen.png");
        scenRec = new Rectangle(0, 0, scenTex.width, scenTex.height);
    }

    public void Rita()
    {
        Raylib.DrawTexture(scenTex, 0, 0, Color.WHITE);
    }
}
```

#### Main

Vi implementerar även klassen `Scen()` i Main:

```csharp
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Hello objekt!");

        // Initialisera Raylib
        Raylib.InitWindow(512, 480, "Jaga monster");
        Raylib.SetTargetFPS(30);

        /** Initiering **/
        Figur figur = new Figur();
        Scen scen = new Scen();

        // Animationsloopen
        while (!Raylib.WindowShouldClose())
        {
            /** Uppdatering **/

            // Starta ritning
            Raylib.BeginDrawing();
            Raylib.ClearBackground(Color.WHITE);

            /** Utritning **/
            scen.Rita();
            figur.Rita();

            // Stäng av ritning
            Raylib.EndDrawing();
        }
    }
}
```

Det ser bättre ut:

![](/.gitbook/assets/raylib/jagamonster/image-16.png)

## Förbättringar

### Förbättring 1

* Utöka klassen Scen med ett hus och en damm
* Rita ut alla delar så det ser ut som bilden nedan

![](/.gitbook/assets/raylib/jagamonster/image-14.png)
