---
description: Raylib med OOP
---

# Förflyttningar

För att få figuren att röra sig behöver vi ändra på dess position. Vi gör det genom att lägga till en metod som heter `Uppdatera()` i klassen `Figur`.

## Uppdateringsmetoden

I metoden `Uppdatera()` ändrar vi på figurens position. Vi gör det genom att lägga till ett värde till `x` och `y`-koordinaten. Vi kan också ändra på värdena genom att subtrahera ett värde.

```csharp
// Uppdatera-metoden (anropas varje bildruta)
public void Uppdatera()
{
    // Ändra på figurens position här
    _position.X += 1;
    _position.Y += 1;
}
```


```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Figur {
    - _position : Vector2
    - _textur : Texture2D
    + Figur(x:int, y:int, texturFil:string)
    + Figur(texturFil:string)
    + Rita() : void
    + Uppdatera() : void
}
@enduml
```

### Använda uppdateringsmetoden

Vi använder metoden `Uppdatera()` i spelets huvudloop. Vi gör det före metoden `Rita()`.

```csharp
while (!Raylib.WindowShouldClose())
{
    // Uppdatera figurens position
    hero.Uppdatera();

    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.DARKGREEN);

    // Rita ut figuren
    hero.Rita();

    Raylib.EndDrawing();
}
```

## Stanna på scenen

Vi vill att figuren ska stanna på scenen. Vi gör det genom att kontrollera att figurens position inte är utanför scenens bredd och höjd.

```csharp
// Uppdatera-metoden (anropas varje bildruta)
public void Uppdatera()
{
    // Ändra på figurens position här
    _position.X += 1;
    _position.Y += 1;
    
    // Studsa mot väggar
    if (_position.X > 512 - _textur.width)
    {
        _position.X = 512 - _textur.width;
    }
    if (_position.Y > 480 - _textur.height)
    {
        _position.Y = 480 - _textur.height;
    }
}
```

Figurerna fastnar nu vid scenens kant:

![Figurerna fastnar](/.gitbook/assets/raylib/jagamonster/JagaMonster-10.png)

### Studsa mot väggar

Vi vill att figuren ska studsa mot väggar. Vi gör det genom att ändra på riktningen när den når scenens kant.  
Dvs genom att subtrahera ett värde istället för att addera.  
För det här behövs två nya variabler `_hastighetX` och `_hastighetY`.

```csharp
// Variabler
private Texture2D _textur;
private vector2 _position;
private vector2 _hastighet;
Random _slump = new Random();

// Konstruktor för att initiera figuren slumpmässigt
public Figur(string texturFil)
{
    _textur = Raylib.LoadTexture(@texturFil);

    // Generera slumpmässig x- och y-position
    int _position.X = _slump.Next(0, 512);
    int _position.Y = _slump.Next(0, 480);

    // Initiera hastigheten i x- och y-led
    _hastighet.X = 1;
    _hastighet.Y = 1;
}

// Uppdatera-metoden (anropas varje bildruta)
public void Uppdatera()
{
    // Ändra på figurens position här
    _position.X += _hastighet.X;
    _position.Y += _hastighet.Y;

    // Studsa mot väggar
    if (_position.X > 512 - _textur.width)
    {
        _hastighet.X *= -1;
    }
    if (_position.Y > 480 - _textur.height)
    {
        _hastighet.Y *= -1;
    }
}
```

Sådär, nu studsar figuren mot väggarna höger och nedåt.


```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Figur {
    - _position : Vector2
    - _hastighet : Vector2
    - _textur : Texture2D
    + Figur(x:int, y:int, texturFil:string)
    + Figur(texturFil:string)
    + Rita() : void
    + Uppdatera() : void
}
@enduml
```

## Förbättringar

### Förbättring 1

* Gör så att figurerna studsar mot övriga väggar också.  
  Vi gör det genom att ändra på riktningen när den når scenens vänstra och övre kant.

### Förbättring 2

* Lägg till 25 figurer i spelet.  
  Vi gör det genom att skapa en List<> med figurer.