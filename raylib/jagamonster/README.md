---
description: Raylib med OOP
---

# Spelet Jaga Monster

## Resultat

![Jaga monster](/.gitbook/assets/raylib/jagamonster/JagaMonster-5.png)

## Beskrivning

Vi ska bygga ett arkadspel där spelaren kontrollerar en hjälte som försöker undvika kollision med andra figurer (monster mm) och hinder (hus och sjö) på skärmen. Det finns också en bakgrundsbild och en poängräknare (Scen).   
Spelaren jagar monster med hjälten och får poäng för varje träff. Monstret dör och spawnar om på en ny position.  
Spelet använder **Raylib_cs** för grafik och inputhantering.