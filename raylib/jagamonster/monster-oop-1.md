---
description: Raylib med OOP
---

# Skapa objekten

## Nytt projekt JagaMonster

Skapa en ny projektmapp **JagaMonster**.  
Här ska vi skapa ett Raylib-projekt.

```csharp
dotnet new console
dotnet add package Raylib-cs
```

### Grunkoden i Raylib

Vi skapar ett fönster med titeln "JagaMonster" som har en bredd på 512 pixlar och en höjd på 480 pixlar. Programmet ställer in FPS till 60 och går sedan in i en loop som fortsätter tills användaren stänger fönstret.

Inuti loopen anropar programmet `Raylib.BeginDrawing()` för att börja rita på fönstret, `Raylib.ClearBackground(Color.DARKGREEN)` för att ställa in bakgrundsfärgen till vitt och `Raylib.EndDrawing()` för att avsluta ritandet på fönstret.

```csharp
using System;
using Raylib_cs;

namespace JagaMonster
{
    class Program
    {
        static void Main(string[] args)
        {
            Raylib.InitWindow(512, 480, "Jaga Monster");
            Raylib.SetTargetFPS(60);

            // Skapa objekt här

            while (!Raylib.WindowShouldClose())
            {
                Raylib.BeginDrawing();
                Raylib.ClearBackground(Color.DARKGREEN);

                // Rita ut här

                Raylib.EndDrawing();
            }
        }
    }
}
```


## Objekten

Vi skall rita ut ett par figurer i spelet. Vi skapar ett objekt för varje figur.  
Objekten håller reda på figurens utseeende och position.

### Bilderna

Ladda ned, packa upp mappen **resurser** och lägg mappen i samma mapp som projektet:  
{% file src="/.gitbook/assets/raylib/jagamonster/resurser.zip" %}

### Klassen Figur

Vi skapar en klass `Figur()` för figurerna.  
Med en konstruktor för att ladda in grafik och sätta positionen.  
Och en metod `Rita()` för utritning i Raylib:

```csharp
// Figur-klass
class Figur
{
    // Egenskaper för figuren
    private Vector2 _position;
    private Texture2D _textur;

    // Konstruktor för att skapa en figur med startvärden
    public Figur(int x, int y, string texturFil)
    {
        _position.X = x;
        _position.Y = y;
        _textur = Raylib.LoadTexture(texturFil);
    }

    // Rita-metoden (anropas varje bildruta)
    public void Rita()
    {
        // Rita ut figuren här
        Raylib.DrawTexture(_textur, _Position.X, _Position.Y, Color.WHITE);
    }
}
```


```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Figur {
    - _position : Vector2
    - _textur : Texture2D
    + Figur(x:int, y:int, texturFil:string)
    + Rita() : void
}
@enduml
```

## Rita ut figurer

Med klassen `Figur()` kan vi skapa objekt för varje figur.

### Figuren hero

Vi skapar ett objekt för figuren **hero**.  
Vi anger startpositionen och vilken bild som ska användas.  
Vi anropar sedan metoden `Rita()` för att rita ut figuren.

```csharp
static void Main(string[] args)
{
    Raylib.InitWindow(512, 480, "Jaga Monster");
    Raylib.SetTargetFPS(60);

    // Skapa en figur på position 100, 100
    Figur hero = new Figur(100, 100, "./resurser/hero.png");

    while (!Raylib.WindowShouldClose())
    {
        Raylib.BeginDrawing();
        Raylib.ClearBackground(Color.DARKGREEN);

        // Rita ut figuren
        hero.Rita();

        Raylib.EndDrawing();
    }
}
```

Nu ser vi figuren hero på skärmen:

![Hero](/.gitbook/assets/raylib/jagamonster/JagaMonster-1.png)

### Figuren monster

Vi skapar ett till objekt för figuren **monster**.

```csharp
static void Main(string[] args)
{
    Raylib.InitWindow(512, 480, "Jaga Monster");
    Raylib.SetTargetFPS(60);

    // Skapa en figur på position 100, 100
    Figur hero = new Figur(100, 100, "./resurser/hero.png");
    Figur monster = new Figur(200, 200, "./resurser/monster.png");

    while (!Raylib.WindowShouldClose())
    {
        Raylib.BeginDrawing();
        Raylib.ClearBackground(Color.DARKGREEN);

        // Rita ut figurer
        hero.Rita();
        monster.Rita();

        Raylib.EndDrawing();
    }
}
```

Nu ser vi både figuren hero och monster på skärmen:

![Hero och monster](/.gitbook/assets/raylib/jagamonster/JagaMonster-2.png)

## Slumpa positionen

Vi vill att figuren monster ska slumpas fram på en slumpad position.  
Vi skapar en till konstruktor som skapar slumpade värden för x och y.

```csharp
// Figur-klass
class Figur
{
    // Konstruktor för att skapa en figur med startvärden
    public Figur(int x, int y, string texturFil)
    {
        _Position.X = x;
        _Position.Y = y;
        _textur = Raylib.LoadTexture(texturFil);
    }

    // Konstruktor för att skapa en figur med slumpade värden
    public Figur(string texturFil)
    {
        // Skapa en random-generator
        Random slump = new Random();

        // Slumpa fram x och y
        _position.X = slump.Next(0, 512);
        _position.Y = slump.Next(0, 480);

        _textur = Raylib.LoadTexture(texturFil);
    }
    ...
}
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Figur {
    - _position : Vector2
    - _textur : Texture2D
    + Figur(x:int, y:int, texturFil:string)
    + Figur(texturFil:string)
    + Rita() : void
}
@enduml
```

Vi skapar ett nytt objekt för figuren **monster2** med den nya konstruktorn:

```csharp
static void Main(string[] args)
{
    Raylib.InitWindow(512, 480, "Jaga Monster");
    Raylib.SetTargetFPS(60);

    // Skapa en figur på position 100, 100
    Figur hero = new Figur(100, 100, "./resurser/hero.png");
    Figur monster = new Figur(200, 200, "./resurser/monster.png");
    Figur monster2 = new Figur("./resurser/monster2.png");

    while (!Raylib.WindowShouldClose())
    {
        Raylib.BeginDrawing();
        Raylib.ClearBackground(Color.DARKGREEN);

        // Rita ut figurer
        hero.Rita();
        monster.Rita();
        monster2.Rita();

        Raylib.EndDrawing();
    }
}
```

Nu ser vi figuren monster2 på en slumpad position:

![Hero, monster och monster2](/.gitbook/assets/raylib/jagamonster/JagaMonster-3.png)

## Förbättringar

### Förbättring 1

* Lägg till ett hus i scenen.
* Lägg till en sjö i scenen.

![Hus och sjö](/.gitbook/assets/raylib/jagamonster/JagaMonster-4.png)

### Förbättring 2

* Lägg till bakgrunden i scenen.

![Figurer mot en bakgrund](/.gitbook/assets/raylib/jagamonster/JagaMonster-5.png)

### Förbättring 3

* Scenen är 512 x 480 pixlar, men figurer måste hålla sig 35 pixlar från alla kanter.
* Det finns olika sätt att se till att figurerna håller sig 35 pixlar från alla kanter av scenen. Ett sätt är att när du slumpar fram x- och y-koordinaterna för varje figur, begränsa det till ett område som är 35 pixlar från kanterna av scenen. Det kan göras genom att använda metoden `Next()` från klassen `Random` och ange värdena för det minsta och det högsta värdet som figuren får ha.

### Förbättring 4

* Slumpa ut hundra figurer på scenen.
* För att slumpa fram 100 figurer och rita ut dem på scenen, kan du skapa en lista av figurer och anropa konstruktorn med slumpade värden i en loop.
* Notera att du behöver en loop för att rita ut figurerna efter de skapats, och den loopen bör ske inne i Raylib-loopen där du uppdaterar vyn för din applikation.

![100 monster](/.gitbook/assets/raylib/jagamonster/JagaMonster-6.png)
