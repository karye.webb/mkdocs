---
description: Raylib med OOP
---

# Kollisioner

För att gå vidare i spelet behöver vi kontrollera om figuren kolliderar med en annan figur. Vi gör det genom att använda `Rectangle`-objekt. Vi tilldelar figuren ett `Rectangle`-objekt. Vi kan sedan använda `Rectangle`-objektet för att kontrollera om figuren kolliderar med en annan figur.

## Rectangle-objekt

Vi skapar ett `Rectangle`-objekt i klassen `Figur`. Vi gör det i konstruktorn.  
Vi använder `Rectangle`-objektets x och y-koordinater istället för figurens egna variabler `_x` och `_y`. Vi använder också `Rectangle`-objektets bredd och höjd istället för figurens egna bredd och höjd.

### Konstruktor nr 1 

Vi byter ut figurens egna variabler `_x` och `_y` mot `Rectangle`-objektets x och y-koordinater. Vi byter också ut figurens egna bredd och höjd mot `Rectangle`-objektets bredd och höjd.

```csharp
// Klassen figur
public class Figur
{
    // Egenskaper för figuren
    private Texture2D _textur;
    private Rectangle _hitbox;
    private vector2 _hastighet;

    // Konstruktor för att initiera figuren
    public Figur(int x, int y, string texturFil)
    {
        _textur = Raylib.LoadTexture(@texturFil);
        _hitbox = new Rectangle(x, y, _textur.width, _textur.height);

        // Initiera hastigheten i x- och y-led
        _hastighet.X = 1;
        _hastighet.Y = 1;
    }

    ...
```

### Konstruktor nr 2

Även här byter vi ut figurens egna variabler `_x` och `_y` mot `Rectangle`-objektets `x` och `y`-koordinater. Vi byter också ut figurens egna bredd och höjd mot `Rectangle`-objektets bredd och höjd.

```csharp
    ...

    // Konstruktor för att initiera figuren slumpmässigt
    public Figur(string texturFil)
    {
        // Skapa en random-generator
        Random random = new Random();

        // Generera slumpmässig x- och y-position
        int x = random.Next(0, 512);
        int y = random.Next(0, 480);

        _textur = Raylib.LoadTexture(@texturFil);
        _hitbox = new Rectangle(x, y, _textur.width, _textur.height);

        // Initiera hastigheten i x- och y-led
        _hastighet.X = 1;
        _hastighet.Y = 1;
    }
    ...
```

### Metoden Rita()

Vi byter ut figurens egna variabler `_x` och `_y` mot `Rectangle`-objektets `x` och `y`-koordinater. 

```csharp
    // Metod för att rita ut figuren
    public void Rita()
    {
        Raylib.DrawTexture(_textur, (int)_hitbox.x, (int)_hitbox.y, Color.WHITE);
    }
    ...
```

### Metoden Uppdatera()

Vi byter ut figurens egna variabler `_x` och `_y` mot `Rectangle`-objektets `x` och `y`-koordinater. Vi byter också ut figurens egna bredd och höjd mot `Rectangle`-objektets bredd och höjd.

```csharp
    // Metod för att uppdatera figurens position
    public void Uppdatera()
    {
        // Uppdatera figurens position
        _hitbox.x += _hastighet.X;
        _hitbox.y += _hastighet.Y;

        // Kontrollera om figuren kolliderar med vänster eller höger kant
        if (_hitbox.x < 0 || _hitbox.x + _hitbox.width > 512)
        {
            _hastighetX. *= -1;
        }

        // Kontrollera om figuren kolliderar med övre eller nedre kant
        if (_hitbox.y < 0 || _hitbox.y + _hitbox.height > 480)
        {
            _hastighet.Y *= -1;
        }
    }
}
```

## Förbättringar

### Förbättring 1

* Gör så att figurerna studsar inanför trädkanten runtom på scenen.

### Förbättring 2

* Gör så att monstren rör åt ett slumpmässigt håll när spelet startar.
