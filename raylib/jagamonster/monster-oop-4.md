---
description: Raylib med OOP
---

# Kollisioner

För att kontrollera om två figurer kolliderar med varandra kan du använda en kollisionskontrollfunktion. 

## Kollisionskontroll

Ett enkelt sätt att implementera detta kan vara att skapa en metod i `Figur()`-klassen som tar en annan figur som parameter och returnerar `true` om de kolliderar, annars `false`.

```csharp
// Klassen figur
public class Figur
{
    // Egenskaper för figuren
    private Texture2D _textur;
    private Rectangle _hitbox;
    private int _hastighetX;
    private int _hastighetY;

    // Konstruktor för att initiera figuren
    public Figur(int x, int y, string texturFil)
    {
        _textur = Raylib.LoadTexture(@texturFil);
        _hitbox = new Rectangle(x, y, _textur.width, _textur.height);

        // Initiera hastigheten i x- och y-led
        _hastighetX = 1;
        _hastighetY = 1;
    }

    ...

    // Metod för att kontrollera om två figurer kolliderar
    public bool KolliderarMed(Figur figur)
    {
        // Kontrollera om två figurer kolliderar
        if (Raylib.CheckCollisionRecs(_hitbox, figur._hitbox))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
```


```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Figur {
    -_textur : Texture2D
    -_hitbox : Rectangle
    -_hastighetX : int
    -_hastighetY : int
    +Figur(int x, int y, string texturFil) : void
    +Rita() : void
    +Flytta() : void
    +KolliderarMed(Figur figur) : bool
}
@enduml
```

### Studs vid kollision

För att få figurerna att studsa tillbaka när de kolliderar kan du ändra på deras hastighet. En enkel lösning kan vara att ändra tecken på hastigheten i den riktning där figuren kolliderar.

Här är ett exempel på hur du kan göra det i Figur-klassen:

```csharp
// Kollisionskontrollfunktion
public bool KolliderarMed(Figur annanFigur)
{
    // Om den här figurens rektangel inte krockar med den andra figurens rektangel, returnera false
    if (!Raylib.CheckCollisionRecs(_hitbox, annanFigur._hitbox))
    {
        return false;
    }

    // Om vi har kommit hit så krockar de, byt tecken på hastigheten i riktningen där de kolliderar
    if (_hitbox.x < annanFigur._hitbox.x)
    {
        // Kollision på vänster sida
        _hastighetX *= -1;
    }
    if (_hitbox.x + _hitbox.width > annanFigur._hitbox.x + annanFigur._hitbox.width)
    {
        // Kollision på höger sida
        _hastighetX *= -1;
    }
    if (_hitbox.y < annanFigur._hitbox.y)
    {
        // Kollision på övre sida
        _hastighetY *= -1;
    }
    if (_hitbox.y + _hitbox.height > annanFigur._hitbox.y + annanFigur._hitbox.height)
    {
        // Kollision på nedre sida
        _hastighetY *= -1;
    }

    // Returnera true eftersom de kolliderade
    return true;
}
```

![](/.gitbook/assets/raylib/jagamonster/JagaMonster-diagram-4.png)

### Använda kollisionskontroll

Och här är ett exempel på hur du kan använda metoden i `Main()`:

```csharp
...
while (!Raylib.WindowShouldClose())
{
    // Kollar efter kollisioner
    monster.KolliderarMed(hero);
    monster2.KolliderarMed(hero);
    monster.KolliderarMed(monster2);
    
    // Uppdatera figurerna
    hero.Uppdatera();
    monster.Uppdatera();
    monster2.Uppdatera();

    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.DARKGREEN);

    // Rita ut bakgrunden
    bakgrund.Rita();

    // Rita ut sjön
    sjö.Rita();

    // Rita ut huset
    hus.Rita();

    // Rita ut figuren
    hero.Rita();

    // Rita ut monster
    monster.Rita();
    monster2.Rita();

    Raylib.EndDrawing();
}
...
```

## Fel i kollisionskontrollen

Om figurerna träffar snett så studsar de inte tillbaka. Det beror på att på det hamnat på varandra. Detta kan vi lösa genom att spara den positionen som figuren hade innan den kolliderade. Då kan vi återställa den positionen om de kolliderar.

Vi skapar en ny variabel i `Figur`-klassen som heter `_positionFöreKollision` och sparar positionen innan vi uppdaterar positionen.

```csharp
// Figur-klass
class Figur
{
    // Egenskaper för figuren
    private Texture2D _textur;
    private Rectangle _hitbox;
    private Vector2 _hartighet;
    private Vector2 _positionFöreKollision;
    private Random _slump = new Random();

    ...
}
```

I `Uppdatera()`-metoden sparar vi positionen innan vi uppdaterar positionen.  
Och återställer positionen om figuren kolliderar med vägg: 

```csharp
    // Metod för att uppdatera figuren
    public void Uppdatera()
    {
        // Spara positionen innan vi uppdaterar
        _positionFöreKollision = new Vector2(_hitbox.x, _hitbox.y);

        // Ändra på figurens position här
        _hitbox.x += _hastighetX;
        _hitbox.y += _hastighetY;

        // Studsa mot väggar
        if (_hitbox.x > 512 - _textur.width)
        {
            _hastighet.X *= -1;
            _hitbox.x = _positionFöreKollision.X;
        }
        ...
    }
```

I `KolliderarMed()`-metoden återställer vi positionen om de kolliderar:

```csharp
    // Kollisionskontrollfunktion
    public bool KolliderarMed(Figur annanFigur)
    {
        // Om den här figurens rektangel inte krockar med den andra figurens rektangel, returnera false
        if (!Raylib.CheckCollisionRecs(_hitbox, annanFigur._hitbox))
        {
            return false;
        }

        // Om vi har kommit hit så krockar de, byt tecken på hastigheten i riktningen där de kolliderar
        // Kollision på vänster sida
        if (_hitbox.x < annanFigur._hitbox.x)
        {
            _hastighet.X *= -1;
            _hitbox.x = _positionFöreKollision.X;
        }
        ...

        // Returnera true eftersom de kolliderade
        return true;
    }
}
```

## Förbättringar

### Förbättring 1

Slumpa fram hastigheten för figuren när den skapas. Dvs att den rör sig i olika hastigheter och olika håll.

