---
description: Raylib med OOP
---

# Återanvända kod med arv

Klasserna `Figur` och `Hjälte` har mycket gemensamt. De har båda en textur och en rektangel. De har också båda en metod för att rita ut sig själva. Vi kan återanvända koden från `Figur` i `Hjälte` genom att använda arv.

## Ärva klass

I den här koden används arv genom att klassen `Hjälte` ärver från klassen `Figur`. Det betyder att `Hjälte` har alla egenskaper och metoder som finns i `Figur`, plus eventuella egenskaper och metoder som är specifika för `Hjälte`.

```csharp
// Figur-klass
public class Figur
{
    // Egenskaper och metoder för Figur...
}

// Klassen Hjälte ärver från klassen Figur
public class Hjälte : Figur
{
    // Egenskaper och metoder för Hjälte...
}
```


```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Figur {
    -_textur : Texture2D
    -_hitbox : Rectangle
    +Figur(int x, int y, string texturFil) : void
    +Rita() : void
    +Uppdatera() : void
}
class Hjälte {
    +Hjälte(int x, int y, string texturFil) : void
}
Figur <|-- Hjälte
@enduml
```

`Hjälte` ärver också alla egenskaper och metoder från `Figur`, såsom `_textur`, `_hitbox`, `_hastighet`, `_positionFöreKollision`, `_slump`, `Rita()` och `Uppdatera()`. Vi behöver inte skriva om dem i klassen `Hjälte`.

## Ärva konstruktor

I det här fallet kan du se att `Hjälte` har en konstruktor som tar emot tre argument: en x-position, en y-position och en sökväg till en texturfil. Dessa argument används för att initiera en instans av `Hjälte`.

Konstruktorn för `Hjälte` ser ut så här:

```csharp
// Konstruktor för att initiera hjälten
public Hjälte(int x, int y, string texturFil) : base(x, y, texturFil)
{
}
```

Observera att `base(x, y, texturFil)` anropar konstruktorn för `Figur` och skickar med de angivna argumenten `x`, `y` och `texturFil`. Detta initierar egenskaperna `_hitbox` och `_textur` för instansen av `Hjälte`.

Och i `Main` skapar vi en instans av `Hjälte` med hjälp av konstruktorn:

```csharp
Hjälte hero = new Hjälte(100, 100, "./resurser/hero.png");
```

## Ändra metoden Uppdatera()

Skillnaden mellan `Figur` och `Hjälte` är att `Hjälte`:ns position kan ändras med hjälp av tangenterna på tangentbordet. Vi kan återanvända koden för att uppdatera positionen från `Figur` i `Hjälte` genom att använda `base.Uppdatera()`.

### override

```csharp
// Uppdatera positionen
public override void Uppdatera()
{
    // Anropa metoden Uppdatera() i klassen Figur
    base.Uppdatera();

    // Ändra positionen med hjälp av tangenterna på tangentbordet
    if (Raylib.IsKeyDown(KeyboardKey.KEY_LEFT))
    {
        _hitbox.x -= _hastighet;
    }
    if (Raylib.IsKeyDown(KeyboardKey.KEY_RIGHT))
    {
        _hitbox.x += _hastighet;
    }
    if (Raylib.IsKeyDown(KeyboardKey.KEY_UP))
    {
        _hitbox.y -= _hastighet;
    }
    if (Raylib.IsKeyDown(KeyboardKey.KEY_DOWN))
    {
        _hitbox.y += _hastighet;
    }
}
```

### virtual

För att `Hjälte` ska kunna använda metoden `Uppdatera()` från `Figur` måste metoden vara `virtual` i `Figur`. Det gör du genom att lägga till `virtual` framför metoden i `Figur`:

```csharp
// Uppdatera positionen
public virtual void Uppdatera()
{
    ...
}
```

### protected

För att `Hjälte` ska kunna använda egenskaperna `_hitbox` och `_hastighet` från `Figur` måste de vara `protected` i `Figur`. Det gör du genom att lägga till `protected` framför egenskaperna i `Figur`:

```csharp
public class Figur
{
    // Egenskaper för figuren
    protected Texture2D _textur;
    protected Rectangle _hitbox;
    protected Vector2 _hastighet;
    protected Vector2 _positionFöreKollision;
    protected Random _slump = new Random();

    ...
}
```

![](/.gitbook/assets/raylib/jagamonster/JagaMonster-diagram-9.png)

## Ändra på andra metoder

På samma sätt om vi vill ha en egen variant av `Uppdatera()` i `Hjälte` så kan vi skriva en egen metod som heter `Uppdatera()` i `Hjälte` och anropa `Figur`s `Uppdatera()` med hjälp av `base.Uppdatera()`.  
Och då måste vi också göra `Figur`s `Uppdatera()` till `virtual`.

![](/.gitbook/assets/raylib/jagamonster/JagaMonster-diagram-10.png)

## Förbättringar

### Förbättring 1

Skapa en ny klass `Monster` som ärver från `Figur`.

### Förbättring 2

När hjälten kolliderar med ett monster så ska monster försvinna och hjälten och liv minska med 1.  
Gör så att monster spawnar om på en slumpmässig plats.

### Förbättring 3

Skapa en klass `Mynt` som ärver från `Figur`.  
När hjälten kolliderar med ett mynt så ska myntet försvinna och hjälten ska få 5 poäng.