---
description: Raylib med OOP
---

# Scenen

Vi skapar en klass `Scen` för att skriva ut lite text på skärmen.  
Den har en privat variabel `_poäng` som håller koll på poängen i spelet och en konstruktor som sätter startvärdet för poängen till 5. Det finns också två metoder: `UppdateraPoäng()` och `Rita()`.

```csharp
// Scen-klass
class Scen
{
    private int _poäng = 0;

    // Uppdatera poängen
    public void UppdateraPoäng(int poäng)
    {
        _poäng += poäng;
    }

    // Rita ut scenen
    public void Rita()
    {
        // Skriv ut text
        Raylib.DrawText("Jaga Monster", 10, 10, 20, Color.WHITE);

        // Skriv ut poängen
        Raylib.DrawText("Poäng: " + _poäng, 500, 40, 20, Color.WHITE);
    }
}
```


```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Scen {
    -_poäng : int
    +Scen() : void
    +UppdateraPoäng(int poäng) : void
    +Rita() : void
}
@enduml
```

## Kollisioner med figurer

Vi vill att `hero` ska kunna kollidera med `monster`.  
Vi lägger till en metod `KolliderarMed()` i klassen `Figur` som kontrollerar om två figurer kolliderar.

```csharp
// Klassen figur
public class Figur
{
    // Egenskaper för figuren
    private Texture2D _textur;
    private Rectangle _hitbox;
    ...

    // Metod för att kontrollera om två figurer kolliderar
    public bool KolliderarMed(Figur figur)
    {
        // Kontrollera om två figurer kolliderar
        if (Raylib.CheckCollisionRecs(_hitbox, figur._hitbox))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
```


```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Figur {
    -_textur : Texture2D
    -_hitbox : Rectangle
    +Figur(string texturFil, int x, int y) : void
    +Rita() : void
    +KolliderarMed(Figur figur) : bool
}
@enduml
```

## Uppdatera Main

I Main skapar vi en scen och uppdaterar poängen om `hero` kolliderar med `monster`.  
Vi ritar ut scenen och uppdaterar skärmen.

```csharp
// Main
static void Main()
{
    // Skapa en scen
    Scen scen1 = new Scen();

    // Spela spelet tills användaren stänger fönstret
    while (!Raylib.WindowShouldClose())
    {
        // Uppdatera scenen
        if (hero.KolliderarMed(monster))
        {
            scen1.UppdateraPoäng(-1);
        }
        ...

        // Rita ut scenen
        scen1.Rita();

        // Uppdatera skärmen
        Raylib.EndDrawing();
    }
}
```

## Förbättringar

### Förbättring 1

Lägg till ett mynt i spelet.
Om `hero` träffar myntet ska poängen öka med 5.

### Förbättring 2

Lägg till en variabel `liv` i klassen `Scen`.  
Om `hero` träffar monster ska `liv` minska med 1.  
Skriv ut hur många liv som finns kvar.
