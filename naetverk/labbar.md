# Labbar

## Labb 1

![](/.gitbook/assets/naetverk/image-10.png)

Skapa en fönsterapp som visar dagens NASA bild ([Astronomy Picture of the Day](http://apod.nasa.gov/apod/astropix.html)) från [https://api.nasa.gov](https://api.nasa.gov):

### Anropet

Anropet ser ut så här:&#x20;

```
https://api.nasa.gov/planetary/apod?api_key=FdZc7kBXQwCcpTDAOcjASquPQWHVW3xseEHlRCS5
```

JSON-svar blir då:

```json
{
    "copyright": "Dietmar Hager",
    "date": "2021-12-02",
    "explanation": "Grand spiral galaxies often seem to get all the glory, flaunting their young, bright, blue star clusters in beautiful, symmetric spiral arms. But small galaxies form stars too, like nearby NGC 6822, also known as Barnard's Galaxy. Beyond the rich starfields in the constellation Sagittarius, NGC 6822 is a mere 1.5 million light-years away, a member of our Local Group of galaxies. A dwarf irregular galaxy similar to the Small Magellanic Cloud, NGC 6822 is about 7,000 light-years across. Brighter foreground stars in our Milky Way have a spiky appearance. Behind them, Barnard's Galaxy is seen to be filled with young blue stars and mottled with the telltale pinkish hydrogen glow of star forming regions in this deep color composite image.",
    "hdurl": "https://apod.nasa.gov/apod/image/2112/NGC6822LRGB.jpg",
    "media_type": "image",
    "service_version": "v1",
    "title": "NGC 6822: Barnard's Galaxy",
    "url": "https://apod.nasa.gov/apod/image/2112/NGC6822LRGB1024.jpg"
}
```

### Appen

#### XAML

Du kan använda följande XAML-element:

* `StackPanel` för layouten
* `Label` för titeln, fylles med `.Content`
* `Image` för bilden fylles med `.Source`
* `TextBlock` för beskrivningen fylles `.Text`

#### C#-koden

Eftersom anropet ska köras direkt, skriver vi koden i metoden `MainWindow().`

Exempel på hur man fyller i en bild i C#:

```csharp
public MainWindow()
{
    InitializeComponent();
    
    // Steg 1 - hämta JSON med WebClient
    
    // Steg 2 - avkoda JSON med JsonSerializer.Deserialize..
    
    // Steg 3 - fyll fönstret med JSON-data
    // Hårdkodat exempel
    bilden.Source = new BitmapImage(new Uri("https://apod.nasa.gov/apod/image/2111/LLPegasi_HubbleLodge_960.jpg"));
}
```

## Labb 2

![](/.gitbook/assets/naetverk/image-11.png)

Bygg en fönsterapp som visar info för vald film. Info om filmer hämtas från [http://www.omdbapi.com](http://www.omdbapi.com).

### Anropet

Exempel på anrop:&#x20;

```
http://www.omdbapi.com/?apikey=ea805224&t=avatar
```

JSON-svar:

```json
{
    "Title": "Avatar",
    "Year": "2009",
    "Rated": "PG-13",
    "Released": "18 Dec 2009",
    "Runtime": "162 min",
    "Genre": "Action, Adventure, Fantasy",
    "Director": "James Cameron",
    "Writer": "James Cameron",
    "Actors": "Sam Worthington, Zoe Saldana, Sigourney Weaver",
    "Plot": "A paraplegic Marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
    "Language": "English, Spanish",
    "Country": "United States",
    "Awards": "Won 3 Oscars. 89 wins & 131 nominations total",
    "Poster": "https://m.media-amazon.com/images/M/MV5BMTYwOTEwNjAzMl5BMl5BanBnXkFtZTcwODc5MTUwMw@@._V1_SX300.jpg",
    "Ratings": [
    {
        "Source": "Internet Movie Database",
        "Value": "7.8/10"
    },
    {
        "Source": "Rotten Tomatoes",
        "Value": "81%"
    },
    {
        "Source": "Metacritic",
        "Value": "83/100"
    }
    ],
    "Metascore": "83",
    "imdbRating": "7.8",
    "imdbVotes": "1,164,464",
    "imdbID": "tt0499549",
    "Type": "movie",
    "DVD": "22 Apr 2010",
    "BoxOffice": "$760,507,625",
    "Production": "N/A",
    "Website": "N/A",
    "Response": "True"
}
```

## Labb 3

![](/.gitbook/assets/naetverk/image-9.png)

Skapa en fönsterapp för att söka på en karaktär i Star Wars filmerna, tex "Luke Skywalker".

För vald karaktär:

* Visar info om karaktären
* Listar alla filmer som en karaktärer är med i

### Anropet

Exempel på anrop:

```
https://swapi.dev/api/people/1
```

JSON-svar:

```json
{
    "name": "Luke Skywalker",
    "height": "172",
    "mass": "77",
    "hair_color": "blond",
    "skin_color": "fair",
    "eye_color": "blue",
    "birth_year": "19BBY",
    "gender": "male",
    "homeworld": "https://swapi.dev/api/planets/1/",
    "films": [
        "https://swapi.dev/api/films/1/",
        "https://swapi.dev/api/films/2/",
        "https://swapi.dev/api/films/3/",
        "https://swapi.dev/api/films/6/"
    ],
    "species": [],
    "vehicles": [
        "https://swapi.dev/api/vehicles/14/",
        "https://swapi.dev/api/vehicles/30/"
    ],
    "starships": [
        "https://swapi.dev/api/starships/12/",
        "https://swapi.dev/api/starships/22/"
    ],
    "created": "2014-12-09T13:50:51.644000Z",
    "edited": "2014-12-20T21:17:56.891000Z",
    "url": "https://swapi.dev/api/people/1/"
}
```
