---
description: Hur vi hämtar data från api:er på Internet
---

# Läsa JSON 1:2

## Väderdata från OWM

![](/.gitbook/assets/naetverk/image-13.png)

### Publikt api med nyckel <a href="#publikt-api-med-nyckel" id="publikt-api-med-nyckel"></a>

* Api:et finns på [openweathermap.org](https://openweathermap.org/)
* Vi kollar på guiden [https://openweathermap.org/current](https://openweathermap.org/current)
* Prova med `appid = "22ee1d58f7adae08ee71fa7c0bd24481"`

### JSON-svaret <a href="#json-svaret" id="json-svaret"></a>

Nu kan vi testa anropet med tex _https://api.openweathermap.org/data/2.5/weather?q=$stad\&appid=$apiid\&units=metric_

```json
{
    "coord": {
        "lon": 18.0649,
        "lat": 59.3326
    },
    "weather": [
        {
            "id": 800,
            "main": "Clear",
            "description": "clear sky",
            "icon": "01d"
        }
    ],
    "base": "stations",
    "main": {
        "temp": 16.54,
        "feels_like": 15.5,
        "temp_min": 15,
        "temp_max": 17.25,
        "pressure": 997,
        "humidity": 48
    },
    "visibility": 10000,
    "wind": {
        "speed": 7.72,
        "deg": 270
    },
    "clouds": {
        "all": 0
    },
    "dt": 1652369703,
    "sys": {
        "type": 1,
        "id": 1788,
        "country": "SE",
        "sunrise": 1652322117,
        "sunset": 1652382381
    },
    "timezone": 7200,
    "id": 2673730,
    "name": "Stockholm",
    "cod": 200
}
```

Vi fokuserar på tre delar:

```json
{
    "weather": [
        {
            "id": 800,
            "main": "Clear",
            "description": "clear sky",
            "icon": "01d"
        }
    ],
     "main": {
        "temp": 16.54,
        "feels_like": 15.5,
        "temp_min": 15,
        "temp_max": 17.25,
        "pressure": 997,
        "humidity": 48
    },
    "visibility": 10000
}
```



De klasser vi behöver då är:

```csharp
public class Prognos
{
    public Weather[] weather { get; set; }
    public Main main { get; set; }
    public int visibility { get; set; }
}

public class Weather
{
    public int id { get; set; }
    public string main { get; set; }
    public string description { get; set; }
    public string icon { get; set; }
}

public class Main
{
    public double temp { get; set; }
    public double feels_like { get; set; }
    public double temp_min { get; set; }
    public double temp_max { get; set; }
    public int pressure { get; set; }
    public int humidity { get; set; }
}
```

### Utskrift av data

Nu kan vi hämta, avkoda och skriva ut svaret såhär:

```csharp
public MainWindow()
{
    InitializeComponent();

    // api-nyckel
    string apiNyckel = "22ee1d58f7adae08ee71fa7c0bd24481";

    // Url:en
    string url = "http://api.openweathermap.org/data/2.5/weather?q=Stockholm,Sweden&units=metric&APPID=" + apiNyckel;

    // Hämta data
    WebClient client = new WebClient();

    // Utför anropet
    string json = client.DownloadString(url);

    // Skapa ett objekt av klassen Väder
    Prognos objekt = JsonSerializer.Deserialize<Prognos>(json);

    // Skriv ut data
    rutaStad.Content = "Stockholm";
    rutaTemp.Text = "Temperaturen är " + objekt.main.temp.ToString() + " grader";
    rutaBeskrivning.Text = objekt.weather[0].description;
}
```

## Förbättringar

* Skapa en TextBox kan användaren ange en stad och program visar vädret där.

## Mer info

* Läs mer om JSON [https://csharp.progdocs.se/csharp-ref/filhantering/serialisering-.../json-serialisering](https://csharp.progdocs.se/csharp-ref/filhantering/serialisering-.../json-serialisering)
