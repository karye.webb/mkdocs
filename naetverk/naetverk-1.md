---
description: Hur vi hämtar data på Internet
---

# Tjänster på Internet

## Hämta data från api

På Internet kan man ansluta till api-tjänster för att hämta intressant data, tex:

* [https://api.chucknorris.io](https://api.chucknorris.io/)

#### Snyggt JSON-format <a href="#json-formatet" id="json-formatet"></a>

* Installera tillägg [JSON formatter](https://chrome.google.com/webstore/detail/json-formatter/bcjindcccaagfpapjjmafapmmgkkhgoa) i Google Chrome

![](/.gitbook/assets/naetverk/image-8.png)

### Vad är ett api? <a href="#vad-ar-ett-api" id="vad-ar-ett-api"></a>

{% embed url="https://www.youtube.com/watch?v=OeXnuug_t3o&feature=emb_logo" %}

## Öppna databas-API:er

Här exempel på några api:er på Internet:

* [Pokemon API](https://pokeapi.co/)
* [Digimon API](https://digimon-api.herokuapp.com/)
* [Star Wars API](https://swapi.dev/)
* [Star Trek API](http://stapi.co/)
* Steam-API
  * [Skaffa en API-nyckel](https://steamcommunity.com/dev/apikey)
  * [Dokumentation](https://partner.steamgames.com/doc/webapi)
* [Marvel API](https://developer.marvel.com/)
* [Superhero API](https://superheroapi.com/)
* [Dog API](https://dog.ceo/dog-api/documentation/)
* [IMDB API](https://imdb-api.com/api)\


### Hur hämtar vi data?

Vi använder oss av `WebClient` med `using System.Net`:

```csharp
using System;
using System.Net;

namespace ChuckNorris
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hämta JSON med WebClient");
            
            WebClient client = new WebClient();

            // Hämta JSON från webben
            string url = "https://api.chucknorris.io/jokes/random";
            string json = client.DownloadString(url);
            
            // Skriv ut JSON
            Console.WriteLine(json);
        }
    }
```

Detta motsvarar ett anrop:

```
GET https://api.chucknorris.io/jokes/random
```

Som ger oss ett **JSON**-svar:

```json
{
    "icon_url" : "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
    "id" : "6181BpiWR_GTOJ1FhC1f5A",
    "url" : "",
    "value" : "Chuck Norris once traveled with the Ringling Bros and Barnum & Bailey Circus as the flaming chainsaw swallower."
}
```

## Avkoda JSON

Vi använda C# inbyggda **JSON**-kod:

```csharp
using System.Text.Json;
```

### En klass för objekten

Vi skapar en klass som motsvarar **JSON**-svaret:

```csharp
class Joke
{
    public string icon_url { get; set; }
    public string id { get; set; }
    public string value { get; set; }
}
```

Skillnaden med tidigare är att vi behöver ange sk **get; set; metoder**, annars är det mycket likt.

### Deserializera JSON

Koden för att deserializera (avkoda) svaret blir då:

```csharp
// Hämta JSON från webben
string json = client.DownloadString(url);

// Skapa objektet som skall lagra JSON-data
Joke skämtet = new Joke();

// Stoppa in svaret i Joke-objektet
skämtet = JsonSerializer.Deserialize<Joke>(json);
Console.WriteLine(skämtet.value);
```

### Resultatet

Hela koden blir då såhär:

```csharp
using System;
using System.IO;
using System.Net;
using System.Text.Json;

namespace ChuckNorris
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hämtar JSON med WebClient");
            WebClient client = new WebClient();

            // Hämta JSON från webben
            string url = "https://api.chucknorris.io/jokes/random";
            string json = client.DownloadString(url);

            // Skapa objektet som skall lagra JSON-data
            Joke skämtet = new Joke();

            // Stoppa in svaret i Joke-objektet
            skämtet = JsonSerializer.Deserialize<Joke>(json);
            Console.WriteLine(skämtet.value);

            // Skriv ut JSON i en fil
            File.WriteAllText("skämt.json", json);
            var options = new JsonSerializerOptions { WriteIndented = true };
            Console.WriteLine(JsonSerializer.Serialize(skämtet, options));
        }
    }

    class Joke
    {
        public string icon_url { get; set; }
        public string updated_at { get; set; }
        public string url { get; set; }
        public string value { get; set; }
    }
}
```

## Mer info

* Läs mer om JSON [https://csharp.progdocs.se/csharp-ref/filhantering/serialisering-.../json-serialisering](https://csharp.progdocs.se/csharp-ref/filhantering/serialisering-.../json-serialisering)
