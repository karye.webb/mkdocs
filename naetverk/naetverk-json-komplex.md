---
description: Hur vi hämtar data från api:er på Internet
---

# Komplex JSON

## PokeApi

Ibland kan JSON-svaret innehålla listor som tex:

```json
{
    "count": 1118,
    "next": "https://pokeapi.co/api/v2/pokemon?offset=20&limit=20",
    "previous": null,
    "results": [
        {
            "name": "bulbasaur",
            "url": "https://pokeapi.co/api/v2/pokemon/1/"
        },
        {
            "name": "ivysaur",
            "url": "https://pokeapi.co/api/v2/pokemon/2/"
        },
        ...
    ]
}
```

Detta svar kommer från [https://pokeapi.co](https://pokeapi.co/), där man kan hämta data om olika Pokemon-figurer.

![](/.gitbook/assets/naetverk/image-12.png)

Hur kan man tolka detta svar? Vi skapar 2 klasser för detta:

```csharp
class Pokemon
{
    public string name { get; set; }
    public string url { get; set; }
}

class Pokemons
{
    public int count { get; set; }
    public List<Pokemon> results { get; set; }
}
```

Sedan hämtar vi JSON-data och deserializerar det:

```csharp
class Program
{
    static string url = "https://pokeapi.co/api/v2";

    static void Main(string[] args)
    {
        Console.Clear();
        Console.WriteLine("Anropar Pokéapi:");

        // Hämta lista på alla pokémon
        var client = new RestClient(url);
        var request = new RestRequest("pokemon", DataFormat.Json);
        var response = client.Get(request);

        // Stoppa in svaret i ett Pokemons-objekt
        Pokemons allaPokemon = JsonSerializer.Deserialize<Pokemons>(response.Content);

        // Kontrollera att vi kunde parsa json, skriv alla pokémon
        if (allaPokemon.results != null)
        {
            foreach (var pokemon in allaPokemon.results)
            {
                Console.WriteLine(pokemon.name);
            }
            Console.WriteLine($"Totalt {allaPokemon.results.Count} pokemon");
        }
        else
        {
            Console.WriteLine("Fick dåligt svar!");
        }
    }
}
```

För säkerhetsskull lägger vi in en kontroll. Om något gick fel får vi `null` vid JSON-avkodningen.
