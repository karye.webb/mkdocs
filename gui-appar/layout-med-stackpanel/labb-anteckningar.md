---
description: Nu bygger vi färdiga fönsterappar
---

# Labb: anteckningar

![](</.gitbook/assets/image-32 copy.png>)

Skapa en ny projektmapp, och kör i konsolen:

```shell
dotnet new wpf
```

## Layouten i MainWindow.xaml

Ersätt ... med rätt kod:

```xml
<Window 
    ...>
    <StackPanel Margin="5">
        <Label ...></Label>
        
        <StackPanel ...>
            <Label ...></Label>
            <TextBox ...></TextBox>
        </StackPanel>
        
        <Button ...></Button>
        <TextBox ...></TextBox>
    </StackPanel>
</Window>
```

### Elementens namn

Du ger elementet ett namn med `Name="..."`. `TextBox`:en skall heta `rutaText`:

```xml
<TextBox Name="rutaText" ...>
```

## Lyssna på Events

### MainWindow.xaml

På samma sätt kan man läsa av vad användaren skrivit:

```xml
<TextBox Name="rutaInput" ...></TextBox>
```

Men det skall göras efter klick på knappen.\
Först ange man ett event `KlickSkicka`:

```xml
<Button Click="KlickSkicka" ...>Skicka!</Button>
```

### MainWindow.xaml.cs

I kodsidan infoga nu en metod `KlickSkicka(object sender, RoutedEventArgs e)` som fångar upp eventet:

```csharp
public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
    }

    private void KlickSkicka(object sender, RoutedEventArgs e)
    {
        // Skriv ut i den stora textrutan med +=
        ..
    }
}
```

Eller skriv ut i **textrutan**:

![](</.gitbook/assets/image-33 copy.png>)

## Utmaningar

* Gör så att anteckningar samlas i den stora rutan
* Spara ned alla anteckningar i en textfil, se [filhantering](https://csharp.progdocs.se/csharp-ref/filhantering/laesa-och-skriva#att-skriva-data-till-en-fil)

## Mer info

* Läs mer om `Button`[ http://www.blackwasp.co.uk/WPFButton.aspx](https://github.com/karye/PRRPRR02-TE/tree/204b427e8d7629378601a047369fc260ddb19a13/WPFButton.aspx)
* Läs mer om `TextBox` [http://www.blackwasp.co.uk/WPFTextBox.aspx](http://www.blackwasp.co.uk/WPFTextBox.aspx)
* Läs mer om `StackPanel` [http://www.blackwasp.co.uk/WPFStackPanel.aspx](http://www.blackwasp.co.uk/WPFStackPanel.aspx)
* Läsr med om `TextBlock` [http://www.blackwasp.co.uk/TextBlock.aspx](http://www.blackwasp.co.uk/TextBlock.aspx)
