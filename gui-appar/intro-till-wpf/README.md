---
description: Hur man skapar en Windows-gui med WPF
---

# En WPF-app

## Resultatet

![](/.gitbook/assets/image-60%20copy.png)

## Grundkoden

I konsolen kör:

```shell
dotnet new wpf
dotnet run
```

Nu ser vi ett fönster på skärmen!

![](/.gitbook/assets/image-61%20copy.png)

**MainWindow.xaml** liknar HTML. Den beskriver hur ett fönster är uppbyggt.

```xml
<Window x:Class="wpf_intro.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/xml-compatibility/2006"
        xmlns:local="clr-namespace:wpf2"
        mc:Ignorable="d"
        Title="MainWindow" Height="450" Width="800">
    <Grid>
    </Grid>
</Window>
```

### VS Code tillägg

För att göra det lättare att skriva XAML i VS Code installera följande några tillägg:

* [Xml Complete](https://marketplace.visualstudio.com/items?itemName=rogalmic.vscode-xml-complete)
* [XML av Red Hat](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-xml)

### Ändra fönstrets utseende

Ändra storleken på fönstret såhär:

```xml
<Window x:Class="wpf_intro.MainWindow" ...
        Title="Min första app" Height="200" Width="150">
        ...
</Window>
```

Ändra bakgrundsfärgen med `Background="#DDD"`:

```xml
<Window x:Class="wpf_intro.MainWindow" ...
        Title="Min första app" Height="200" Width="150"
        Background="#EEE">
        ...
</Window>
```

![](/.gitbook/assets/image-62%20copy.png)

### Infoga formulärelement

Ta bort `Grid` och infoga en `Label` istället:

```xml
<Window ...>
    <Label>Ange en text</Label>
</Window>
```

![](/.gitbook/assets/image-63%20copy.png)

Infoga nu en knapp `Button`:

```xml
<Window ...>
    <Label>Ange en text</Label>
    <Button>OK</Button>
</Window>
```

Prova köra programmet. Du får ett felmeddelande! Du måste bygga en layout!

## Skapa en layout

För att bygga en layout kan man använda `StackPanel` för att stapla element:

```xml
<StackPanel>
    <Label>Ange en text</Label>
    <Button>OK</Button>
    <Button>Avbryt</Button>
</StackPanel>
```

Nu kan du köra programmet.

![](/.gitbook/assets/image-64%20copy.png)

### Infoga en textruta

För att kunna skriva text i HTML används:

```xml
<input type="text"...>
```

I XAML används `TextBox`:

```xml
<StackPanel>
    <Label>Ange en text</Label>
    <TextBox />
    <Button>OK</Button>
    <Button>Avbryt</Button>
</StackPanel>
```

![](/.gitbook/assets/image-65%20copy.png)

### Snygga till layouten

Vi har redan använt attributet `Background` för att byta bakgrundsfärg. Vi ändrar höjden med `Height`:

```xml
<TextBox Height="50" />
```

![](/.gitbook/assets/image-66%20copy.png)

Med attribut `Margin` blir layouten luftigare:

```xml
<Window ...>
    <StackPanel Margin="5">
        <Label>Ange en text</Label>
        <TextBox Margin="5" Height="50" />
        <Button Margin="5" Padding="5">OK</Button>
        <Button Margin="5" Padding="5">Avbryt</Button>
    </StackPanel>
</Window>
```

![](/.gitbook/assets/image-67%20copy.png)

### Fler attribut

Det finns såklart många fler attribut man kan använd för att bygga en gui, testa:

* Ändra OK-knappen till grön
* Ändra Avbryt-knappen till röd
* Prova `FontSize`, `FontFamily` mm
* Prova `MinHeight`, `MaxHeight` på \<Window>

## Händerelser (Events)

WPF-appar är händelsestyrda (events). När man tex klickar på en knapp så skall programmet göra något.

### MainWindow.xaml

För att läsa/skriv i textrutor behöver dessa ett namn:

* `Name="rutaText"`

För att lyssna på knapparnas Click-event skriver man:

* `Click="KlickOk"`
* `Click="KlickAvbryt"`

```xml
<Window ...>
    <StackPanel Margin="5">
        <Label>Ange en text</Label>
        <TextBox Name="rutaText" Margin="5" Height="50" />
        <Button Click="KlickOk" Margin="5">OK</Button>
        <Button Click="KlickAvbryt" Margin="5">Avbryt</Button>
    </StackPanel>
</Window>
```

### MainWindow.xaml.cs

För att fånga upp "klicket" på knapparna skapar man sedan metoder:

* `private void KlickOk(...)`
* `private void KlickAvbryt(...)`

```csharp
...
namespace wpf_intro
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // Ny metod för att hantera klicksignal från knappen Ok
        private void KlickOk(object sender, RoutedEventArgs e)
        {
            MessageBox.Show($"Du skrev: {rutaText.Text}");
        }

        // Ny metod för att hantera klicksignal från knappen Avbryt
        private void KlickAvbryt(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Du klickade på Avbryt");
        }
    }
}
```

## C# snippet för XAML-event

Med snippets kan du skapa egna kortkommandon för kod du ofta använder:

* *Inställningar -> User Snippets-> New Global Snippets -> C#*

```json
{
	"EventXAML": {
		"prefix": "eventXAML",
		"body": [
			"private void $1(object sender, RoutedEventArgs e)",
			"{",
			"\t$2",
			"}"
		],
		"description": "Handle event from XAML WPF"
	}
}
```

## Mer info

* Läs mer om `Button` [http://www.blackwasp.co.uk/WPFButton.aspx](http://www.blackwasp.co.uk/WPFButton.aspx)
