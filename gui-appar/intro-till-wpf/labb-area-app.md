---
description: Nu bygger vi färdiga responsiva fönsterappar
---

# Labb: area app

Skapa en enkel app med 3 textrutor och en knapp. \
När man klickar på knappen räknas A \* B ut och visas i rutan längst ned.

![](</.gitbook/assets/image-57 copy.png>)

I konsolen kör:

```bash
dotnet new wpf
dotnet run
```

## XAML-koden

Ersätt `...` med rätt kod/text:

```xml
<Window ...>
    <StackPanel ...>
        ...
    </StackPanel>
</Window>
```

### Fler attribut

Det finns såklart många fler attribut man kan använd för att bygga en gui, testa:  

* Prova `Background`, `Foreground` på `<Button>`
* Prova `FontSize`, `FontFamily` på `<Label>` och `<TextBox>`
* Prova `MinHeight`, `MaxHeight` på `<Window>`

## C#-koden

Ersätt `...` med rätt kod/text:

```csharp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // Ny metod för att hantera klicksignal från knappen Räkna ut
        private void ...(object sender, RoutedEventArgs e)
        {
        
        }
    }
}
```

