---
description: Nu bygger vi färdiga responsiva fönsterappar
---

# Labb: slumptal app

Skapa en enkel app som slumpar fram ett tal 1-100.

![](</.gitbook/assets/image-58 copy.png>)

I konsolen kör:

```bash
dotnet new wpf
dotnet run
```

## XAML-koden

Ersätt `...` med rätt kod/text:

```xml
<Window ...>
    <StackPanel ...>
        <Label ...>
        <Button ...>
        <TextBox ...>
    </StackPanel>
</Window>
```

### Fler attribut

Anpassa `TextBox` med följande attribut:

* `TextAlignment="Center"`
* `IsReadOnly="True"`
* `FontSize="40"`

## C#-koden

Ersätt `...` med rätt kod/text:

```csharp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // Ny metod för att hantera klicksignal från knappen Räkna ut
        private void ...(object sender, RoutedEventArgs e)
        {
            // Slumpa fram ett tal 1-100
            Random ...
            
            // Skriv i TexBox-rutan
            ...
        }
    }
}
```

## Utmaningar

* Ändra appen så att användaren kan mata in en max-gräns.  
  Se till att appen använder den inmatade gränsen.  
* Om användaren inte skriver ett tal i maxrutan används default max = 101
* Lägg till en statusrad längst ned för att skriva ut felmeddelanden, som att användaren skrivit fel maxtal

![](/.gitbook/assets/labb-2c.png)