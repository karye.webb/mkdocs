---
description: Hur man skapar en Windows-gui med WPF
---

# Intro till WPF

![](/.gitbook/assets/image-68%20copy.png)

## WPF

[WPF](https://docs.microsoft.com/en-us/dotnet/desktop/wpf/overview/) står för Windows Presentation Foundation.  
Det är ett ramverk för att skapa grafiska användargränssnitt (GUI) i Windows. Det är ett ramverk som är skapat av Microsoft och som är en del av .NET Core. 

### Beskrivning

WPF består av två delar:

* XAML - ett språk för att beskriva hur en grafisk användargränssnitt ser ut
* C# - ett ramverk för att skapa logik för en grafisk användargränssnitt

### VS Code tillägg

För att göra det lättare att skriva XAML i VS Code installera följande några tillägg:

* [Xml Complete](https://marketplace.visualstudio.com/items?itemName=rogalmic.vscode-xml-complete)
* [XML av Red Hat](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-xml)

### Youtube-tutorial

{% embed url="https://www.youtube.com/watch?v=Vjldip84CXQ&list=PLrW43fNmjaQVYF4zgsD0oL9Iv6u23PI6M" %}

## Andra ramverk

Det finns flera andra ramverk för att skapa grafiska användargränssnitt i Windows. Det finns till exempel:

* [WinForms](https://docs.microsoft.com/en-us/dotnet/desktop/winforms/?view=netdesktop-5.0) - ett äldre ramverk av Microsoft och som är en del av .NET Framework
* [UWP](https://docs.microsoft.com/en-us/windows/uwp/get-started/universal-application-platform-guide) - ett ramverk av Microsoft och som är en del av .NET Core
* [Xamarin](https://dotnet.microsoft.com/apps/xamarin) - ett cross-platform ramverk av Microsoft för att skapa grafiska användargränssnitt för Android och iOS
* [Uno](https://platform.uno/) - ett cross-platform ramverk för att skapa grafiska användargränssnitt i Windows, macOS, Linux, Android och iOS
* [Flutter](https://flutter.dev/) - ett ramverk av Google
* [GTK](https://www.gtk.org/) - ett ramverk för att skapa grafiska användargränssnitt i Linux
* [Qt](https://www.qt.io/) - liksom Gtk är ett ramverk för att skapa grafiska användargränssnitt i Linux
* [Electron](https://www.electronjs.org/) - ett ramverk för att skapa cross-platform grafiska användargränssnitt i Windows, macOS och Linux med hjälp av JavaScript, HTML och CSS