# Läsa in JSON

Vi skapar ett konsolprogram som läser namnen på alla länder i världen från en fil i JSON-format.   
Användaren ska kunna söka efter ett land och få upp information om det.

## Startkoden
Skapa ett konsolprogram som heter `Countries`.  

### Läser in JSON

Vi börjar med att läsa in alla länder som finns i JSON-format:
Ladda ned JSON-filen med all data från {% file src="/.gitbook/assets/samlingar/countries.json" %}

Program.cs:

```csharp
static void Main(string[] args)
{
    Console.WriteLine("Landlexikon");

    // Läser in json-filen
    string jsonString = File.ReadAllText("countries.json");

    // Deserialiserar json med array av objekt
    var countryList = JsonSerializer.Deserialize<List<Country>>(jsonString);

    // Om vi inte får någon JSON, återställ listan till tom
    if (countryList == null)
    {
        countryList = new List<Country>();
    }

    // Skriver ut vad vi hittat
    int i = 0;
    foreach (var country in countryList)
    {
        i++;
        Console.WriteLine($"{i}: {country.Name} {country.Code}");
    }
}
```

Country.cs:

```csharp
class Country
{
    public string Code {get; set;}
    public string Name {get; set;}
}
```

## Uppgift

Gör nu så att användaren kan mata in en sökterm, och programmet hittar matchande länder. Du kan använda [.Contains()](https://csharp.progdocs.se/csharp-ref/grundlaeggande/listor-och-arrayer#contains).

## Mer info

Läs mer om JSON på [C# snabbref](https://csharp.progdocs.se/csharp-ref/filhantering/serialisering-.../json-serialisering)
