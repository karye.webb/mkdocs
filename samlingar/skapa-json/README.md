---
description: Använda objekt för att skriva ut JSON
---

# Skapa JSON

## JSON-formatet

Idag använder man JSON för att lagra data i en textfil, som tex:

![Inställningar i VS Code](/.gitbook/assets/samlingar/skapa-json/image-7.png)

Hur skapar man nu detta JSON-format?

### Skapa JSON-text

Vi skapar ett projekt:

```bash
dotnet new console 
```

### Lagra information i ett objekt

Låt säg att vi skall spara en films titel, årtal och regissör. Vi skriver tex:

* Titel = "Avatar"
* Årtal = "2009"
* Regissör = "James Cameron"

I vår kod blir det:

```csharp
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Program för att skapa JSON");

        // Skapa ett objekt för filmen "Avatar"
        Film avatar = new Film
        {
            Titel = "Avatar",
            Artal = "2009",
            Regissor = "James Cameron"
        };

        // Skapa JSON-text från objekt
        var options = new JsonSerializerOptions { WriteIndented = true };
        string JsonText = JsonSerializer.Serialize(avatar, options);

        // Skriv ut JSON-texten
        Console.WriteLine(JsonText);
        
        // Spara JSON-text i en textfil
        File.WriteAllText("filmer.json", JsonText);
    }
}
```

En sak saknas, mallen dvs klassen med de tre egenskaperna:

* Titel
* Artal
* Regissor

```csharp
using System;

namespace JsonIntro
{
    class Film
    {
        public string Titel { get; set; }
        public string Artal { get; set; }
        public string Regissor { get; set; }
    }
    
    class Program
    {
    ...
```

Resultatet ser ut såhär i **filmer.json**

```json
{
  "Titel": "Avatar",
  "Artal": "2009",
  "Regissor": "James Cameron"
}
```

## Utökad JSON

Om vi vill ha en JSON-text med flera objekt i, hur gör vi då?

### Fler objekt

Skapa en lista som skall innehålla objektet `Movie`:

```csharp
static List<Film> lista = new List<Film>();
```

Och så lägger vi till **avatar**-objektet i listan:

```csharp
// Infoga avatar-objektet i listan
lista.Add(avatar);
```

Nu kan vi lägga till fler filmer i listan:

```csharp
// Skapa ett andra objekt
Film legend = new Film
{
    Titel = "I Am Legend",
    Artal = "207",
    Regissor = "Francis Lawrence"
};
lista.Add(legend);

// Skapa ett tredje objekt
Film starwars = new Film
{
    Titel = "Star Wars: The Force Awakens",
    Artal = "2015",
    Regissor = "J.J. Abrams"
};
lista.Add(starwars);
```

### Skapa JSON-text

Vi skapar JSON av listan och sparar ned i en fil:

```csharp
// Skapa JSON från listan
string JsonTextLista = JsonSerializer.Serialize(lista, options);
Console.WriteLine(JsonTextLista);
File.WriteAllText("filmlista.json", JsonTextLista);
```

### Resultatet

```json
[
  {
    "Titel": "Avatar",
    "Artal": "2009",
    "Regissor": "James Cameron"
  },
  {
    "Titel": "I Am Legend",
    "Artal": "207",
    "Regissor": "Francis Lawrence"
  },
  {
    "Titel": "Star Wars: The Force Awakens",
    "Artal": "2015",
    "Regissor": "J.J. Abrams"
  }
]
```

## Mer info

Läs mer om JSON på [C# snabbref](https://csharp.progdocs.se/csharp-ref/filhantering/serialisering-.../json-serialisering)
