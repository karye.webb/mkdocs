---
description: Träna på att skapa JSON och parsa JSON
---

# Uppgifter

## Uppgift 1

* Skapa ett konsolprogram som heter AdressLista.
* Användaren matar in **förnamn**, **efternamn** och **gatuadress**. Allt detta sparas i ett objekt.
* Skapa en klass som heter `NamnAdress`.
