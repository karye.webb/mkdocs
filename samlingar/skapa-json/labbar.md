---
description: Träna på att skapa JSON och parsa JSON
---

# Labbar

## Labb 2

Skapa ett konsolprogram Inkomster.

### Klassen

Börja med att skapa en klass för objekten:

```csharp
class PersonInkomst
{
    public string Namn {get; set;}
    public string Lon {get; set;}
}
```

### Lista

Sen skapar vi en lista som lagrar objekten:

```csharp
static List<PersonInkomst> lista = new List<PersonInkomst>();
```

För varje gång vi matar in en person med lön, infogar vi den i listan:

```csharp
lista.Add(person);
```

### Spara listan i json-fil

```csharp
// Skapa JSON från objekt
string json = JsonSerializer.Serialize(lista, options);

// Skriv ut JSON
File.WriteAllText(@"inkomster.json", json);

rutaFel.Text = "Namnlistan har sparats som inkomster.json";
```

### Resultatet

```json
[
  {
    "Namn": "Kalle",
    "Lön": "25000"
  },
  {
    "Namn": "Johan",
    "Lön": "24000"
  }
]
```
