---
description: Träna på List
---

# Labbar

## Labb 1

### Resultat

Vi skapar en fönsterapp där användaren kan registrera personer och deras lön.

![](/.gitbook/assets/samlingar/intro-till-list/image-4.png)

### XAML

Vi skapar en ny rad:

```xml
<Label Grid.Row="2" Grid.Column="0" Margin="5">Ange lön</Label>
<TextBox Grid.Row="2" Grid.Column="1" Name="rutaLön" Margin="5"
Padding="5"></TextBox>
```

### Spara data i en lista

För varje person skall vi spara namn och lön. För enkelhetens skull sparar vi lön som en string.\
Listan kommer nu se ut såhär:

```xml
static List<string> namnlista = new List<string>();
```

Och när vi infogar ett element blir det:

```csharp
namnlista.Add($"{namn}\t{lön}");
```

### Spara i fil

```csharp
File.WriteAllLines(@"namnlista.tsv", namnlista);
rutaFel.Text = "Namnlistan har sparats som namnlista.tsv";
```

Hur ser då textfilen ut?

```csharp
Ewa     10000
John    5000
johan   23000
```
