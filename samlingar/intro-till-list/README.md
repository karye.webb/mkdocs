---
description: Skapa en app för att spara kontakter
---

# Listor

## Resultat

![](/.gitbook/assets/samlingar/intro-till-list/image-5.png)

## Fönstret med WPF Grid

### Skapa layouten med Grid

Mha `Grid` bygger vi en layout 6x2 med `Label`, `TextBox` och `Button`:

```xml
<Window x:Class="Kontaktlista.MainWindow"
...
       Title="Kontaktlista"
       MinWidth="250" MaxWidth="400" MinHeight="400" MaxHeight="600"
       SizeToContent="WidthAndHeight" Background="#EEE">
    <Grid Margin="10">
        <Grid.RowDefinitions>               <!-- Grid 6 x 2 -->
            <RowDefinition Height="Auto" /> <!-- 0 -->
            <RowDefinition Height="Auto" /> <!-- 1 -->
            <RowDefinition Height="Auto" /> <!-- 2 -->
            <RowDefinition Height="Auto" /> <!-- 3 -->
            <RowDefinition Height="*" />    <!-- 4 -->
            <RowDefinition Height="Auto" /> <!-- 5 -->
        </Grid.RowDefinitions>
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="Auto" /> <!-- 0 -->
            <ColumnDefinition Width="*" />    <!-- 1 -->
        </Grid.ColumnDefinitions>

        <Label Grid.Row="0" Grid.ColumnSpan="2" FontSize="24">Kontaktlista</Label>

        <Label Grid.Row="1" Grid.Column="0" Margin="5">Ange namn</Label>
        <TextBox Grid.Row="1" Grid.Column="1" Name="rutaNamn" Margin="5" Padding="5"></TextBox>
        <Label Grid.Row="2" Grid.Column="0" Margin="5">Ange mobil</Label>
        <TextBox Grid.Row="2" Grid.Column="1" Name="rutaMobil" Margin="5" Padding="5"></TextBox>

        <Button Grid.Row="3" Grid.ColumnSpan="2" Margin="5" Height="30" 
        Click="KlickLäggTill">Lägg till</Button>

        <TextBox Grid.Row="4" Name="rutaLista" Grid.ColumnSpan="2" Margin="5" 
        TextWrapping="Wrap" IsReadOnly="True" VerticalScrollBarVisibility="Auto" />

        <TextBox Grid.Row="5" Name="rutaStatus" Grid.ColumnSpan="2" Margin="5" 
        IsReadOnly="True" Background="#EEE" Foreground="#999">0 kontakter</TextBox>
    </Grid>
</Window>
```

### MainWindow.xaml.cs

I **MainWindow.xaml.cs** skriver vi:

```csharp
public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
    }

    private void ClickLäggTill(object sender, RoutedEventArgs e)
    {
        // Läs in namn & mobil
        string namn = rutaNamn.Text;
        string mobil = rutaMobil.Text;
        
        // Om namn & mobil ej tomma
        if (namn != "" && mobil != "")
        {
            // Infoga namn och mobil i listrutan
            rutaLista.Text += namn + "\t" + mobil + "\n";
        }
        else
        {
            // Skriv ut felmeddelande
            rutaStatus.Text = "Inget namn matades in, försök igen!";
        }
    }
}
```

Inget konstigt så här långt!

### Spara ned i textfil

Vi lägger en rad för att spara ned alla kontakter i en textfil:

```csharp
private void KlickLäggTill(object sender, RoutedEventArgs e)
{
    ...

    File.WriteAllLines(@"kontakter.txt", namnlista);
}
```

Men var får vi `namnlista` ifrån?

## Samla in namnlista

### I en array

Först `namnlista` som en array av string:

```csharp
static string[] lista = new string[100];
```

Det leder till två "problem". För det första måste vi alltid skapa en `tillräckligt stor array** i förväg, eftersom vi inte vet hur många namn användaren skall registrera. För det andra behöver vi en variabel för hålla koll på **antalet registrerade** namn. Dvs:

```csharp
public partial class MainWindow : Window
{
    static string[] lista = new string[100];
    static int antal = 0;

    public MainWindow()
    {
        InitializeComponent();
    }

    private void KlickLäggTill(object sender, RoutedEventArgs e)
    {
        // Läs in namn & mobil
        string namn = rutaNamn.Text;
        string mobil = rutaMobil.Text;
        
        // Om namn & mobil ej tomma
        if (namn != "" && mobil != "")
        {
            // Infoga namn och mobil i listrutan
            rutaLista.Text += namn + "\t" + mobil + "\n";
            
            // Infoga kontakten i interna listan
            lista[antal - 1] = namn;
            antal++;

            rutaStatus.Text = antal + 1 + " namn i listan";
        }
        else
        {
            rutaStatus.Text = "Inget namn matades in, försök igen!";
        }
    }
}
```

### I en lista

En enklare variant på samling är sk listor som är en generisk klass.\
En lista kan växa allt eftersom vi behöver det. Först deklarationen:

```csharp
...
    public partial class MainWindow : Window
    {
        static List<string> lista = new List<string>();
...
```

Att infoga ett element i listan är mycket enkelt:

```csharp
lista.Add(namn);
```

Antalet kontakter i listan får vi av `.Count`:

```csharp
rutaStatus.Text = lista.Count + " namn i listan";
```

![](/.gitbook/assets/samlingar/intro-till-list/image-6.png)

### kontakter.txt

Textfilens innehåll ser ut såhär:

```
John	0708798798
Anna	0704654652
Louisa	0707646546
Sid	07065761768
Kasimir	0734532543
```

## Ladda in kontaktlista

När appen startar vore det bra den kunde ladda in kontaktlistan från **kontakter.txt**:

```csharp
public MainWindow()
{
    InitializeComponent();

    // Kontrollera om filen finns och ladda in
    if (File.Exists(@"kontakter.txt"))
    {
        // Ladda in
        lista = File.ReadAllLines(@"kontakter.txt").ToList();
    }
}
```

#### Visa kontakterna

Nu när den interna listan är laddad, kan vi enkelt skriva ut i fönstret:

```csharp
// Fyll rutaLista med namn och mobil
foreach (string kontakt in lista)
{
    rutaLista.Text += kontakt + "\n";
}
```

#### Feedback

Det är bra att berätta användaren vad som hänt:

```csharp
// Feedback
rutaStatus.Text = lista.Count + " namn i listan";
```

## Förbättringar

* Se till att mobilen har rätt format med [regex](https://uibakery.io/regex-library/email-regex-csharp)
* Se till nya kontakter inte har samma namn som de de som redan finns i listan

## Mer info

* C# referens om [List och array](https://csharp.progdocs.se/csharp-ref/grundlaeggande/listor-och-arrayer)
* På charpskolan hittar [generiska-klasser](https://csharpskolan.se/article/generiska-klasser/)
