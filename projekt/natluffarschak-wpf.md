# Nätluffarschak - WPF

_Förkunskaper är OOP, nätverksprogrammering och WPF._

## Bakgrund

Nätluffarschak är ett program som låter dig spela luffarschack mot en annan spelare över internet. Det är ett program som är skrivet i C# och använder sig av WPF för att skapa en grafisk användargränssnitt.

Se tidigare version av programmet [labb-luffarschak.md](../gui-appar/labb-luffarschak.md "mention").

![](/.gitbook/assets/projekt/image-133.png)

## Uppgift

Uppgiften är att skapa en klient/server applikation som låter dig spela luffarschack mot en annan spelare. Du ska kunna välja om du vill spela mot en annan spelare på samma dator eller mot en spelare på en annan dator. Du ska även kunna välja om du vill spela mot en dator eller en annan spelare.

### Servern

Servern ska vara en konsolapplikation som tar emot anslutningar från klienter.  

* [ ] Servern ska starta ett nytt spel när två klienter har anslutit sig.
* [ ] Servern håller på turordningen och skickar tillbaka till klienterna när det är deras tur att spela.
* [ ] Servern håller reda på spelstatus och skickar tillbaka till klienterna när någon har vunnit eller om det blir oavgjort.
* [ ] Servern ska kunna hantera flera klienter samtidigt.
* [ ] Servern ska kunna hantera att en klient kopplar ifrån sig.

Tips [RESTful-server](https://csharp.progdocs.se/csharp-ref/annat/naetverk-och-internet-.../restful-server)

Skapa ett nytt projekt i VS Code med webbapplikation template:

```shell
dotnet new webapi
```

### Klienten

Klienten är en fönsterapplikation som ansluter sig till servern.

* [ ] Klienten ska vara ett WPF-program.
* [ ] IP-adressen till servern ska kunna anges i en textruta.
* [ ] Klienten ska kunna ansluta till servern.
* [ ] Klienten ska kunna välja om den vill spela mot en annan spelare eller mot en dator.

Tips [RESTful-client](https://csharp.progdocs.se/csharp-ref/annat/naetverk-och-internet-.../restful-client)


* Skapa ett klassdiagram över din lösning, både för Servern och Klienten.