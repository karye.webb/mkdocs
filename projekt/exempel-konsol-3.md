---
description: Exempel på konsoleapplikationer
---

# Exempel på konsoleapplikationer

## Konsoleapplikationer med tre klasser

#### Matlagningsrepertoar

En konsolapplikation för att hålla koll på sin **matlagningsrepertoar**, där användaren kan lägga till och ta bort recept samt söka efter recept baserat på ingredienser eller kategori.

#### Träningsmål

En konsolapplikation för att hålla koll på sina **träningsmål och förloppet**, där användaren kan lägga till och ta bort mål samt logga träningspass och visa statistik över förloppet.

#### Kondition

En konsolapplikation för att hålla koll på sin vikt och **kroppsmått**, där användaren kan logga sin vikt och mått över tid och visa grafer över förloppet.