---
description: Spelet Pong
---

# Spelet Pong

![Alt text](/.gitbook/assets/projekt/pong.png)

## Beskrivning

Pong är ett arkadspel som är skapat av Atari 1972. Spelet är grafiskt och spelas på en dator eller konsol. Spelet går ut på att två spelare kontrollerar paddlar som ska försöka få en boll att träffa motståndarens sida. Bollen studsar från paddlarna och väggarna. Spelet är över när bollen träffar en spelares sida.

Spelet kan ha olika svårighetsgrader, som att paddlarna kan röra sig snabbare eller att bollen studsar snabbare. Spelet kan också ha olika variationer, som att paddlarna kan röra sig vertikalt istället för horisontellt.

## Pseudokod

För att skapa ett Pong-spel med hjälp av klasser, skulle pseudokoden se ungefär så här:

Skapa en klass för spelet, tex:  

* `Game`-klassen skulle hålla ihop allt som rör spelet, inklusive initiering, uppdatering och rendering av spelobjekten. 

Skapa klasser för spelobjekten:  

* `Ball` för bollen med egenskaper som position, hastighet och storlek.
* `Paddle` för paddlarna med egenskaper som position, hastighet och storlek.
* `Score` för poängräkningen med egenskaper som poäng för varje spelare.

Lägg till metoder för varje klass:  

* `Game`:  
  * `Initialize()` för att skapa och initiera spelobjekten och sätta spelet i rullning.
  * `Update()` för att uppdatera spelobjekten och hålla reda på spelets status.
  * `Render()` för att rita ut spelobjekten på skärmen.
* `Ball`:  
  * `Move()` för att uppdatera bollens position baserat på hastighet och riktning.
  * `Collide()` för att hantera kollisioner med paddel och väggar.
* `Paddle`:
  * `Move()` för att uppdatera paddelns position baserat på hastighet och input från spelaren.
* `Score`:  
  * `Update()` för att uppdatera poängräkningen vid varje poäng.
  * `Render()` för att visa poängen på skärmen.

Skapa en huvudloop i `Main()` metoden i `Game` klassen, som kallar på metoderna `Update()` och `Render()` för att hålla spelet igång.
Detta är en grov beskrivning av pseudokoden för att skapa ett Pong-spel med hjälp av klasser, det faktiska implement

Dessa klasser skulle hålla all kod för dessa objekt organiserat och gör det lättare att underhålla och ändra programmet.
