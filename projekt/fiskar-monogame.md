# Fiskar - MonoGame

_Förkunskaper är OOP och MonoGame._

I detta projekt får Du möjlighet att skapa och designa ett eget spel.  
Men inte riktigt ändå, eftersom ni endast får använda den bifogade grafiken som följer med.

![](/.gitbook/assets/projekt/image-132.png)

## Förkunskaper

Inga direkta förkunskaper krävs inom MonoGame.  
Rekommenderat är att följa nedan angivna tutorials. Följ dem noga och skriv av koden, undvik Copy & Paste!

* [monogame-snabbstart/ ](http://csharpskolan.se/article/monogame-snabbstart/) Främst ljud & musik

Du får endast använda bifogad grafik till ert spel. Ljud & musik får Du hitta på annat håll.

## Grafik till spelet
Ladda ned grafiken: 

{% file src="https://csharpskolan.se/wp-content/uploads/2018/09/projekt\_fiskar\_grafik.zip" %}

## Klassdiagram

Du ska göra en inledande analys över vilka klasser som kommer att behövas och hur Du ska strukturera er. Ett tips är att kika lite på designen till MonoAsteroids.  
Ett klassdiagram/analys bör göras _innan_ Du sätter igång med programmeringen.

{% hint style="info" %}
Uppgift från [csharpskolan.se](https://csharpskolan.se/)
{% endhint %}
