---
description: Exempel på konsoleapplikationer
---

# Exempel på konsoleapplikationer

## Konsoleapplikationer med en klass

#### Inköpslista

En konsolapplikation för att hålla koll på **inköpslista**, där användaren kan lägga till och ta bort varor från listan och spara den till en textfil.

Pseudokod för programmet:

* Skapa en tom lista för inköpsvaror.
* Skriv en loop som visar användarmenyn: lägg till vara, ta bort vara, visa lista, sök vara, spara lista till fil, läs lista från fil, avsluta.
* Om användaren väljer "lägg till vara", fråga efter varans namn och lägg till den i inköpslistan.
* Om användaren väljer "ta bort vara", fråga efter varans namn och ta bort den från inköpslistan.
* Om användaren väljer "visa lista", visa innehållet i inköpslistan.
* Om användaren väljer "sök vara", fråga efter varans namn och sök i inköpslistan. Om varan finns, visa den. Annars, visa ett felmeddelande.
* Om användaren väljer "spara lista till fil", försök att öppna en textfil för skrivning. Om filen kan öppnas, spara innehållet i inköpslistan till den. Annars, visa ett felmeddelande.
* Om användaren väljer "läs lista från fil", försök att öppna en textfil för läsning. Om filen kan öppnas, läs innehållet i filen och lägg till varorna i inköpslistan. Annars, visa ett felmeddelande.
* Om användaren väljer "avsluta", fråga om användaren vill spara ändringar till filen. Om användaren svarar ja, spara innehållet i inköpslistan till en textfil. Avsluta programmet.
* Upprepa loopen tills användaren väljer "avsluta".

Här är några förslag på hur man kan utöka programmet:

* Möjlighet att sortera listan, t.ex. i alfabetisk ordning eller i ordning efter inköpsdatum.
* Möjlighet att tilldela varor till olika kategorier (t.ex. matvaror, hygienprodukter, elektronik).
* Möjlighet att lägga till flera detaljer till varje vara, t.ex. antal, pris, inköpsdatum.
* Möjlighet att spara och läsa flera olika inköpslistor till och från filer.
* Möjlighet att visa en sammanställning av alla inköpta varor och deras totala kostnad.
* Möjlighet att markera varor som redan har köpts och visa en lista över varor som fortfarande behöver köpas.
* Möjlighet att skriva ut inköpslistan, t.ex. som en PDF-fil.
* Möjlighet att lägga till bilder till varorna i listan.

#### Träningsrutiner

En konsolapplikation för att hålla koll på **träningsrutiner**, där användaren kan lägga till och ta bort övningar från sin rutin och spara den till en textfil.

#### Personlig budget

En konsolapplikation för att hålla koll på **personlig budget**, där användaren kan lägga till och ta bort transaktioner och visa sin nuvarande budget.
