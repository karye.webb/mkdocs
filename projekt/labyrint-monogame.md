# Labyrint - MonoGame

_Förkunskaper är MonoGame._

![](/.gitbook/assets/projekt/image-136.png)

## Uppgift

Du ska skapa en 40×22 labyrint där spelaren kan navigera runt.  
Detta skulle kunna vara en utgångspunkt till en vidareutveckling.

## Kravlista

Kraven är listade i prioriteringsordning:

* [ ] Du ska kunna navigera runt i labyrinten med tangenterna W, A, S, D.
* [ ] Du ska kunna se var spelaren gått i labyrinten.
* [ ] Spelaren ska inte kunna gå igenom väggar.
* [ ] En spotlights effekt ska visas runt spelaren som avslöjar labyrinten.
* [ ] Trycka på R ska starta om spelet.


## Vidareutveckling

* [ ] Du kan infoga en minimap över labyrinten.
* [ ] Du kan infoga tid eller tid kvar.
* [ ] Du kan fiender och skatter.

## Kod

Projektet är Visual Studio 2017 med MonoGame 3.7.  
Ladda ned projektet:

{% file src="https://csharpskolan.se/wp-content/uploads/2019/09/LabyrinthGame.zip" %}

{% hint style="info" %}
Uppgift från [csharpskolan.se](https://csharpskolan.se/)
{% endhint %}
