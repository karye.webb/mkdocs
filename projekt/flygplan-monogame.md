# Flygplan - MonoGame

_Förkunskaper är OOP och MonoGame._

I detta projekt får du möjlighet att skapa och designa ett eget spel.  
Men inte riktigt ändå, eftersom du endast får använda den bifogade grafiken som följer med.

![](/.gitbook/assets/projekt/image-135.png)

## Förkunskaper

Inga direkta förkunskaper krävs inom MonoGame.  
Rekommenderat är att följa nedan angivna tutorials. Följ dem noga och skriv av koden, undvik Copy & Paste!

* [monogame-snabbstart/ ](http://csharpskolan.se/article/monogame-snabbstart/) Främst ljud & musik

## Grafik till spelet

Du får endast använda bifogad grafik till ert spel. Ljud & musik får Du hitta på annat håll.  
Ladda ned grafiken:

{% file src="https://csharpskolan.se/wp-content/uploads/2018/09/projekt\_tappy\_gfx.zip" %}

## Klassdiagram

Du ska göra en inledande analys över vilka klasser som kommer att behövas och hur Du ska strukturera er. Ett tips är att kika lite på designen till MonoAsteroids.  
Ett klassdiagram/analys bör göras _innan_ Du sätter igång med programmeringen.

Spelets inriktning styrs såklart av den grafik Du fått. En stor anledning till varför spel inte blir klara är att man underskattar hur lång tid det faktiskt tar att t.ex. få fram grafik. På detta sätt slipper Du den biten och kan koncentrera er helt på programmeringen och att få spelet spelbart!

{% hint style="info" %}
Uppgift från [csharpskolan.se](https://csharpskolan.se/)
{% endhint %}
