---
description: Skapa spelet Asteroids i MonoGame
---

# Labb MonoAsteroids

## Nytt MonoGame projekt

* Skapa en ny projektmapp **MonoAsteroids**.
* Skapa en MonoGame projekt:

```powershell
dotnet new mgdesktopgl
```

## MonoAsteroids

{% embed url="https://youtu.be/9YMi0sSOgH8" %}

### Assets

Grafik och kod till spelet hittar du på [csharpskolan.se](https://csharpskolan.se/article/monoasteroids-del-1/)

Vi använder sprites från: [Kenneys Game Assets](https://www.kenney.nl/assets)

Här nedan hittar du den zip-fil med grafik och ljud som du behöver för att kunna följa artikeln. Ladda hem den och packa sedan upp innehållet på t.ex. skrivbordet:

{% file src="/.gitbook/assets/monogame/monoasteroids_content.zip" %}

### Startkoden

#### MonoAsteroids.cs

```csharp
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MonoAsteroids
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class MonoAsteroids : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public MonoAsteroids()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
```

#### Program.cs

```csharp
using System;

namespace MonoAsteroids
{
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = new MonoAsteroids())
                game.Run();
        }
    }
}
```

### Förbättring 1

Första delen i en serie om spelprogrammering i MonoGame. Vi tittar på design, uppritning, rotering och fysik i vårt försök att skapa en kopia av det klassiska spelet Asteroids.

{% embed url="https://youtu.be/NohyT2OwlTs" %}

### Förbättring 2

Andra delen i en serie om spelprogrammering i MonoGame. Vi tittar på design, meteoriter, skott och kollisioner i vårt försök att skapa en kopia av det klassiska spelet Asteroids.

{% embed url="https://youtu.be/AovI7UMH2Ak" %}

### Förbättring 3

Tredje delen i en serie om spelprogrammering i MonoGame. Vi tittar på explosioner, ljudeffekter och olika typer av meteoriter i vårt försök att skapa en kopia av det klassiska spelet Asteroids.

{% embed url="https://youtu.be/XcyrN0wjzxs" %}

### Förbättring 4

Fjärde delen i en serie om spelprogrammering i MonoGame. Vi storstädar, inför meddelandehantering med designmönstret Mediator och inför också Game States.

{% embed url="https://youtu.be/zGbznqPDA6g" %}

### Förbättring 5

Femte delen i en serie om spelprogrammering i MonoGame. Vi slutför och gör spelet "klart". Vi avslutar med en diskussion kring features man kan lägga till för att göra spelet roligare.

{% embed url="https://youtu.be/IHjBYZxhZdY" %}

## Förbättringar

Spelet MonoAsteroids är i princip komplett men det går alltid att göra lite mer intressant. I denna uppgift får du möjlighet att skapa både nya vapen och powerups. Till din uppgift får du grafik som du kan använda till både olika skott och powerups.

{% file src="https://csharpskolan.se/wp-content/uploads/2018/09/lasers.zip" %}

{% file src="https://csharpskolan.se/wp-content/uploads/2018/09/power-ups.zip" %}

* Skapa minst en ny klass som beskriver minst en ny sorts vapen. Gör detta genom att införa ett arv från klassen `Shot`. Du ska också kunna växla mellan det vanliga vapnet och ditt nya vapen. Ett exempel skulle kunna vara ett vapen som skjuter större skott men som går långsammare.
* Det nya skottet _kan_ också ha en ”cool down” timer, dvs. att man inte kan skjuta hur snabbt som helst.
* Det nya skottet _kan_ ha en annan effekt på meteoriterna, t.ex. att de försvinner direkt utan att dela på sig.

Skapa en klass `PowerUpManager` som hanterar minst två sorters powerups. Powerups ska kunna komma slumpmässigt när du skjuter sönder meteoriter.

* Nya powerups _kan_ ge dig nytt liv.
* Nya powerups _kan_ ge dig ett nytt vapen, t.ex. att du inte har alla vapen från början.
* Nya powerups _kan_ t.ex. automatiskt spränga X meteoriter.

Modellera dina klasser med klassdiagram innan du börjar koda.

* Powerups och val av vapen _bör_ också visas i användargränssnittet om de har en långvarig effekt.

{% hint style="info" %}
Uppgift från [csharpskolan.se](https://csharpskolan.se/)
{% endhint %}
