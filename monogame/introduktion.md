---
description: Skapa grafiskt spel med biblioteket MonoGame
---

# Introduktion

## Installera MonoGame

### Biblioteket och hjälp app

Vi börjar med att skapa en ny mapp **MonoGame** för alla framtida spelprojekt.  
Installera Monogame template för VS Code genom att köra följande kommando i terminalen:

```bash
dotnet tool install --global dotnet-mgcb-editor --version 3.8.0.1641
mgcb-editor --register
dotnet new --install MonoGame.Templates.CSharp::3.8.0.1641
```

### Installera MonoGame VSCode-tillägget

* Installera [mgcb-vscode](https://marketplace.visualstudio.com/items?itemName=mgcb-vscode.mgcb-vscode)

Du kan starta **MonoGame Pipeline Tool** genom att öppna **Content/Content.mcgb**:

![](/.gitbook/assets/monogame/image-19.png)

## Monogames programflöde

Monogame använder ett flöde som är liknande till det som används i Unity.  
Det finns tre delar i ett Monogame projekt:  
* **Content**: Innehåller alla filer som används i spelet.  
* **Game**: Innehåller alla klasser som används i spelet.  
* **Game1**: Innehåller all kod som startar spelet.

Monogames flöde består av 5 metoder. Se bilden nedan:

* `Initialize()`: Sätter upp allt som behövs för spelet
* `LoadContent()`: Laddar in alla filer som behövs för spelet
* `Draw()`: Ritar ut allt som ska visas på skärmen
* `Update()`: Uppdaterar allt som ska uppdateras i spelet
* `UnLoadContent()`: Tar bort allt som inte behövs längre

![](/.gitbook/assets/monogame/image-20.png)

## Mer info

* Läs mer om MonoGame här på [progsharp.se](https://progsharp.se/kapitel/8/)
